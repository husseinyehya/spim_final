package br.com.ecommet.filter;

import java.io.IOException;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.ext.ReaderInterceptor;
import javax.ws.rs.ext.ReaderInterceptorContext;

@Provider
public class Interceptor implements ReaderInterceptor {

	private static final String RESTEASY_DEFAULT_CHARSET_PROPERTY = "resteasy.provider.multipart.inputpart.defaultCharset";

	public Object aroundReadFrom(ReaderInterceptorContext ctx) throws IOException, WebApplicationException {
		ctx.setProperty(RESTEASY_DEFAULT_CHARSET_PROPERTY, "UTF-8");
		return ctx.proceed();
	}
}
