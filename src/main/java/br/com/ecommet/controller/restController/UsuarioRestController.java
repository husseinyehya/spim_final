package br.com.ecommet.controller.restController;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.SimpleEmail;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.auth0.jwt.JWTSigner;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.JWTVerifyException;

import br.com.ecommet.model.Administrador;
import br.com.ecommet.model.Colaborador;
import br.com.ecommet.model.Consumidor;
import br.com.ecommet.model.TipoAdministrador;
import br.com.ecommet.model.TokenJWT;
import br.com.ecommet.model.Usuario;
import br.com.ecommet.repositorio.UsuarioRepositorio;

@Path("/usuario")
public class UsuarioRestController {

	@EJB
	private UsuarioRepositorio repositorio;

	public static final String EMISSOR = "ECOMMET";
	public static final String SECRET = "3C0MM37";

	@Inject
	private ServletContext context;

	//
	// Alterar uma imagem
	//
	@POST
	@Path("/")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response alterarImagem(@MultipartForm MultipartFormDataInput input) {

		String fileName = "";
		criarPasta();
		String token = null;
		try {
			token = input.getFormDataMap().get("token").get(0).getBodyAsString();

		} catch (IOException e1) {
			e1.printStackTrace();
		}
		System.out.println(token);
		if (token == null) {
			return null;
		} else {
			JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
			Map<String, Object> claims = null;

			try {
				claims = verifier.verify(token);
			} catch (Exception e) {
				e.printStackTrace();
			}
			String idString = claims.get("id_usuario").toString();
			Long idUsuario = Long.valueOf(idString);
			System.out.println(idUsuario);

			Usuario usuario = repositorio.buscar(idUsuario);

			String raiz = context.getRealPath("/");
			System.out.println("--------------RAIZ: " + raiz);

			Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
			List<InputPart> inputParts = uploadForm.get("fotoPerfil");
			for (InputPart inputPart : inputParts) {

				try {

					System.out.println("Nome: " + inputPart);
					System.out.println("Imagens: " + inputParts);
					MultivaluedMap<String, String> header = inputPart.getHeaders();
					fileName = getFileName(header);

					// convert the uploaded file to inputstream
					InputStream inputStream = inputPart.getBody(InputStream.class, null);

					byte[] bytes = IOUtils.toByteArray(inputStream);

					// constructs upload file path
					boolean arquivoVazio = fileName.isEmpty();
					if (!arquivoVazio) {
						if (!fileName.equals("unknown")) {
							if (fileName.endsWith(".jpeg") || fileName.endsWith(".jpg") || fileName.endsWith(".png")) {

								salvarImagem(bytes, raiz, fileName, usuario);

							} else {
								System.out.println("FORMATO INV�LIDO");
								return Response.status(Response.Status.UNSUPPORTED_MEDIA_TYPE).build();
							}
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			repositorio.alterar(usuario);
			return Response.ok().build();

		}

	}

	//
	// Lista todos os usuarios ativos
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/listar")
	public List<Usuario> listar() {
		return repositorio.usuarios();
	}
	
	
	//
	// Lista todos os usuarios ativos
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/listar/{tipo}")
	public List<Object> listar(@PathParam("tipo") int tipo) {
		return repositorio.usuariosAtivos(tipo);
	}
	
	//
	// Lista todos os produtos especifico
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/buscar/{nome}/{tipo}")
	public List<Usuario> listarEspecifico(@PathParam("nome") String nome, @PathParam("tipo") int tipo) {

		return repositorio.buscarPorUsuario(nome, tipo);
	}

	//
	// desativa um usuario
	//
	@PUT
	@Path("/desativar/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response desativarProduto(@PathParam("id") Long id) {

		repositorio.desativar(id);

		System.out.println("usuario DESATIVADO");
		return Response.ok().build();
	}

	//
	// Lista todos os usuarios inativos
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/inativos/{tipo}")
	public List<Usuario> listarInativos(@PathParam("tipo") int tipo) {
		return repositorio.usuariosInativos(tipo);
	}

	//
	// Busca um determinado Usuario
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Usuario buscar(@PathParam("id") Long id) {

		return repositorio.buscar(id);

	}

	//
	// Busca um determinado Usuario
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/token")
	public Usuario buscar(@Context HttpServletRequest req) throws InvalidKeyException, NoSuchAlgorithmException,
			IllegalStateException, SignatureException, IOException, JWTVerifyException {
		String token = req.getHeader("Authorization");
		if (token == null) {
			return null;
		} else {
			JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
			Map<String, Object> claims = verifier.verify(token);
			String idString = claims.get("id_usuario").toString();
			Long id = Long.valueOf(idString);
			System.out.println("ID DO USU�RIO: " + id);
			return repositorio.buscar(id);
		}

	}

	//
	// Altera um determinado Usuario
	//
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/")
	public Response alterar(@Context HttpServletRequest req, Usuario usuario) throws NoSuchAlgorithmException,
			InvalidKeyException, IllegalStateException, SignatureException, IOException, JWTVerifyException {

		String token = req.getHeader("Authorization");
		if (token == null) {
			return null;
		} else {
			JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
			Map<String, Object> claims = verifier.verify(token);
			String idString = claims.get("id_usuario").toString();
			Long id = Long.valueOf(idString);
			System.out.println("ID DO USU�RIO: " + id);
			Usuario buscado = repositorio.buscar(id);

			System.out.println("NOME -------------> " + usuario.getNome());
			String senha = buscado.getSenha();
			System.out.println("senha buscada-------------------- alterar" + senha);
			buscado.setNome(usuario.getNome());

			System.out.println("NOME -------------> " + buscado.getNome());
			buscado.setSobrenome(usuario.getSobrenome());
			buscado.setCpf(usuario.getCpf());
			buscado.setEmail(usuario.getEmail());
			buscado.setTelefone(usuario.getTelefone());
			buscado.setCelular(usuario.getCelular());
			buscado.setConta(usuario.getConta());
			System.out.println("senha -------------------------- alterar: " + buscado.getSenha());
			verificarCpf(usuario);
			repositorio.alterar(buscado);

			return Response.ok().build();
		}
	}

	//
	// Altera um determinado Usuario
	//
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response alterar(Usuario usuario, @PathParam("id") Long id) {

		Usuario buscado = repositorio.buscar(id);

		System.out.println("NOME -------------> " + usuario.getNome());
		String senha = buscado.getSenha();
		System.out.println("senha buscada-------------------- alterar" + senha);
		buscado.setNome(usuario.getNome());

		System.out.println("NOME -------------> " + buscado.getNome());
		buscado.setSobrenome(usuario.getSobrenome());
		buscado.setCpf(usuario.getCpf());
		buscado.setEmail(usuario.getEmail());
		buscado.setTelefone(usuario.getTelefone());
		buscado.setCelular(usuario.getCelular());
		buscado.setConta(usuario.getConta());
		System.out.println("senha -------------------------- alterar: " + buscado.getSenha());
		// verificarCpf(usuario);
		repositorio.alterar(buscado);

		return Response.ok().build();

	}

	//
	// Altera um determinado Usuario
	//
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/alterarSenha/{senhaAntiga}")
	public Response alterarSenha(@Context HttpServletRequest req, Usuario usuario, @PathParam("senhaAntiga") String senhaAntiga) throws NoSuchAlgorithmException,
			InvalidKeyException, IllegalStateException, SignatureException, IOException, JWTVerifyException {

		String token = req.getHeader("Authorization");
		if (token == null) {
			return null;
		} else {
			JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
			Map<String, Object> claims = verifier.verify(token);
			String idString = claims.get("id_usuario").toString();
			Long id = Long.valueOf(idString);
			System.out.println("ID DO USU�RIO: " + id);
			Usuario buscado = repositorio.buscar(id);
			
			senhaAntiga = criptografarSenha(senhaAntiga);
			System.out.println("senha antiga --------" + senhaAntiga);
			if (buscado.getSenha().equals(senhaAntiga)) {
				System.out.println("SENHA ANTIGA : " + senhaAntiga);
				System.out.println("SENHA NOVA : " + usuario.getSenha());
				buscado.setSenha(usuario.getSenha());
				criptografarSenha(buscado);
				repositorio.alterar(buscado);

				return Response.ok().build();
			}else{
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
	
		}
	}

	//
	// Deleta um determinado Usuario
	//
	@DELETE
	@Path("/{id}")
	public Response deletar(@PathParam("id") Long id) {

		repositorio.deletar(id);
		return Response.noContent().build();
	}

	//
	// Insere um novo Usuario
	//
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/login")
	public Response logar(Usuario usuario)
			throws IllegalArgumentException, UnsupportedEncodingException, NoSuchAlgorithmException {

		System.out.println("USUARIO SENHA ANTES DE CADASTRAR S/CRIPTO ------------------" + usuario.getSenha());
		if (usuario.getSenha().length() <= 13) {
			criptografarSenha(usuario);
		}

		Usuario usuarioRetornado = repositorio.logar(usuario);

		System.out.println("USUARIO SENHA DEPOIS DE CADASTRAR C/CRIPTO -------------: " + usuario.getSenha());

		if (usuarioRetornado != null) {
			HashMap<String, Object> claims = new HashMap<String, Object>();
			claims.put("iss", EMISSOR);
			claims.put("id_usuario", usuarioRetornado.getId());
			claims.put("nome_usuario", usuarioRetornado.getNome());
			if (usuarioRetornado instanceof Colaborador) {
				claims.put("tipoUsuario", "Colaborador");
			} else if (usuarioRetornado instanceof Consumidor) {
				claims.put("tipoUsuario", "Consumidor");
			} else if (usuarioRetornado instanceof Administrador) {
				if (((Administrador) usuarioRetornado).getTipoAdministrador() == TipoAdministrador.COMPLETO) {
					claims.put("tipoUsuario", "Completo");
				} else {
					claims.put("tipoUsuario", "Parcial");
				}
			}

			// hora atual em segundos
			long horaAtual = System.currentTimeMillis() / 1000;
			// expira��o do token (3600 segundos)
			long horaExpiracao = horaAtual + 86400;
			claims.put("iat", horaAtual);
			claims.put("exp", horaExpiracao);

			JWTSigner signer = new JWTSigner(SECRET);
			TokenJWT tokenJwt = new TokenJWT();
			tokenJwt.setToken(signer.sign(claims));
			tokenJwt.setUsuario(usuarioRetornado);
			tokenJwt.setTipoUsuario(String.valueOf(claims.get("tipoUsuario")));
			System.out.println("LOGIN COM SUCESSO");
			return Response.ok().entity(tokenJwt).build();

		} else {
			System.out.println("LOGIN FRACASSADO");
			return Response.status(Response.Status.UNAUTHORIZED).build();
		}

	}

	//
	// Gera uma nova senha aleat�ria e manda pro e-mail do usuario
	//
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/recuperarSenha")
	public Response recuperarSenha(Usuario usuario)
			throws IllegalArgumentException, UnsupportedEncodingException, NoSuchAlgorithmException {
		System.out.println("1EMAIL USUARIO: " + usuario.getEmail());
		Usuario usuarioRecuperado = repositorio.recuperarSenha(usuario);
		
		System.out.println("EMAIL USUARIO: " + usuarioRecuperado.getEmail());
		if (usuarioRecuperado.getEmail() != null && usuario.getEmail().equals(usuarioRecuperado.getEmail())) {
			System.out.println("email --------" + usuario.getEmail());
			try {
				enviarEmail(usuarioRecuperado);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return Response.ok().build();
		} else {
			return Response.status(Response.Status.FORBIDDEN).build();
		}

	}

	//
	// M�TODOS COMPLEMENTARES
	//
	public void enviarEmail(final Usuario usuario) {
		new Thread() {
			@Override
			public void run() {
				Random gerador = new Random();
				int numero = gerador.nextInt();
				String senhaNova = String.valueOf(numero);
				usuario.setSenha(senhaNova);

				SimpleEmail email = new SimpleEmail();
				email.setHostName("smtp.gmail.com");
				email.setAuthenticator(new DefaultAuthenticator("spimecommet@gmail.com", "Ecommet#SPIM132"));
				try {
					email.getMailSession().getProperties().put("mail.smtp.socketFactory.port", "465");
					email.getMailSession().getProperties().put("mail.smtps.auth", "true");
					email.getMailSession().getProperties().put("mail.debug", "true");
					email.getMailSession().getProperties().put("mail.smtps.port", "587");
					email.getMailSession().getProperties().put("mail.smtps.socketFactory.port", "587");
					email.getMailSession().getProperties().put("mail.smtps.socketFactory.class",
							"javax.net.ssl.SSLSocketFactory");
					email.getMailSession().getProperties().put("mail.smtps.socketFactory.fallback", "false");
					email.getMailSession().getProperties().put("mail.smtp.starttls.enable", "true");
				} catch (EmailException e1) {
					e1.printStackTrace();
				}
				try {
					email.setFrom("spimecommet@gmail.com", "Ecommet");
					email.setSubject("RECUPERA��O DE SENHA");
					email.setMsg("Esta � sua nova senha: " + senhaNova);
					email.addTo(usuario.getEmail());
					email.send();
					criptografarSenha(usuario);
					repositorio.alterar(usuario);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}.start();

	}

	public void criptografarSenha(Usuario usuario) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String senha = usuario.getSenha();

		MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
		byte messageDigestSenhaAdmin[] = algorithm.digest(senha.getBytes("UTF-8"));

		StringBuilder hexStringSenhaAdmin = new StringBuilder();
		for (byte b : messageDigestSenhaAdmin) {
			hexStringSenhaAdmin.append(String.format("%02X", 0xFF & b));
		}
		String senhahex = hexStringSenhaAdmin.toString();
		usuario.setSenha(senhahex);
		System.out.println("senha: " + senhahex);

	}
	
	public String criptografarSenha(String senha) throws NoSuchAlgorithmException, UnsupportedEncodingException {

		MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
		byte messageDigestSenhaAdmin[] = algorithm.digest(senha.getBytes("UTF-8"));

		StringBuilder hexStringSenhaAdmin = new StringBuilder();
		for (byte b : messageDigestSenhaAdmin) {
			hexStringSenhaAdmin.append(String.format("%02X", 0xFF & b));
		}
		senha = hexStringSenhaAdmin.toString();
		
		return senha;
	}


	public boolean verificarCpf(Usuario usuario) {
		int somaJ, somaK, restoJ, restoK, a, b, c, d, e, f, g, h, i, j, k, j1, k1;

		a = Integer.parseInt(String.valueOf(usuario.getCpf().charAt(0)));
		b = Integer.parseInt(String.valueOf(usuario.getCpf().charAt(1)));
		c = Integer.parseInt(String.valueOf(usuario.getCpf().charAt(2)));
		d = Integer.parseInt(String.valueOf(usuario.getCpf().charAt(4)));
		e = Integer.parseInt(String.valueOf(usuario.getCpf().charAt(5)));
		f = Integer.parseInt(String.valueOf(usuario.getCpf().charAt(6)));
		g = Integer.parseInt(String.valueOf(usuario.getCpf().charAt(8)));
		h = Integer.parseInt(String.valueOf(usuario.getCpf().charAt(9)));
		i = Integer.parseInt(String.valueOf(usuario.getCpf().charAt(10)));
		j = Integer.parseInt(String.valueOf(usuario.getCpf().charAt(12)));
		k = Integer.parseInt(String.valueOf(usuario.getCpf().charAt(13)));
		j1 = 0;
		k1 = 0;

		somaJ = 10 * a + 9 * b + 8 * c + 7 * d + 6 * e + 5 * f + 4 * g + 3 * h + 2 * i;
		restoJ = somaJ % 11;

		somaK = 11 * a + 10 * b + 9 * c + 8 * d + 7 * e + 6 * f + 5 * g + 4 * h + 3 * i + 2 * j;
		restoK = somaK % 11;

		if (restoJ > 1) {
			j1 = 11 - restoJ;
		}

		if (restoK > 1) {
			k1 = 11 - restoK;
		}

		if (j == j1 && k == k1) {
			// System.out.println("\nCPF v�lido.");
			return true;
		} else {
			// System.out.println("\nCPF inv�lido.");
			return false;
		}
	}

	private void writeFile(byte[] content, String filename) throws IOException {

		File file = new File(filename);

		System.out.println("CAMINHO FILE: " + file.getAbsolutePath());
		if (!file.exists()) {
			file.createNewFile();
		}

		FileOutputStream fop = new FileOutputStream(file);

		fop.write(content);
		fop.flush();
		fop.close();

	}

	private void criarPasta() {
		try {
			File file = new File(context.getRealPath("/") + "/usuario");
			file.mkdirs();
			String caminhoCompleto = file.getAbsolutePath();
			System.out.println("CAMINHO COMPLETO: " + caminhoCompleto);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void salvarImagem(byte[] bytes, String raiz, String fileName, Usuario usuario) {
		try {
			Random gerador = new Random();
			int numero = gerador.nextInt();
			String nomeArquivo = String.valueOf(numero) + fileName;

			if (nomeArquivo.startsWith("-")) {
				nomeArquivo = nomeArquivo.replace("-", "").replace(" ", "");
			}

			nomeArquivo = nomeArquivo.replace(" ", "");

			if (nomeArquivo != null) {
				String complemento = "/usuario/" + nomeArquivo;
				if (usuario.getCaminhoImagem() != null) {
					String imagemExistente = usuario.getCaminhoImagem().replace("/", "\\");
					File imagem = new File(raiz + imagemExistente);
					if (imagem != null) {
						imagem.delete();
					}
				}
				usuario.setCaminhoImagem(complemento);
				complemento = complemento.replace("/", "\\");
				fileName = raiz + complemento;
				System.out.println("Caminho: " + fileName);
				System.out.println("Novo caminho: " + fileName);

				writeFile(bytes, fileName);

				System.out.println("Done");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private String getFileName(MultivaluedMap<String, String> header) {

		String[] contentDisposition = header.getFirst("Content-Disposition").split(";");

		for (String filename : contentDisposition) {
			if ((filename.trim().startsWith("filename"))) {

				String[] name = filename.split("=");

				String finalFileName = name[1].trim().replaceAll("\"", "");
				return finalFileName;
			}
		}
		return "unknown";
	}

}
