package br.com.ecommet.controller.restController;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.JWTVerifyException;

import br.com.ecommet.model.Conta;
import br.com.ecommet.model.Usuario;
import br.com.ecommet.repositorio.ColaboradorRepositorio;
import br.com.ecommet.repositorio.ContaRepositorio;
import br.com.ecommet.repositorio.UsuarioRepositorio;

@Path("/conta")
public class ContaRestController {

	@EJB
	private ContaRepositorio repositorio;

	@EJB
	private ColaboradorRepositorio colaboradorRepositorio;

	@EJB
	private UsuarioRepositorio usuarioRepositorio;

	//
	// Insere um novo Conta
	//
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/")
	public Response inserir(Conta conta, @Context HttpServletRequest req) throws InvalidKeyException,
			NoSuchAlgorithmException, IllegalStateException, SignatureException, IOException, JWTVerifyException {

		String token = req.getHeader("Authorization");
		JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
		Map<String, Object> claims = verifier.verify(token);
		String idString = claims.get("id_usuario").toString();
		Long id = Long.valueOf(idString);
		System.out.println("ID DO USU�RIO: " + id);
		boolean cpfOk = false;
		String cpfDigitado = conta.getCpfTitular();
		Conta contaCpf = repositorio.buscarPorCpf(cpfDigitado);

		if (contaCpf == null) {
			cpfOk = true;
		} else {
			return Response.status(Status.NOT_ACCEPTABLE).build();
		}
		
		if (cpfOk) {
			if (verificarCpf(cpfDigitado)) {
				Usuario colaboradorBuscado = usuarioRepositorio.buscar(id);
				repositorio.adicionar(conta);

				colaboradorBuscado.setConta(conta);
				colaboradorRepositorio.alterar(colaboradorBuscado);

				return Response.ok().build();
			}
		}
		
		return Response.status(Status.FORBIDDEN).build();

	}

	//
	// List todos os contas
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/listar")
	public List<Conta> listar() {
		return repositorio.contas();
	}

	//
	// Busca um determinado Conta
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Conta buscar(@PathParam("id") Long id) {
		return repositorio.buscar(id);
	}

	// p
	// Busca um determinado Conta
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/verificar")
	public Conta verificarConta(@Context HttpServletRequest req) throws InvalidKeyException, NoSuchAlgorithmException,
			IllegalStateException, SignatureException, IOException, JWTVerifyException {

		String token = req.getHeader("Authorization");
		JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
		Map<String, Object> claims = verifier.verify(token);
		String idString = claims.get("id_usuario").toString();
		System.out.println("idstrign: " + idString);
		Long id = Long.valueOf(idString);
		System.out.println("id: " + id);

		Usuario colaboradorBuscado = usuarioRepositorio.buscar(id);
		System.out.println(colaboradorBuscado);

		if (colaboradorBuscado.getConta() == null) {
			return null;
		} else {
			return colaboradorBuscado.getConta();
		}
	}

	//
	// Altera um determinado Conta
	//
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response alterar(@PathParam("id") Long id, Conta conta) {
		Conta buscada = repositorio.buscar(id);
		buscada.setAgencia(conta.getAgencia());
		buscada.setBanco(conta.getBanco());
		buscada.setConta(conta.getConta());
		buscada.setCpfTitular(conta.getCpfTitular());
		buscada.setDigito(conta.getDigito());
		buscada.setTitular(conta.getTitular());

		repositorio.alterar(buscada);

		return Response.ok().build();
	}

	//
	// Deleta um determinado Conta
	//
	@DELETE
	@Path("/{id}")
	public Response deletar(@PathParam("id") Long id) {
		repositorio.deletar(id);
		return Response.noContent().build();
	}
	
	public boolean verificarCpf(String cpf) {
		int somaJ, somaK, restoJ, restoK, a, b, c, d, e, f, g, h, i, j, k, j1, k1;

		a = Integer.parseInt(String.valueOf(cpf.charAt(0)));
		b = Integer.parseInt(String.valueOf(cpf.charAt(1)));
		c = Integer.parseInt(String.valueOf(cpf.charAt(2)));
		d = Integer.parseInt(String.valueOf(cpf.charAt(4)));
		e = Integer.parseInt(String.valueOf(cpf.charAt(5)));
		f = Integer.parseInt(String.valueOf(cpf.charAt(6)));
		g = Integer.parseInt(String.valueOf(cpf.charAt(8)));
		h = Integer.parseInt(String.valueOf(cpf.charAt(9)));
		i = Integer.parseInt(String.valueOf(cpf.charAt(10)));
		j = Integer.parseInt(String.valueOf(cpf.charAt(12)));
		k = Integer.parseInt(String.valueOf(cpf.charAt(13)));
		j1 = 0;
		k1 = 0;

		somaJ = 10 * a + 9 * b + 8 * c + 7 * d + 6 * e + 5 * f + 4 * g + 3 * h + 2 * i;
		restoJ = somaJ % 11;

		somaK = 11 * a + 10 * b + 9 * c + 8 * d + 7 * e + 6 * f + 5 * g + 4 * h + 3 * i + 2 * j;
		restoK = somaK % 11;

		if (restoJ > 1) {
			j1 = 11 - restoJ;
		}

		if (restoK > 1) {
			k1 = 11 - restoK;
		}

		if (a == b && b == c && c == d && d == e && e == f && f == g && g == h && h == i && i == j && j == k) {
			System.out.println("\nCPF inv�lido.");
			return false;
		}

		if (j == j1 && k == k1) {
			System.out.println("\nCPF v�lido.");
			return true;
		} else {
			System.out.println("\nCPF inv�lido.");
			return false;
		}
	}

}
