package br.com.ecommet.controller.restController;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.ecommet.model.Categoria;
import br.com.ecommet.repositorio.CategoriaRepositorio;

@Path("/categoria")
public class CategoriaRestController {

	@EJB
	private CategoriaRepositorio repositorio;

	/*
	 * Insere uma nova Categoria
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/")
	public Response inserir(Categoria categoria) {
		repositorio.adicionar(categoria);
		return Response.ok().build();
	}

	/*
	 * Lista categoria por nome especifico
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/buscar/{nome}")
	public List<Categoria> listarEspecifico(@PathParam("nome") String nome) {

		return repositorio.buscarPorCategoria(nome);
	}

	/*
	 * Lista todas as categorias com pai_id igual a null
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/listar")
	public List<Categoria> listar() {

		return repositorio.categorias();
	}

	/*
	 * Lista todas as categorias
	 * 
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/listarTodas")
	public List<Categoria> listarTodas() {

		return repositorio.todasCategorias();
	}

	/*
	 * Lista todas as categorias por Pai
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/filtrar/{pai_id}")
	public List<Categoria> listarPai(@PathParam("pai_id") Long id) {

		return repositorio.categoriasPai(id);
	}

	/*
	 * Busca uma determinada Categoria
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Categoria buscar(@PathParam("id") Long id) {
		return repositorio.buscar(id);
	}

	/*
	 * Altera uma determinada Categoria
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response alterar(@PathParam("id") Long id, Categoria categoria) {
		Categoria buscada = repositorio.buscar(id);
		buscada.setNome(categoria.getNome());
		buscada.setPai(categoria.getPai());

		repositorio.alterar(buscada);
		return Response.ok().build();
	}

	/*
	 * Deleta uma determinada Categoria
	 */
	@DELETE
	@Path("/{id}")
	public Response deletar(@PathParam("id") Long id) {

		repositorio.deletar(id);
		return Response.noContent().build();
	}

}
