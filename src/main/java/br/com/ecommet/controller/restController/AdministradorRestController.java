package br.com.ecommet.controller.restController;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.JWTVerifyException;

import br.com.ecommet.model.Administrador;
import br.com.ecommet.model.TipoAdministrador;
import br.com.ecommet.model.Usuario;
import br.com.ecommet.repositorio.AdministradorRepositorio;
import br.com.ecommet.repositorio.UsuarioRepositorio;

@Path("/administrador")
public class AdministradorRestController {

	@EJB
	private AdministradorRepositorio repositorio;

	@EJB
	private UsuarioRepositorio usuarioRepositorio;

	public static final String EMISSOR = "ECOMMET";
	public static final String SECRET = "3C0MM37";

	@Inject
	private ServletContext context;

	private String inputPart;

	/*
	 * Lista todos os administradores
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/listar")
	public List<Administrador> listar() {
		return repositorio.administradores();
	}

	/*
	 * Busca um determinado administrador por ID
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Administrador buscar(@PathParam("id") Long id) {

		return repositorio.buscar(id);
	}

	/*
	 * Cadastra um administrador
	 */
	@POST
	@Path("/")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response inserir(@MultipartForm MultipartFormDataInput input) throws NoSuchAlgorithmException {
		Administrador administrador = new Administrador();
		String fileName = "";
		criarPasta();
		boolean emailOk = false;
		boolean cpfOk = false;
		try {
			inputPart = input.getFormDataMap().get("nome").get(0).getBodyAsString();
			administrador.setNome(inputPart.toString());

			inputPart = input.getFormDataMap().get("sobrenome").get(0).getBodyAsString();
			if (!inputPart.isEmpty()) {
				administrador.setSobrenome(inputPart.toString());
			} else {
				administrador.setSobrenome(null);
			}

			inputPart = input.getFormDataMap().get("cpf").get(0).getBodyAsString();
			if (!inputPart.isEmpty()) {
				administrador.setCpf(inputPart.toString());
			} else {
				administrador.setCpf(null);
			}

			// VERIFICA SE O E-MAIL J� EXISTE NO BD

			String cpfDigitado = administrador.getCpf();

			Usuario administradorCpf = usuarioRepositorio.buscarPorCpf(cpfDigitado);

			if (administradorCpf == null) {
				cpfOk = true;
			} else {
				return Response.status(Status.NOT_ACCEPTABLE).build();
			}

			inputPart = input.getFormDataMap().get("email").get(0).getBodyAsString();
			if (!inputPart.isEmpty()) {
				administrador.setEmail(inputPart.toString());
			} else {
				administrador.setEmail(null);
			}

			String emailDigitado = administrador.getEmail();
			Usuario administradorEmail = usuarioRepositorio.buscarPorEmail(emailDigitado);

			if (administradorEmail == null) {
				System.out.println("email --------------- " + administradorEmail);
				emailOk = true;
			} else {
				return Response.status(Status.UNAUTHORIZED).build();
			}

			inputPart = input.getFormDataMap().get("telefone").get(0).getBodyAsString();
			if (!inputPart.isEmpty()) {
				administrador.setTelefone(inputPart.toString());
			} else {
				administrador.setTelefone(null);
			}

			inputPart = input.getFormDataMap().get("celular").get(0).getBodyAsString();
			if (!inputPart.isEmpty()) {
				administrador.setCelular(inputPart.toString());
			} else {
				administrador.setCelular(null);
			}

			inputPart = input.getFormDataMap().get("senha").get(0).getBodyAsString();
			if (!inputPart.isEmpty()) {
				administrador.setSenha(inputPart.toString());
				criptografarSenha(administrador);
			} else {
				administrador.setSenha(null);
			}

			inputPart = input.getFormDataMap().get("tipoAdministrador").get(0).getBodyAsString();
			if (!inputPart.isEmpty()) {
				TipoAdministrador tipoAdministrador = null;
				if (inputPart.equals("parcial")) {
					tipoAdministrador = TipoAdministrador.PARCIAL;
				} else if (inputPart.equals("completo")) {
					tipoAdministrador = TipoAdministrador.COMPLETO;
				}

				administrador.setTipoAdministrador(tipoAdministrador);
			} else {
				administrador.setTipoAdministrador(null);
			}

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");

			try {
				administrador.setDataCadastro(sdf.parse(sdf.format(new Date())));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (IOException e1) {
			e1.printStackTrace();
		}

		String raiz = context.getRealPath("/");
		System.out.println("--------------RAIZ: " + raiz);

		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		List<InputPart> inputParts = uploadForm.get("fotoPerfil");

		for (InputPart inputPart : inputParts) {
			try {
				MultivaluedMap<String, String> header = inputPart.getHeaders();
				fileName = getFileName(header);

				// converte o uploaded file para inputstream
				InputStream inputStream = inputPart.getBody(InputStream.class, null);

				byte[] bytes = IOUtils.toByteArray(inputStream);

				// constructs upload file path
				boolean arquivoVazio = fileName.isEmpty();
				if (!arquivoVazio) {
					if (!fileName.equals("unknown")) {

						if (fileName.endsWith(".jpeg") || fileName.endsWith(".jpg") || fileName.endsWith(".png")) {

							salvarImagem(bytes, raiz, fileName, administrador);

						} else {
							System.out.println("FORMATO INV�LIDO: " + fileName);
							return Response.status(Response.Status.UNSUPPORTED_MEDIA_TYPE).build();
						}

					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (emailOk == true && cpfOk == true) {
			if (verificarCpf(administrador.getCpf())) {
				repositorio.adicionar(administrador);
				return Response.ok().build();
			}
		}
		return Response.status(Status.FORBIDDEN).build();
	}

	/*
	 * Altera um determinado administrador
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response alterar(@Context HttpServletRequest req, Administrador administrador)
			throws NoSuchAlgorithmException, InvalidKeyException, IllegalStateException, SignatureException,
			IOException, JWTVerifyException {

		String token = req.getHeader("Authorization");
		if (token == null) {
			return null;
		} else {
			JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
			Map<String, Object> claims = verifier.verify(token);
			String idString = claims.get("id_usuario").toString();
			Long id = Long.valueOf(idString);
			System.out.println("ID DO USU�RIO: " + id);

			Administrador buscado = repositorio.buscar(id);
			buscado.setNome(administrador.getNome());
			buscado.setSobrenome(administrador.getSobrenome());
			buscado.setCpf(administrador.getCpf());
			buscado.setEmail(administrador.getEmail());
			buscado.setTelefone(administrador.getTelefone());
			buscado.setCelular(administrador.getCelular());
			buscado.setConta(administrador.getConta());

			repositorio.alterar(buscado);
			return Response.ok().build();
		}
	}

	/*
	 * Deleta um administradore
	 */
	@DELETE
	@Path("/{id}")
	public Response deletar(@PathParam("id") Long id) {

		repositorio.deletar(id);
		return Response.noContent().build();
	}

	/*
	 * MET�DOS COMPLEMENTARES
	 */
	public void criptografarSenha(Usuario usuario) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String senha = usuario.getSenha();

		MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
		byte messageDigestSenhaAdmin[] = algorithm.digest(senha.getBytes("UTF-8"));

		StringBuilder hexStringSenhaAdmin = new StringBuilder();
		for (byte b : messageDigestSenhaAdmin) {
			hexStringSenhaAdmin.append(String.format("%02X", 0xFF & b));
		}
		String senhahex = hexStringSenhaAdmin.toString();
		usuario.setSenha(senhahex);
		System.out.println("senha: " + senhahex);
	}

	private void salvarImagem(byte[] bytes, String raiz, String fileName, Usuario usuario) {
		try {
			Random gerador = new Random();
			int numero = gerador.nextInt();
			String nomeArquivo = String.valueOf(numero) + fileName;

			if (nomeArquivo.startsWith("-")) {
				nomeArquivo = nomeArquivo.replace("-", "").replace(" ", "");
			}

			nomeArquivo = nomeArquivo.replace(" ", "");

			if (nomeArquivo != null) {
				String complemento = "/usuario/" + nomeArquivo;
				if (usuario.getCaminhoImagem() != null) {
					String imagemExistente = usuario.getCaminhoImagem().replace("/", "\\");
					File imagem = new File(raiz + imagemExistente);
					if (imagem != null) {
						imagem.delete();
					}
				}
				usuario.setCaminhoImagem(complemento);
				complemento = complemento.replace("/", "\\");
				fileName = raiz + complemento;
				System.out.println("Caminho: " + fileName);
				System.out.println("Novo caminho: " + fileName);

				writeFile(bytes, fileName);

				System.out.println("Done");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private String getFileName(MultivaluedMap<String, String> header) {

		String[] contentDisposition = header.getFirst("Content-Disposition").split(";");

		for (String filename : contentDisposition) {
			if ((filename.trim().startsWith("filename"))) {

				String[] name = filename.split("=");

				String finalFileName = name[1].trim().replaceAll("\"", "");
				return finalFileName;
			}
		}
		return "unknown";
	}

	private void writeFile(byte[] content, String filename) throws IOException {

		File file = new File(filename);

		System.out.println("CAMINHO FILE: " + file.getAbsolutePath());
		if (!file.exists()) {
			file.createNewFile();
		}

		FileOutputStream fop = new FileOutputStream(file);

		fop.write(content);
		fop.flush();
		fop.close();

	}

	public boolean verificarCpf(String cpf) {
		int somaJ, somaK, restoJ, restoK, a, b, c, d, e, f, g, h, i, j, k, j1, k1;

		a = Integer.parseInt(String.valueOf(cpf.charAt(0)));
		b = Integer.parseInt(String.valueOf(cpf.charAt(1)));
		c = Integer.parseInt(String.valueOf(cpf.charAt(2)));
		d = Integer.parseInt(String.valueOf(cpf.charAt(4)));
		e = Integer.parseInt(String.valueOf(cpf.charAt(5)));
		f = Integer.parseInt(String.valueOf(cpf.charAt(6)));
		g = Integer.parseInt(String.valueOf(cpf.charAt(8)));
		h = Integer.parseInt(String.valueOf(cpf.charAt(9)));
		i = Integer.parseInt(String.valueOf(cpf.charAt(10)));
		j = Integer.parseInt(String.valueOf(cpf.charAt(12)));
		k = Integer.parseInt(String.valueOf(cpf.charAt(13)));
		j1 = 0;
		k1 = 0;

		somaJ = 10 * a + 9 * b + 8 * c + 7 * d + 6 * e + 5 * f + 4 * g + 3 * h + 2 * i;
		restoJ = somaJ % 11;

		somaK = 11 * a + 10 * b + 9 * c + 8 * d + 7 * e + 6 * f + 5 * g + 4 * h + 3 * i + 2 * j;
		restoK = somaK % 11;

		if (restoJ > 1) {
			j1 = 11 - restoJ;
		}

		if (restoK > 1) {
			k1 = 11 - restoK;
		}

		if (a == b && b == c && c == d && d == e && e == f && f == g && g == h && h == i && i == j && j == k) {
			System.out.println("\nCPF inv�lido.");
			return false;
		}

		if (j == j1 && k == k1) {
			System.out.println("\nCPF v�lido.");
			return true;
		} else {
			System.out.println("\nCPF inv�lido.");
			return false;
		}
	}

	private void criarPasta() {
		try {
			File file = new File(context.getRealPath("/") + "/usuario");
			file.mkdirs();
			String caminhoCompleto = file.getAbsolutePath();
			System.out.println("CAMINHO COMPLETO: " + caminhoCompleto);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
