package br.com.ecommet.controller.restController;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.JWTVerifyException;

import br.com.ecommet.model.Colaborador;
import br.com.ecommet.model.Usuario;
import br.com.ecommet.repositorio.ColaboradorRepositorio;
import br.com.ecommet.repositorio.ContaRepositorio;
import br.com.ecommet.repositorio.UsuarioRepositorio;

@Path("/colaborador")
public class ColaboradorRestController {

	@EJB
	private ColaboradorRepositorio repositorio;

	@EJB
	private UsuarioRepositorio repositorioUsuario;

	@EJB
	private ContaRepositorio contaRepositorio;

	@Inject
	private ServletContext context;

	public static final String EMISSOR = "ECOMMET";
	public static final String SECRET = "3C0MM37";

	private String inputPart;

	/*
	 * Alterar uma imagem do colaborador
	 */
	@POST
	@Path("/")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response alterarImagem(@MultipartForm MultipartFormDataInput input) {
		criarPasta();
		String fileName = "";

		String token = null;
		try {
			token = input.getFormDataMap().get("token").get(0).getBodyAsString();

		} catch (IOException e1) {
			e1.printStackTrace();
		}
		System.out.println(token);
		if (token == null) {
			return null;
		} else {
			JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
			Map<String, Object> claims = null;

			try {
				claims = verifier.verify(token);
			} catch (Exception e) {
				e.printStackTrace();
			}
			String idString = claims.get("id_usuario").toString();
			Long idUsuario = Long.valueOf(idString);
			System.out.println(idUsuario);

			Usuario colaborador = repositorio.buscar(idUsuario);

			String raiz = context.getRealPath("/");
			System.out.println("--------------RAIZ: " + raiz);

			Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
			List<InputPart> inputParts = uploadForm.get("fotoPerfil");
			for (InputPart inputPart : inputParts) {

				try {

					System.out.println("Nome: " + inputPart);
					System.out.println("Imagens: " + inputParts);
					MultivaluedMap<String, String> header = inputPart.getHeaders();
					fileName = getFileName(header);

					// convert the uploaded file to inputstream
					InputStream inputStream = inputPart.getBody(InputStream.class, null);

					byte[] bytes = IOUtils.toByteArray(inputStream);

					// constructs upload file path
					boolean arquivoVazio = fileName.isEmpty();
					if (!arquivoVazio) {
						if (!fileName.equals("unknown")) {
							if (fileName.endsWith(".jpeg") || fileName.endsWith(".jpg") || fileName.endsWith(".png")) {

								salvarImagem(bytes, raiz, fileName, colaborador);

							} else {
								System.out.println("FORMATO INV�LIDO");
								return Response.status(Response.Status.UNSUPPORTED_MEDIA_TYPE).build();
							}
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			repositorio.alterar(colaborador);
			return Response.ok().build();

		}

	}

	/*
	 * Insere um novo Colaborador
	 */
	@SuppressWarnings("")
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/mobile")
	public Response inserirMobile(Colaborador colaborador)
			throws UnsupportedEncodingException, NoSuchAlgorithmException {
		criarPasta();

		System.out.println("senha: " + colaborador.getSenha());
		criptografarSenha(colaborador);

		String imagemPerfil = colaborador.getCaminhoImagem().replaceAll("\\s+", "");

		System.out.println("IMAGEM PERFIL: " + imagemPerfil);
		String raiz = context.getRealPath("/");

		if (imagemPerfil != null) {
			salvarImagemAndroid(imagemPerfil, raiz, colaborador);
		}

		// VERIFICA SE O E-MAIL J� EXISTE NO BD
		String emailDigitado = colaborador.getEmail();
		String cpfDigitado = colaborador.getCpf();
		Usuario colaboradorEmail = repositorioUsuario.buscarPorEmail(emailDigitado);
		Usuario colaboradorCpf = repositorioUsuario.buscarPorCpf(cpfDigitado);
		boolean emailOk = false;
		boolean cpfOk = false;

		if (colaboradorCpf == null) {
			cpfOk = true;
		} else {
			return Response.status(Status.NOT_ACCEPTABLE).build();
		}

		if (colaboradorEmail == null) {
			System.out.println("email --------------- " + colaboradorEmail);
			emailOk = true;
		} else {
			return Response.status(Status.UNAUTHORIZED).build();
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");

		try {
			colaborador.setDataCadastro(sdf.parse(sdf.format(new Date())));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (emailOk == true && cpfOk == true) {
			if (verificarCpf(colaborador.getCpf())) {
				repositorio.adicionar(colaborador);
				return Response.ok().build();
			}
		}
		return Response.status(Status.FORBIDDEN).build();

	}

	/*
	 * Insere um novo Produto com imagem
	 */
	@POST
	@Path("/inserir")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Produces(MediaType.APPLICATION_JSON)
	public Response inserir(@MultipartForm MultipartFormDataInput input) throws NoSuchAlgorithmException {
		Colaborador colaborador = new Colaborador();
		String fileName = "";
		criarPasta();
		boolean cpfOk = false;
		boolean emailOk = false;

		try {
			inputPart = input.getFormDataMap().get("nome").get(0).getBodyAsString();
			colaborador.setNome(inputPart.toString());

			inputPart = input.getFormDataMap().get("sobrenome").get(0).getBodyAsString();
			if (!inputPart.isEmpty()) {
				colaborador.setSobrenome(inputPart.toString());
			} else {
				colaborador.setSobrenome(null);
			}

			inputPart = input.getFormDataMap().get("cpf").get(0).getBodyAsString();
			if (!inputPart.isEmpty()) {
				colaborador.setCpf(inputPart.toString());
			} else {
				colaborador.setCpf(null);
			}
			// VERIFICA SE O E-MAIL J� EXISTE NO BD

			String cpfDigitado = colaborador.getCpf();

			Usuario colaboradorCpf = repositorioUsuario.buscarPorCpf(cpfDigitado);

			if (colaboradorCpf == null) {
				cpfOk = true;
			} else {
				return Response.status(Status.NOT_ACCEPTABLE).build();
			}

			inputPart = input.getFormDataMap().get("email").get(0).getBodyAsString();
			if (!inputPart.isEmpty()) {
				colaborador.setEmail(inputPart.toString());
			} else {
				colaborador.setEmail(null);
			}
			String emailDigitado = colaborador.getEmail();
			Usuario colaboradorEmail = repositorioUsuario.buscarPorEmail(emailDigitado);

			if (colaboradorEmail == null) {
				System.out.println("email --------------- " + colaboradorEmail);
				emailOk = true;
			} else {
				return Response.status(Status.UNAUTHORIZED).build();
			}

			inputPart = input.getFormDataMap().get("telefone").get(0).getBodyAsString();
			if (!inputPart.isEmpty()) {
				colaborador.setTelefone(inputPart.toString());
			} else {
				colaborador.setTelefone(null);
			}

			inputPart = input.getFormDataMap().get("celular").get(0).getBodyAsString();
			if (!inputPart.isEmpty()) {
				colaborador.setCelular(inputPart.toString());
			} else {
				colaborador.setCelular(null);
			}

			inputPart = input.getFormDataMap().get("senha").get(0).getBodyAsString();
			if (!inputPart.isEmpty()) {
				colaborador.setSenha(inputPart.toString());
				criptografarSenha(colaborador);
			} else {
				colaborador.setSenha(null);
			}
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");

			try {
				colaborador.setDataCadastro(sdf.parse(sdf.format(new Date())));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (IOException e1) {
			e1.printStackTrace();
		}

		String raiz = context.getRealPath("/");
		System.out.println("--------------RAIZ: " + raiz);

		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		List<InputPart> inputParts = uploadForm.get("fotoPerfil");
		for (InputPart inputPart : inputParts) {
			try {
				MultivaluedMap<String, String> header = inputPart.getHeaders();
				fileName = getFileName(header);

				// convert the uploaded file to inputstream
				InputStream inputStream = inputPart.getBody(InputStream.class, null);

				byte[] bytes = IOUtils.toByteArray(inputStream);

				// constructs upload file path
				boolean arquivoVazio = fileName.isEmpty();
				if (!arquivoVazio) {
					if (!fileName.equals("unknown")) {
						if (fileName.endsWith(".jpeg") || fileName.endsWith(".jpg") || fileName.endsWith(".png")) {

							salvarImagem(bytes, raiz, fileName, colaborador);

						} else {
							System.out.println("FORMATO INV�LIDO");
							return Response.status(Response.Status.UNSUPPORTED_MEDIA_TYPE).build();
						}
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		if (emailOk == true && cpfOk == true) {
			if (verificarCpf(colaborador.getCpf())) {
				repositorio.adicionar(colaborador);
				return Response.ok().build();
			}
		}

		return Response.status(Status.FORBIDDEN).build();

	}

	/*
	 * Altera uma imagem
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/mobile/{id}")
	public Response alterarImagemAndroid(@PathParam("id") Long id, String imagemBase64) {
		imagemBase64 = imagemBase64.replaceAll("\\s+", "");
		Colaborador colaborador = repositorio.buscar(id);

		alterarImagem(imagemBase64, colaborador);
		repositorio.alterar(colaborador);

		return Response.ok().build();
	}

	public void alterarImagem(String imagemBase64, Colaborador colaborador) {

		if (!imagemBase64.isEmpty()) {
			try {
				String raiz = context.getRealPath("/");

				salvarImagemAndroid(imagemBase64, raiz, colaborador);
			} catch (Exception e) {
				// That string wasn't valid.
				System.out.println("ERRO AO DECODIFICAR IMAGEM: " + e.getMessage());
			}
		} else {
			System.out.println("IMAGEM ANDROID NULA");
		}

	}

	/*
	 * Lista todos os colaboradores
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/listar")
	public List<Colaborador> listar() {
		return repositorio.colaboradores();
	}

	/*
	 * Busca um determinado Colaborador atrav�s do token
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/")
	public Colaborador buscar(@Context HttpServletRequest req) throws InvalidKeyException, NoSuchAlgorithmException,
			IllegalStateException, SignatureException, IOException, JWTVerifyException {
		String token = req.getHeader("Authorization");
		if (token == null) {
			return null;
		} else {
			JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
			Map<String, Object> claims = verifier.verify(token);
			String idString = claims.get("id_usuario").toString();
			Long id = Long.valueOf(idString);
			System.out.println("ID DO USU�RIO: " + id);
			return repositorio.buscar(id);
		}
	}

	/*
	 * Altera um determinado Colaborador
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/")
	public Response alterar(@Context HttpServletRequest req, Colaborador colaborador) throws NoSuchAlgorithmException,
			InvalidKeyException, IllegalStateException, SignatureException, IOException, JWTVerifyException {
		String token = req.getHeader("Authorization");
		System.out.println("token----------- " + token);
		if (token == null) {
			return null;
		} else {
			JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
			Map<String, Object> claims = verifier.verify(token);
			String idString = claims.get("id_usuario").toString();
			Long id = Long.valueOf(idString);
			System.out.println("ID DO USU�RIO: " + id);
			Usuario buscado = repositorioUsuario.buscar(id);
			System.out.println("nome " + buscado.getNome());
			buscado.setNome(colaborador.getNome());
			buscado.setSobrenome(colaborador.getSobrenome());
			buscado.setCpf(colaborador.getCpf());
			buscado.setEmail(colaborador.getEmail());
			buscado.setTelefone(colaborador.getTelefone());
			buscado.setCelular(colaborador.getCelular());
			verificarCpf(colaborador.getCpf());
			repositorio.alterar(buscado);
		}

		return Response.ok().build();

	}

	/*
	 * Deleta um determinado Colaborador
	 */
	@DELETE
	@Path("/{id}")
	public Response deletar(@PathParam("id") Long id) {

		repositorio.deletar(id);
		return Response.noContent().build();
	}

	/*
	 * M�TODOS COMPLEMENTARES
	 */
	public void criptografarSenha(Usuario usuario) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String senha = usuario.getSenha();

		MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
		byte messageDigestSenhaAdmin[] = algorithm.digest(senha.getBytes("UTF-8"));

		StringBuilder hexStringSenhaAdmin = new StringBuilder();
		for (byte b : messageDigestSenhaAdmin) {
			hexStringSenhaAdmin.append(String.format("%02X", 0xFF & b));
		}
		String senhahex = hexStringSenhaAdmin.toString();
		usuario.setSenha(senhahex);
		System.out.println("senha: " + senhahex);
	}

	private void salvarImagemAndroid(String imagemBase64, String raiz, Colaborador colaborador) {
		try {
			Random gerador = new Random();
			int numero = gerador.nextInt();
			String nomeArquivo = String.valueOf(numero) + ".jpg";

			if (nomeArquivo.startsWith("-")) {
				nomeArquivo = nomeArquivo.replace("-", "").replace(" ", "");
			}

			nomeArquivo = nomeArquivo.replace(" ", "");

			if (colaborador.getCaminhoImagem() != null) {
				String imagemExistente = colaborador.getCaminhoImagem().replace("/", "\\");
				File imagem = new File(raiz + imagemExistente);
				if (imagem != null) {
					imagem.delete();
				}
			}

			String complemento = "/usuario/" + nomeArquivo;
			colaborador.setCaminhoImagem(complemento);
			complemento = complemento.replace("/", "\\");

			System.out.println("NOVO CAMINHO IMAGEM: " + colaborador.getCaminhoImagem());
			String fileName = raiz + complemento;
			System.out.println("Caminho: " + fileName);

			byte[] imagemArray = Base64.getDecoder().decode(imagemBase64.getBytes());

			writeFile(imagemArray, fileName);

			System.out.println("Done");

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void writeFile(byte[] content, String filename) throws IOException {
		criarPasta();
		File file = new File(filename);

		System.out.println("CAMINHO FILE: " + file.getAbsolutePath());
		if (!file.exists()) {
			file.createNewFile();
		}

		FileOutputStream fop = new FileOutputStream(file);

		fop.write(content);
		fop.flush();
		fop.close();

	}

	private void criarPasta() {
		try {
			File file = new File(context.getRealPath("/") + "/usuario");
			file.mkdirs();
			String caminhoCompleto = file.getAbsolutePath();
			System.out.println("CAMINHO COMPLETO: " + caminhoCompleto);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void salvarImagem(byte[] bytes, String raiz, String fileName, Usuario usuario) {
		try {
			Random gerador = new Random();
			int numero = gerador.nextInt();
			String nomeArquivo = String.valueOf(numero) + fileName;

			if (nomeArquivo.startsWith("-")) {
				nomeArquivo = nomeArquivo.replace("-", "").replace(" ", "");
			}

			nomeArquivo = nomeArquivo.replace(" ", "");

			if (nomeArquivo != null) {
				String complemento = "/usuario/" + nomeArquivo;
				if (usuario.getCaminhoImagem() != null) {
					String imagemExistente = usuario.getCaminhoImagem().replace("/", "\\");
					File imagem = new File(raiz + imagemExistente);
					if (imagem != null) {
						imagem.delete();
					}
				}
				usuario.setCaminhoImagem(complemento);
				complemento = complemento.replace("/", "\\");
				fileName = raiz + complemento;
				System.out.println("Caminho: " + fileName);
				System.out.println("Novo caminho: " + fileName);

				writeFile(bytes, fileName);

				System.out.println("Done");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private String getFileName(MultivaluedMap<String, String> header) {

		String[] contentDisposition = header.getFirst("Content-Disposition").split(";");

		for (String filename : contentDisposition) {
			if ((filename.trim().startsWith("filename"))) {

				String[] name = filename.split("=");

				String finalFileName = name[1].trim().replaceAll("\"", "");
				return finalFileName;
			}
		}
		return "unknown";
	}

	public boolean verificarCpf(String cpf) {
		int somaJ, somaK, restoJ, restoK, a, b, c, d, e, f, g, h, i, j, k, j1, k1;

		a = Integer.parseInt(String.valueOf(cpf.charAt(0)));
		b = Integer.parseInt(String.valueOf(cpf.charAt(1)));
		c = Integer.parseInt(String.valueOf(cpf.charAt(2)));
		d = Integer.parseInt(String.valueOf(cpf.charAt(4)));
		e = Integer.parseInt(String.valueOf(cpf.charAt(5)));
		f = Integer.parseInt(String.valueOf(cpf.charAt(6)));
		g = Integer.parseInt(String.valueOf(cpf.charAt(8)));
		h = Integer.parseInt(String.valueOf(cpf.charAt(9)));
		i = Integer.parseInt(String.valueOf(cpf.charAt(10)));
		j = Integer.parseInt(String.valueOf(cpf.charAt(12)));
		k = Integer.parseInt(String.valueOf(cpf.charAt(13)));
		j1 = 0;
		k1 = 0;

		somaJ = 10 * a + 9 * b + 8 * c + 7 * d + 6 * e + 5 * f + 4 * g + 3 * h + 2 * i;
		restoJ = somaJ % 11;

		somaK = 11 * a + 10 * b + 9 * c + 8 * d + 7 * e + 6 * f + 5 * g + 4 * h + 3 * i + 2 * j;
		restoK = somaK % 11;

		if (restoJ > 1) {
			j1 = 11 - restoJ;
		}

		if (restoK > 1) {
			k1 = 11 - restoK;
		}

		if (a == b && b == c && c == d && d == e && e == f && f == g && g == h && h == i && i == j && j == k) {
			System.out.println("\nCPF inv�lido.");
			return false;
		}

		if (j == j1 && k == k1) {
			System.out.println("\nCPF v�lido.");
			return true;
		} else {
			System.out.println("\nCPF inv�lido.");
			return false;
		}
	}

	public static String removeTillWord(String input, String word) {
		return input.substring(input.indexOf(word) + 1);
	}

}
