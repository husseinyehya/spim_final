package br.com.ecommet.controller.restController;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.ecommet.model.Banco;
import br.com.ecommet.repositorio.BancoRepositorio;

@Path("/banco")
public class BancoRestController {

	@EJB
	private BancoRepositorio repositorio;

	//
	// Insere um novo Banco
	//
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/")
	public Response inserir(Banco banco) {
		repositorio.adicionar(banco);
		return Response.ok().build();
	}

	//
	// Lista todos os bancos
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/listar")
	public List<Banco> listar() {
		return repositorio.bancos();
	}

	//
	// Busca um determinado Banco
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Banco buscar(@PathParam("id") Long id) {
		return repositorio.buscar(id);
	}

	//
	// Altera um determinado Banco
	//
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response alterar(@PathParam("id") Long id, Banco banco) {
		Banco buscado = repositorio.buscar(id);
		buscado.setCodigo(banco.getCodigo());
		buscado.setNome(banco.getNome());

		repositorio.alterar(buscado);

		return Response.ok().build();
	}

	//
	// Deleta um determinado Banco
	//
	@DELETE
	@Path("/{id}")
	public Response deletar(@PathParam("id") Long id) {
		repositorio.deletar(id);
		return Response.noContent().build();
	}

}
