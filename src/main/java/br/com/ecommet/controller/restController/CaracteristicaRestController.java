package br.com.ecommet.controller.restController;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.auth0.jwt.JWTVerifier;

import br.com.ecommet.model.Administrador;
import br.com.ecommet.model.Atributo;
import br.com.ecommet.model.Caracteristica;
import br.com.ecommet.model.Colaborador;
import br.com.ecommet.model.Imagem;
import br.com.ecommet.model.ListasAvaliar;
import br.com.ecommet.model.MovimentoCredito;
import br.com.ecommet.model.Produto;
import br.com.ecommet.model.TipoAtributo;
import br.com.ecommet.model.TipoMovimento;
import br.com.ecommet.model.Usuario;
import br.com.ecommet.repositorio.AtributoRepositorio;
import br.com.ecommet.repositorio.CaracteristicaRepositorio;
import br.com.ecommet.repositorio.ColaboradorRepositorio;
import br.com.ecommet.repositorio.MovimentoCreditoRepositorio;
import br.com.ecommet.repositorio.ProdutoRepositorio;
import br.com.ecommet.repositorio.UsuarioRepositorio;

@Path("/caracteristica")
public class CaracteristicaRestController {

	@EJB
	private CaracteristicaRepositorio repositorio;

	@EJB
	private AtributoRepositorio atributoRepositorio;

	@EJB
	private ProdutoRepositorio produtoRepositorio;

	@EJB
	private UsuarioRepositorio usuarioRepositorio;

	@EJB
	private ColaboradorRepositorio colaboradorRepositorio;

	@EJB
	private MovimentoCreditoRepositorio movimentoRepositorio;

	@Inject
	private ServletContext context;

	/*
	 * Insere uma nova caracteristica
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/")
	public Response inserir(@Context HttpServletRequest req, Caracteristica caracteristica) {
		String token = req.getHeader("Authorization");
		if (token == null) {
			return null;
		} else {
			JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
			Map<String, Object> claims = null;

			try {
				claims = verifier.verify(token);
			} catch (Exception e) {
				e.printStackTrace();
			}
			String idString = claims.get("id_usuario").toString();
			Long idUsuario = Long.valueOf(idString);

			Colaborador colaboradorPreenchedor = colaboradorRepositorio.buscar(idUsuario);

			Long idProduto = caracteristica.getProduto().getId();
			Long idAtributo = caracteristica.getAtributo().getId();

			Caracteristica carac = repositorio.existente(idProduto, idAtributo, colaboradorPreenchedor.getId());

			if (carac == null) {
				caracteristica.setUsuarioPreenchedor(colaboradorPreenchedor);
				repositorio.adicionar(caracteristica);
				return Response.ok().build();
			} else {
				carac.setAvaliada(null);
				carac.setValor(caracteristica.getValor());
				carac.setUsuarioPreenchedor(colaboradorPreenchedor);
				repositorio.alterar(carac);
				return Response.ok().build();
			}
		}

	}

	/*
	 * Insere uma nova Caracteristica e atributo secundario
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/secundario")
	public Response inserirSecundario(@Context HttpServletRequest req, Caracteristica caracteristica) {
		String token = req.getHeader("Authorization");
		if (token == null) {
			return null;
		} else {
			JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
			Map<String, Object> claims = null;

			try {
				claims = verifier.verify(token);
			} catch (Exception e) {
				e.printStackTrace();
			}
			String idString = claims.get("id_usuario").toString();
			Long idUsuario = Long.valueOf(idString);

			Colaborador colaboradorPreenchedor = colaboradorRepositorio.buscar(idUsuario);

			Long idProduto = caracteristica.getProduto().getId();
			Caracteristica carac = repositorio.existenteSecundario(idProduto, caracteristica.getAtributo().getNome());

			if (carac == null) {
				Atributo atributoAdicionado = null;
				System.out.println(caracteristica.getAtributo().getNome());
				Atributo atributoBuscado = atributoRepositorio.atributosPorNome(caracteristica.getAtributo().getNome());

				if (atributoBuscado == null) {
					atributoAdicionado = atributoRepositorio.adicionarSecundario(caracteristica.getAtributo());
				} else {
					atributoAdicionado = atributoRepositorio.atributosPorNome(caracteristica.getAtributo().getNome());
				}

				caracteristica.setAtributo(atributoAdicionado);
				caracteristica.setUsuarioPreenchedor(colaboradorPreenchedor);
				repositorio.adicionar(caracteristica);
				return Response.ok().build();
			} else {
				System.out.println("CARAC JA EXISTE");
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
		}
	}

	/*
	 * Lista todas as caracteristica
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/listar")
	public List<Caracteristica> listar() {

		return repositorio.caracteristicas();
	}

	/*
	 * Busca uma determinada caracteristica
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Caracteristica buscar(@PathParam("id") Long id) {
		return repositorio.buscar(id);
	}

	/*
	 * Busca uma determinada caracteristica por determinado produto
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/filtrar/{produto_id}")
	public List<Caracteristica> listarPai(@PathParam("produto_id") Long id) {

		return repositorio.caracteristicaProdutoTrue(id);
	}

	/*
	 * Lista as caracteristicas com o tipo prim�rios
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/principais/{produto_id}")
	public List<Caracteristica> listarAtributosPrincipais(@PathParam("produto_id") Long id) {

		List<Caracteristica> caracs = repositorio.caracteristicaProdutoTrue(id);
		List<Caracteristica> principais = new ArrayList<Caracteristica>();

		for (Caracteristica caracteristica : caracs) {
			Atributo atributo = atributoRepositorio.buscar(caracteristica.getAtributo().getId());
			if (atributo.getTipoAtributo() == TipoAtributo.PRINCIPAL) {
				principais.add(caracteristica);
			}
		}

		return principais;
	}

	/*
	 * Lista as caracteristicas com o tipo secund�rios
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/secundarios/{produto_id}")
	public List<Caracteristica> listarAtributosSecundarios(@PathParam("produto_id") Long id) {

		List<Caracteristica> caracs = repositorio.caracteristicaProdutoTrue(id);
		List<Caracteristica> secundarios = new ArrayList<Caracteristica>();

		for (Caracteristica caracteristica : caracs) {
			Atributo atributo = atributoRepositorio.buscar(caracteristica.getAtributo().getId());
			if (atributo.getTipoAtributo() == TipoAtributo.SECUNDARIO) {
				secundarios.add(caracteristica);
			}
		}

		return secundarios;
	}

	/*
	 * Busca uma determinada Caracteristica por Produto que esteja como false
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/filtrar/falso/{tipo}/{produto_id}")
	public List<Caracteristica> listarPaiFalse(@PathParam("produto_id") Long id, @PathParam("tipo") int tipo,
			@Context HttpServletRequest req) {
		String token = req.getHeader("Authorization");
		JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
		Map<String, Object> claims = null;

		try {
			claims = verifier.verify(token);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String idString = claims.get("id_usuario").toString();
		Long idUsuario = Long.valueOf(idString);

		return repositorio.caracteristicaProdutoFalse(id, idUsuario, tipo);
	}

	/*
	 * Busca uma determinada Caracteristica por Produto n�o avaliado
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/filtrar/pendentes/{produto_id}")
	public List<Caracteristica> listarN�oAvaliado(@PathParam("produto_id") Long id) {

		return repositorio.caracteristicaNaoAvaliada(id);
	}

	/*
	 * Avalia as caracteristicas do produto
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/avaliar/{idProduto}")
	public Response avaliarListCaracteristica(@PathParam("idProduto") Long idProduto, @Context HttpServletRequest req,
			ListasAvaliar listas) {

		List<Caracteristica> caracteristicas = listas.getCaracteristicas();
		System.out.println("TAMANHO carcs: " + caracteristicas.size());
		List<Imagem> imagensProduto = listas.getImagens();
		System.out.println("TAMANHO IMAGENS: " + imagensProduto.size());

		for (Imagem imagem : imagensProduto) {
			System.out.println("IN�CIO - AVALIAR IAMGEM: " + imagem.isAvaliada());
		}

		String token = req.getHeader("Authorization");
		if (token == null) {
			return null;
		} else {
			JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
			Map<String, Object> claims = null;

			try {
				claims = verifier.verify(token);
			} catch (Exception e) {
				e.printStackTrace();
			}
			String idString = claims.get("id_usuario").toString();
			Long idUsuario = Long.valueOf(idString);

			Usuario usuarioBuscado = usuarioRepositorio.buscar(idUsuario);
			System.out.println("ID PRODUTO: " + idProduto);
			Produto produtoBuscado = produtoRepositorio.buscar(idProduto);

			if (!(usuarioBuscado instanceof Administrador)) {
				for (Caracteristica caracteristica : caracteristicas) {
					BigDecimal remuneracao = produtoBuscado.getRemuneracaoPorAtributo();
					if (caracteristica.isAvaliada() != null && caracteristica.isAvaliada() == true) {

						// avaliador
						Colaborador usuarioAvaliador = colaboradorRepositorio.buscar(idUsuario);
						caracteristica.setUsuarioAvaliador(usuarioAvaliador);
						BigDecimal creditoColaboradorAvaliador = usuarioAvaliador.getCredito();
						creditoColaboradorAvaliador = creditoColaboradorAvaliador.add(remuneracao);
						usuarioAvaliador.setCredito(creditoColaboradorAvaliador);
						colaboradorRepositorio.alterar(usuarioAvaliador);
						MovimentoCredito movimentoAvaliador = new MovimentoCredito();
						movimentoAvaliador.setEcoins(remuneracao);
						movimentoAvaliador.setPendente(false);
						movimentoAvaliador.setTipoMovimento(TipoMovimento.ENTRADA);
						movimentoAvaliador.setUsuario(usuarioAvaliador);
						try {
							SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
							movimentoAvaliador.setDataAcao((sdf.parse(sdf.format(new Date()))));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						movimentoRepositorio.adicionar(movimentoAvaliador);

						// preenchedor
						if (caracteristica.getUsuarioPreenchedor() != null) {
							Colaborador usuarioPreenchedor = colaboradorRepositorio
									.buscar(caracteristica.getUsuarioPreenchedor().getId());
							BigDecimal creditoColaboradorPreenchedor = usuarioPreenchedor.getCredito();
							creditoColaboradorPreenchedor = creditoColaboradorPreenchedor.add(remuneracao);
							usuarioPreenchedor.setCredito(creditoColaboradorPreenchedor);

							if (usuarioPreenchedor.getRanking() < 100) {
								usuarioPreenchedor.setRanking(usuarioPreenchedor.getRanking() + 1);
							}

							colaboradorRepositorio.alterar(usuarioPreenchedor);
							MovimentoCredito movimentoPreenchedor = new MovimentoCredito();
							movimentoPreenchedor.setEcoins(remuneracao);
							movimentoPreenchedor.setPendente(false);
							movimentoPreenchedor.setTipoMovimento(TipoMovimento.ENTRADA);
							movimentoPreenchedor.setUsuario(usuarioPreenchedor);
							try {
								SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
								movimentoPreenchedor.setDataAcao((sdf.parse(sdf.format(new Date()))));
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							movimentoRepositorio.adicionar(movimentoPreenchedor);
						}
					}
				}

				for (Imagem imagem : imagensProduto) {
					BigDecimal remuneracao = produtoBuscado.getRemuneracaoPorAtributo();
					if (imagem.isAvaliada() != null && imagem.isAvaliada() == true) {
						// avaliador
						Colaborador usuarioAvaliador = colaboradorRepositorio.buscar(idUsuario);
						imagem.setUsuarioAvaliador(usuarioAvaliador);
						BigDecimal creditoColaboradorAvaliador = usuarioAvaliador.getCredito();
						creditoColaboradorAvaliador = creditoColaboradorAvaliador.add(remuneracao);
						usuarioAvaliador.setCredito(creditoColaboradorAvaliador);
						colaboradorRepositorio.alterar(usuarioAvaliador);
						MovimentoCredito movimentoAvaliador = new MovimentoCredito();
						movimentoAvaliador.setEcoins(remuneracao);
						movimentoAvaliador.setPendente(false);
						movimentoAvaliador.setTipoMovimento(TipoMovimento.ENTRADA);
						movimentoAvaliador.setUsuario(usuarioAvaliador);
						try {
							SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
							movimentoAvaliador.setDataAcao((sdf.parse(sdf.format(new Date()))));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						movimentoRepositorio.adicionar(movimentoAvaliador);

						// preenchedor
						if (imagem.getUsuarioPreenchedor() != null) {
							Colaborador usuarioPreenchedor = colaboradorRepositorio
									.buscar(imagem.getUsuarioPreenchedor().getId());
							BigDecimal creditoColaboradorPreenchedor = usuarioPreenchedor.getCredito();
							creditoColaboradorPreenchedor = creditoColaboradorPreenchedor.add(remuneracao);
							usuarioPreenchedor.setCredito(creditoColaboradorPreenchedor);
							colaboradorRepositorio.alterar(usuarioPreenchedor);
							MovimentoCredito movimentoPreenchedor = new MovimentoCredito();
							movimentoPreenchedor.setEcoins(remuneracao);
							movimentoPreenchedor.setPendente(false);
							movimentoPreenchedor.setTipoMovimento(TipoMovimento.ENTRADA);
							movimentoPreenchedor.setUsuario(usuarioPreenchedor);
							try {
								SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
								movimentoPreenchedor.setDataAcao((sdf.parse(sdf.format(new Date()))));
							} catch (ParseException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							movimentoRepositorio.adicionar(movimentoPreenchedor);
						}

					}
				}
			} else {

				Usuario usuarioAvaliador = usuarioRepositorio.buscar(idUsuario);
				for (Caracteristica caracteristica : caracteristicas) {
					caracteristica.setUsuarioAvaliador(usuarioAvaliador);
					BigDecimal remuneracao = produtoBuscado.getRemuneracaoPorAtributo();
					// preenchedor
					if (caracteristica.getUsuarioPreenchedor() != null) {
						Colaborador usuarioPreenchedor = colaboradorRepositorio
								.buscar(caracteristica.getUsuarioPreenchedor().getId());
						BigDecimal creditoColaboradorPreenchedor = usuarioPreenchedor.getCredito();
						creditoColaboradorPreenchedor = creditoColaboradorPreenchedor.add(remuneracao);
						usuarioPreenchedor.setCredito(creditoColaboradorPreenchedor);

						if (usuarioPreenchedor.getRanking() < 100) {
							usuarioPreenchedor.setRanking(usuarioPreenchedor.getRanking() + 1);
						}

						colaboradorRepositorio.alterar(usuarioPreenchedor);
						MovimentoCredito movimentoPreenchedor = new MovimentoCredito();
						movimentoPreenchedor.setEcoins(remuneracao);
						movimentoPreenchedor.setPendente(false);
						movimentoPreenchedor.setTipoMovimento(TipoMovimento.ENTRADA);
						movimentoPreenchedor.setUsuario(usuarioPreenchedor);
						try {
							SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
							movimentoPreenchedor.setDataAcao((sdf.parse(sdf.format(new Date()))));
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						movimentoRepositorio.adicionar(movimentoPreenchedor);
					}
				}

				for (Imagem imagem : imagensProduto) {
					imagem.setUsuarioAvaliador(usuarioAvaliador);
				}
			}
			Set<Imagem> imagensExistentes = produtoBuscado.getImagens();
			System.out.println("IMAGENS EXISTENTES: " + imagensExistentes.size());

			List<Caracteristica> caracsParaDeletar = new ArrayList<>();
			for (Caracteristica caracteristica : caracteristicas) {
				caracteristica.setProduto(produtoBuscado);
				System.out.println("CARCS: " + caracteristica.getAtributo().getNome() + " - "
						+ caracteristica.getValor() + " / " + caracteristica.isAvaliada());
				if (caracteristica.isAvaliada() != null && caracteristica.isAvaliada() == false) {
					System.out.println("CARAC PARA DELETAR: " + caracteristica.getId() + " / "
							+ caracteristica.getAtributo().getNome() + " - " + caracteristica.getValor());
					System.out.println("DELETANDO CARAC");
					caracsParaDeletar.add(caracteristica);
					produtoBuscado = repositorio.deletar(caracteristica.getId());
				}

				if (caracteristica.isAvaliada() != null && caracteristica.isAvaliada() == true) {
					repositorio.alterar(caracteristica);
				}
			}
			for (Imagem imagem : imagensProduto) {

				if (imagem.isAvaliada() != null && imagem.isAvaliada() == false) {
					System.out.println("AVALIADA: " + imagem.isAvaliada());
					if (imagem.getCaminho() != null) {
						String imagemExistente = imagem.getCaminho().replace("/", "\\");
						String raiz = context.getRealPath("/");
						File arquivoImagem = new File(raiz + imagemExistente);
						System.out.println("CAMINHO IMAGEM: " + raiz + imagemExistente);
						if (arquivoImagem != null) {
							System.out.println("DELETANDO NO SERVIDOR");
							arquivoImagem.delete();
						}
					}
					System.out.println("DELETANDO IMAGEM");
					System.out.println("IMAGEM PARA DELETAR: " + imagem.getId() + " / " + imagem.isAvaliada() + " / "
							+ imagem.getCaminho() + " / " + imagem.getUsuarioAvaliador()
							+ imagem.getUsuarioPreenchedor());
					System.out.println();
					produtoBuscado = produtoRepositorio.deletarImagem(imagem.getId(), produtoBuscado.getId());

				}

				if (imagem.isAvaliada() != null && imagem.isAvaliada() == true) {
					for (Imagem imagemExistente : imagensExistentes) {
						if (imagem.getId() == imagemExistente.getId()) {
							imagemExistente.setAvaliada(true);
						}
					}
				}
			}

			imagensExistentes = produtoBuscado.getImagens();
			List<Caracteristica> todasCaracteristicas = repositorio.caracteristicaPorProduto(produtoBuscado.getId());
			List<Caracteristica> caracteristicasAvaliadas = repositorio
					.caracteristicaProdutoTrue(produtoBuscado.getId());
			System.out.println("TOTAL CARCS PRODUTO: " + todasCaracteristicas.size());
			List<Atributo> atributosPrincipais = atributoRepositorio.atributosPorTipo(0);
			System.out.println("IMAGENS DO PRODUTO: " + imagensExistentes.size());
			for (Imagem imagem : imagensExistentes) {
				System.out.println("IMAGEM PROD: " + imagem.getId() + " - " + imagem.getCaminho());
			}
			int totalAtributosPrincipais = atributosPrincipais.size();
			int contAvaliadas = caracteristicasAvaliadas.size();

			System.out.println("INICIO TOTAL AVALIADAS: " + contAvaliadas);

			for (Caracteristica caracteristica : todasCaracteristicas) {
				if (caracteristica.getAtributo().getTipoAtributo() == TipoAtributo.PRINCIPAL) {
					totalAtributosPrincipais -= 1;
				}
			}

			for (Imagem imagem : imagensExistentes) {
				if (imagem.isAvaliada() != null && imagem.isAvaliada() == true) {
					contAvaliadas += 1;
					System.out.println("IMG AVALIADA " + contAvaliadas);
				}
			}

			System.out.println("TOTAL AVALIADAS ANDROID: " + contAvaliadas);
			System.out.println("TOTAL DE CARACS : " + todasCaracteristicas.size());
			System.out.println("TOTAL DE IMGS : " + imagensExistentes.size());
			System.out.println("TOTAL DE ATRIBUTOS PRINCIPAIS : " + totalAtributosPrincipais);
			int contTotal = todasCaracteristicas.size() + imagensExistentes.size() + totalAtributosPrincipais;
			System.out.println("RESULTADO ANDROID: " + contTotal);
			int total = (contAvaliadas * 100) / contTotal;
			produtoBuscado.setProgresso(total);
			if (total == 100) {
				produtoBuscado.setPendente(false);
			}
			produtoRepositorio.alterar(produtoBuscado);

			for (Caracteristica caracteristica : caracteristicas) {
				System.out.println("TESTE CARAC: " + caracteristica.getId() + " / " + caracteristica.getValor());
				if (caracteristica.isAvaliada() != null) {
					if (caracteristica.isAvaliada() == true) {
						caracteristica.setProduto(produtoBuscado);
						repositorio.alterar(caracteristica);
					}
				}

			}

			return Response.ok().build();
		}

	}

	/*
	 * Altera uma determinada Caracteristica
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response alterar(@PathParam("id") Long id, Caracteristica caracteristica) {
		Caracteristica buscada = repositorio.buscar(id);
		buscada.setAtributo(caracteristica.getAtributo());
		buscada.setValor(caracteristica.getValor());
		repositorio.alterar(buscada);
		return Response.ok().build();
	}

	/*
	 * PREENCHER CARACTERISTICA COM PRIM�RIO E SECUND�RIO E ALTERAR IMAGEM
	 */

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/preencherMobile/{idProduto}")
	public Response alterarCaracteristicaParaMobile(@Context HttpServletRequest req,
			@PathParam("idProduto") Long idProduto, ListasAvaliar listas) {

		List<Caracteristica> caracteristicas = listas.getCaracteristicas();

		System.out.println("TAMANHO carcs: " + caracteristicas.size());
		List<Imagem> imagensProduto = listas.getImagens();
		System.out.println("TAMANHO IMAGENS: " + imagensProduto.size());

		String token = req.getHeader("Authorization");
		if (token == null) {
			return null;
		} else {
			JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
			Map<String, Object> claims = null;

			try {
				claims = verifier.verify(token);
			} catch (Exception e) {
				e.printStackTrace();
			}
			String idString = claims.get("id_usuario").toString();
			Long idUsuario = Long.valueOf(idString);

			Colaborador colaboradorPreenchedor = colaboradorRepositorio.buscar(idUsuario);

			for (Caracteristica caracteristica : caracteristicas) {
				System.out.println("carac id -------" + caracteristica.getId());
				System.out.println("nome -----------" + caracteristica.getAtributo().getNome());
				System.out.println("valor ----------" + caracteristica.getValor());
				if (caracteristica.getAtributo().getTipoAtributo().equals(TipoAtributo.PRINCIPAL)) {

					Long idAtributo = caracteristica.getAtributo().getId();
					Caracteristica carac = repositorio.existente(idProduto, idAtributo, colaboradorPreenchedor.getId());

					if (carac == null) {
						caracteristica.setUsuarioPreenchedor(colaboradorPreenchedor);
						repositorio.adicionar(caracteristica);

					} else {
						carac.setAvaliada(null);
						carac.setValor(caracteristica.getValor());
						carac.setUsuarioPreenchedor(colaboradorPreenchedor);
						repositorio.alterar(carac);
					}
				}

				if (caracteristica.getAtributo().getTipoAtributo().equals(TipoAtributo.SECUNDARIO)) {
					System.out.println("SECUNDARIOS -----------" + caracteristica.getAtributo().getNome() + " | "
							+ caracteristica.getValor());
					Caracteristica carac = repositorio.existenteSecundario(idProduto,
							caracteristica.getAtributo().getNome());

					if (carac == null) {
						Atributo atributoAdicionado = null;
						System.out.println(caracteristica.getAtributo().getNome());
						Atributo atributoBuscado = atributoRepositorio
								.atributosPorNome(caracteristica.getAtributo().getNome());

						if (atributoBuscado == null) {
							atributoAdicionado = atributoRepositorio.adicionarSecundario(caracteristica.getAtributo());
						} else {
							atributoAdicionado = atributoRepositorio
									.atributosPorNome(caracteristica.getAtributo().getNome());
						}
						caracteristica.setAtributo(atributoAdicionado);
						caracteristica.setUsuarioPreenchedor(colaboradorPreenchedor);
						repositorio.adicionar(caracteristica);
					}
				}

			}

			for (Imagem imagem : imagensProduto) {
				Produto buscado = produtoRepositorio.buscar(idProduto);
				System.out.println("MOBILE ALTERAR");
				Imagem novaImagem = new Imagem();
				novaImagem.setImagemAndroid(imagem.getImagemAndroid());
				buscado.getImagens().add(novaImagem);

				Set<Imagem> imagens = buscado.getImagens();
				alterarImagens(novaImagem, imagens, idProduto);

				buscado.setImagens(imagens);

				produtoRepositorio.alterar(buscado);

			}
			
			// ALTERANDO PROGRESSO
			Produto produtoBuscado = produtoRepositorio.buscar(idProduto);
			Set<Imagem> imagensExistentes = produtoBuscado.getImagens();
			List<Caracteristica> todasCaracteristicas = repositorio.caracteristicaPorProduto(produtoBuscado.getId());
			List<Caracteristica> caracteristicasAvaliadas = repositorio
					.caracteristicaProdutoTrue(produtoBuscado.getId());
			List<Atributo> atributosPrincipais = atributoRepositorio.atributosPorTipo(0);
			
			int totalAtributosPrincipais = atributosPrincipais.size();
			int contAvaliadas = caracteristicasAvaliadas.size();

			for (Caracteristica caracteristica : todasCaracteristicas) {
				if (caracteristica.getAtributo().getTipoAtributo() == TipoAtributo.PRINCIPAL) {
					totalAtributosPrincipais -= 1;
				}
			}

			for (Imagem imagem : imagensExistentes) {
				if (imagem.isAvaliada() != null && imagem.isAvaliada() == true) {
					contAvaliadas += 1;
				}
			}

			int contTotal = todasCaracteristicas.size() + imagensExistentes.size() + totalAtributosPrincipais;
			int total = (contAvaliadas * 100) / contTotal;
			produtoBuscado.setProgresso(total);
			if (total == 100) {
				produtoBuscado.setPendente(false);
			}
			
			produtoRepositorio.alterar(produtoBuscado);
			
			return Response.ok().build();

		}

	}

	/*
	 * 
	 * M�TODOS COMPLEMENTARES
	 * 
	 */

	public void alterarImagens(Imagem imagem, Set<Imagem> imagens, Long idProduto) {
		String imagemBase64 = imagem.getImagemAndroid().replaceAll("\\s+", "");
		if (!imagemBase64.isEmpty()) {
			try {
				String raiz = context.getRealPath("/");
				String fileName = "prod" + idProduto + "img.jpg";
				byte[] imagemArray = Base64.getDecoder().decode(imagemBase64.getBytes());

				salvarImagemAndroid(imagemArray, raiz, fileName, imagem, imagens);
			} catch (Exception e) {
				// That string wasn't valid.
				System.out.println("ERRO AO DECODIFICAR IMAGEM: " + e.getMessage());
			}
		} else {
			System.out.println("IMAGEM ANDROID NULA");
		}

	}

	public static String removeTillWord(String input, String word) {
		return input.substring(input.indexOf(word) + 1);
	}

	private void salvarImagemAndroid(byte[] bytes, String raiz, String fileName, Imagem imagem, Set<Imagem> imagens) {
		try {
			Random gerador = new Random();
			int numero = gerador.nextInt();
			String nomeArquivo = String.valueOf(numero) + fileName;

			if (nomeArquivo.startsWith("-")) {
				nomeArquivo = nomeArquivo.replace("-", "").replace(" ", "");
			}

			String complemento = "/produto/" + nomeArquivo;
			imagem.setCaminho(complemento);
			complemento = complemento.replace("/", "\\");
			fileName = raiz + complemento;
			imagens.add(imagem);
			System.out.println("Caminho: " + fileName);
			fileName = removeTillWord(fileName, "/produto");
			System.out.println("Novo caminho: " + fileName);

			writeFile(bytes, fileName);

			System.out.println("Done");

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void writeFile(byte[] content, String filename) throws IOException {

		File file = new File(filename);

		if (!file.exists()) {
			file.createNewFile();
		}

		FileOutputStream fop = new FileOutputStream(file);

		fop.write(content);
		fop.flush();
		fop.close();

	}

	public void criarPasta() {
		try {
			File file = new File(context.getRealPath("/") + "//produto");
			file.mkdirs();
			String caminhoCompleto = file.getAbsolutePath();
			System.out.println("CAMINHO COMPLETO: " + caminhoCompleto);
			System.out.println("PASTA CRIADA COM SUCESSO");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
