package br.com.ecommet.controller.restController;

import java.io.IOException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.JWTVerifyException;

import br.com.ecommet.model.Colaborador;
import br.com.ecommet.model.Consumidor;
import br.com.ecommet.model.MovimentoCredito;
import br.com.ecommet.model.TipoMovimento;
import br.com.ecommet.model.Usuario;
import br.com.ecommet.repositorio.ColaboradorRepositorio;
import br.com.ecommet.repositorio.MovimentoCreditoRepositorio;
import br.com.ecommet.repositorio.UsuarioRepositorio;

@Path("/historico")
public class MovimentoCreditoRestController {

	@EJB
	private MovimentoCreditoRepositorio repositorio;

	@EJB
	private ColaboradorRepositorio colaboradorRepositorio;
	
	@EJB
	private UsuarioRepositorio usuarioRepositorio;

	/*
	 * Insere um novo MovimentoCredito do tipo Saida
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/saida")
	public Response realizarRetirada(@Context HttpServletRequest req, MovimentoCredito movimentoCredito)
			throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException,
			IOException, JWTVerifyException {

		String token = req.getHeader("Authorization");
		if (token == null) {
			return null;
		} else {
			JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
			Map<String, Object> claims = verifier.verify(token);
			String idString = claims.get("id_usuario").toString();
			Long id = Long.valueOf(idString);
			System.out.println("ID DO USU�RIO: " + id);

			Colaborador colaboradorBuscado = repositorio.buscarColaborador(id);
			System.out.println("Movimento e-coins: " + movimentoCredito.getEcoins());
			System.out.println("Movimento valor: " + movimentoCredito.getValor());
			System.out.println("colaboradorBuscado ----------->" + colaboradorBuscado.getId());
			List<MovimentoCredito> verficarMovimento = repositorio.verificarMovimento(id);
			System.out.println("AQUI DANIEL" + verficarMovimento.size());
			if (verficarMovimento.size() > 0) {
				return Response.status(Status.NOT_ACCEPTABLE).build();
			} else {
				movimentoCredito.setUsuario(colaboradorBuscado);
				movimentoCredito.setTipoMovimento(TipoMovimento.SAIDA);

				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");

				try {
					movimentoCredito.setDataAcao(sdf.parse(sdf.format(new Date())));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				repositorio.adicionar(movimentoCredito);

				return Response.ok().build();
			}
		}
	}

	/*
	 * Insere um novo MovimentoCredito do tipo Entrada
	 */
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/entrada")
	public Response realizarEntrada(@Context HttpServletRequest req, MovimentoCredito movimentoCredito)
			throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException,
			IOException, JWTVerifyException {

		String token = req.getHeader("Authorization");
		if (token == null) {
			return null;
		} else {
			JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
			Map<String, Object> claims = verifier.verify(token);
			String idString = claims.get("id_usuario").toString();
			Long id = Long.valueOf(idString);
			System.out.println("ID DO USU�RIO: " + id);

			Consumidor consumidorBuscado = repositorio.buscarConsumidor(id);

			movimentoCredito.setUsuario(consumidorBuscado);
			movimentoCredito.setTipoMovimento(TipoMovimento.ENTRADA);
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");

			try {
				movimentoCredito.setDataAcao(sdf.parse(sdf.format(new Date())));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			repositorio.adicionar(movimentoCredito);
			return Response.ok().build();
		}
	}

	/*
	 * Lista todos os MovimentoCreditos
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/true")
	public List<MovimentoCredito> listarTrue() {
		return repositorio.movimentoCreditosTrue();
	}

	/*
	 * Lista todos os MovimentoCreditos
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/false")
	public List<MovimentoCredito> listarFalse() {
		return repositorio.movimentoCreditosFalse();
	}

	/*
	 * List todos os MovimentoCreditos por determinado usu�rio
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/naoPendentes/{tipo}")
	public List<MovimentoCredito> historicoAsc(@Context HttpServletRequest req, @PathParam("tipo") int tipo)
			throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException,
			IOException, JWTVerifyException {

		String token = req.getHeader("Authorization");
		if (token == null) {
			return null;
		} else {
			JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
			Map<String, Object> claims = verifier.verify(token);
			String idString = claims.get("id_usuario").toString();
			Long id = Long.valueOf(idString);
			System.out.println("ID DO USU�RIO: " + id);

			return repositorio.historicoOrdenadorTipo(id, tipo);
		}
	}

	/*
	 * List todos os MovimentoCreditos por determinado usu�rio
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/tipo/{tipo}")
	public List<MovimentoCredito> tipoHistoricoPorUsuario(@Context HttpServletRequest req, @PathParam("tipo") int tipo)
			throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException,
			IOException, JWTVerifyException {
		String token = req.getHeader("Authorization");
		if (token == null) {
			return null;
		} else {
			JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
			Map<String, Object> claims = verifier.verify(token);
			String idString = claims.get("id_usuario").toString();
			Long id = Long.valueOf(idString);
			System.out.println("ID DO USU�RIO: " + id);
			return repositorio.tipoHistoricoPorUsuario(id, tipo);
		}
	}

	/*
	 * List todos os MovimentoCreditos por determinado usu�rio
	 */

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/pendentes")
	public List<MovimentoCredito> movimentosPendentesPorUsuario(@Context HttpServletRequest req)
			throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException,
			IOException, JWTVerifyException {

		String token = req.getHeader("Authorization");
		if (token == null) {
			return null;
		} else {
			JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
			Map<String, Object> claims = verifier.verify(token);
			String idString = claims.get("id_usuario").toString();
			Long id = Long.valueOf(idString);
			System.out.println("ID DO USU�RIO: " + id);

			return repositorio.historicoPendentesPorUsuario(id);
		}
	}

	/*
	 * List todos os MovimentoCreditos por determinado usu�rio
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/saidas/{tipo}")
	public List<MovimentoCredito> tipoSaidasPendentes(@Context HttpServletRequest req, @PathParam("tipo") int tipo)
			throws InvalidKeyException, NoSuchAlgorithmException, IllegalStateException, SignatureException,
			IOException, JWTVerifyException {

		String token = req.getHeader("Authorization");
		if (token == null) {
			return null;
		} else {
			JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
			Map<String, Object> claims = verifier.verify(token);
			String idString = claims.get("id_usuario").toString();
			Long id = Long.valueOf(idString);
			System.out.println("ID DO USU�RIO: " + id);

			return repositorio.saidas(tipo);
		}
	}

	/*
	 * Lista todos os produtos especifico
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/buscar/{nome}/{tipo}")
	public List<MovimentoCredito> listarEspecifico(@PathParam("nome") String nome, @PathParam("tipo") int tipo) {

		return repositorio.buscarRetiradasPorTitular(nome, tipo);
	}

	/*
	 * Busca um determinado MovimentoCredito
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public MovimentoCredito buscar(@PathParam("id") Long id) {
		return repositorio.buscar(id);
	}

	/*
	 * libera a retirada de credito
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/finalizar/{idMovimento}")
	public Response liberarSaidaDeCredito(@PathParam("idMovimento") Long idMovimento) {
		MovimentoCredito movimentoCredito = repositorio.buscar(idMovimento);
		System.out.println("MOVIMENTO VALOR: " + movimentoCredito.getValor());
		System.out.println("MOVIMENTO USU�RIO ID: " + movimentoCredito.getUsuario().getId());
		Colaborador colaboradorBuscado = repositorio.buscarColaborador(movimentoCredito.getUsuario().getId());
		System.out.println("NOME COLABORADOR: " + colaboradorBuscado.getNome());
		BigDecimal credito = colaboradorBuscado.getCredito();
		BigDecimal quantidadeECoins = movimentoCredito.getEcoins();
		Double quantidadeECoinsDouble = Double.parseDouble(String.valueOf(movimentoCredito.getEcoins()));
		System.out.println("DOUBLE ECOINS: " + quantidadeECoinsDouble);
		System.out.println("contaa---------------------------" + colaboradorBuscado.getConta());

		if (quantidadeECoinsDouble <= 0 || credito.compareTo(quantidadeECoins) < 1
				|| colaboradorBuscado.getConta() == null) {
			String error = "Saldo � menor que o valor solicitado";
			return Response.status(Status.FORBIDDEN).entity(error).build();
		} else {

			credito = credito.subtract(quantidadeECoins);
			movimentoCredito.setUsuario(colaboradorBuscado);
			movimentoCredito.setTipoMovimento(TipoMovimento.SAIDA);
			movimentoCredito.setPendente(false);
			repositorio.alterar(movimentoCredito);
			colaboradorBuscado.setCredito(credito);
			colaboradorRepositorio.alterar(colaboradorBuscado);

			return Response.ok().build();
		}
	}

	/*
	 * Deleta um determinado MovimentoCredito
	 */
	@DELETE
	@Path("/{id}")
	public Response deletar(@PathParam("id") Long id) {
		repositorio.deletar(id);
		return Response.noContent().build();
	}

	/*
	 * altera um determinado MovimentoCredito que est� pendente para false
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/finalizarEntrada/{idMovimento}")
	public Response liberarEntradaDeCredito(@PathParam("idMovimento") Long idMovimento) {
		MovimentoCredito movimentoCredito = repositorio.buscar(idMovimento);
		System.out.println("MOVIMENTO VALOR: " + movimentoCredito.getValor());
		System.out.println("MOVIMENTO USU�RIO ID: " + movimentoCredito.getUsuario().getId());

		Consumidor consumidorBuscado = repositorio.buscarConsumidor(movimentoCredito.getUsuario().getId());
		System.out.println("NOME COLABORADOR: " + consumidorBuscado.getNome());

		BigDecimal credito = consumidorBuscado.getCredito();
		BigDecimal quantidadeECoins = movimentoCredito.getEcoins();

		System.out.println("contaa---------------------------" + consumidorBuscado.getConta());

		if (consumidorBuscado.getConta() == null) {
			String error = "Saldo � menor que o valor solicitado";
			return Response.status(Status.FORBIDDEN).entity(error).build();
		} else {

			credito = credito.add(quantidadeECoins);
			movimentoCredito.setUsuario(consumidorBuscado);
			movimentoCredito.setTipoMovimento(TipoMovimento.SAIDA);
			movimentoCredito.setPendente(false);
			repositorio.alterar(movimentoCredito);
			consumidorBuscado.setCredito(credito);
			colaboradorRepositorio.alterar(consumidorBuscado);

			return Response.ok().build();
		}
	}

}
