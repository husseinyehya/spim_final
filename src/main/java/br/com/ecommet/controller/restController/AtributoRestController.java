package br.com.ecommet.controller.restController;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.ecommet.model.Atributo;
import br.com.ecommet.repositorio.AtributoRepositorio;

@Path("/atributo")
public class AtributoRestController {

	@EJB
	private AtributoRepositorio repositorio;

	/*
	 * Insere um novo Atributo
	 */	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/")
	public Response inserir(Atributo atributo) {
		repositorio.adicionar(atributo);
		return Response.ok().build();
	}


	/*
	 * Lista todos os Atributos
	 */	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/listar")
	public List<Atributo> listar() {
		return repositorio.atributos();
	}


	/*
	 * Lista Atributo por nome por tipo (Primário ou Secundário)
	 */	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/buscar/{nome}/{tipo}")
	public List<Atributo> listarEspecifico(@PathParam("nome") String nome, @PathParam("tipo") int tipo) {
		System.out.println("tipo que veio------------- " + tipo);
		return repositorio.atributosPorNomeETipo(nome, tipo);
	}

	
	/*
	 * Lista atributos por seu tipoAtributo (Primário ou Secundário)
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/listarTipo/{tipo}")
	public List<Atributo> listarAtributoPorTipo(@PathParam("tipo") int tipo) {
		return repositorio.atributosPorTipo(tipo);
	}

	/*
	 * Busca atributo por seu ID
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Atributo buscar(@PathParam("id") Long id) {
		return repositorio.buscar(id);
	}

	/*
	 *	Altera um determinado atributo
	 */
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response alterar(@PathParam("id") Long id, Atributo atributo) {
		Atributo buscado = repositorio.buscar(id);
		buscado.setNome(atributo.getNome());
		buscado.setTipoAtributo(atributo.getTipoAtributo());
		repositorio.alterar(buscado);

		return Response.ok().build();
	}

	/*
	 * Deleta um determinado atributo
	 */
	@DELETE
	@Path("/{id}")
	public Response deletar(@PathParam("id") Long id) {

		repositorio.deletar(id);
		return Response.noContent().build();
	}

}
