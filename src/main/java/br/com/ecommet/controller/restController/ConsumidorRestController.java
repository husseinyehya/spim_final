package br.com.ecommet.controller.restController;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import br.com.ecommet.model.Consumidor;
import br.com.ecommet.model.Usuario;
import br.com.ecommet.repositorio.ConsumidorRepositorio;
import br.com.ecommet.repositorio.UsuarioRepositorio;

@Path("/consumidor")
public class ConsumidorRestController {

	@EJB
	private ConsumidorRepositorio repositorio;

	@EJB
	private UsuarioRepositorio usuarioRepositorio;

	@Inject
	private ServletContext context;
	private String inputPart;
	public static final String EMISSOR = "ECOMMET";
	public static final String SECRET = "3C0MM37";

	//
	// Insere um novo Produto com imagem
	//
	@POST
	@Path("/")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response inserir(@MultipartForm MultipartFormDataInput input) throws NoSuchAlgorithmException {
		Consumidor consumidor = new Consumidor();
		String fileName = "";
		// criarPasta();
		boolean emailOk = false;
		boolean cpfOk = false;
		try {
			inputPart = input.getFormDataMap().get("nome").get(0).getBodyAsString();
			consumidor.setNome(inputPart.toString());

			inputPart = input.getFormDataMap().get("sobrenome").get(0).getBodyAsString();
			if (!inputPart.isEmpty()) {
				consumidor.setSobrenome(inputPart.toString());
			} else {
				consumidor.setSobrenome(null);
			}

			inputPart = input.getFormDataMap().get("cpf").get(0).getBodyAsString();
			if (!inputPart.isEmpty()) {
				consumidor.setCpf(inputPart.toString());
			} else {
				consumidor.setCpf(null);
			}

			// VERIFICA SE O E-MAIL J� EXISTE NO BD

			String cpfDigitado = consumidor.getCpf();

			Usuario colaboradorCpf = usuarioRepositorio.buscarPorCpf(cpfDigitado);

			if (colaboradorCpf == null) {
				cpfOk = true;
			} else {
				return Response.status(Status.NOT_ACCEPTABLE).build();
			}

			inputPart = input.getFormDataMap().get("email").get(0).getBodyAsString();
			if (!inputPart.isEmpty()) {
				consumidor.setEmail(inputPart.toString());
			} else {
				consumidor.setEmail(null);
			}

			String emailDigitado = consumidor.getEmail();
			Usuario consumidorEmail = usuarioRepositorio.buscarPorEmail(emailDigitado);

			if (consumidorEmail == null) {
				System.out.println("email --------------- " + consumidorEmail);
				emailOk = true;
			} else {
				return Response.status(Status.UNAUTHORIZED).build();
			}

			inputPart = input.getFormDataMap().get("telefone").get(0).getBodyAsString();
			if (!inputPart.isEmpty()) {
				consumidor.setTelefone(inputPart.toString());
			} else {
				consumidor.setTelefone(null);
			}

			inputPart = input.getFormDataMap().get("celular").get(0).getBodyAsString();
			if (!inputPart.isEmpty()) {
				consumidor.setCelular(inputPart.toString());
			} else {
				consumidor.setCelular(null);
			}

			inputPart = input.getFormDataMap().get("senha").get(0).getBodyAsString();
			if (!inputPart.isEmpty()) {

				consumidor.setSenha(inputPart.toString());
				criptografarSenha(consumidor);
			} else {
				consumidor.setSenha(null);
			}

			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");

			try {
				consumidor.setDataCadastro(sdf.parse(sdf.format(new Date())));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (IOException e1) {
			e1.printStackTrace();
		}

		String raiz = context.getRealPath(File.separator + "usuario");
		System.out.println("--------------RAIZ: " + raiz);

		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		List<InputPart> inputParts = uploadForm.get("fotoPerfil");

		for (InputPart inputPart : inputParts) {
			try {
				MultivaluedMap<String, String> header = inputPart.getHeaders();
				fileName = getFileName(header);

				// convert the uploaded file to inputstream
				InputStream inputStream = inputPart.getBody(InputStream.class, null);

				byte[] bytes = IOUtils.toByteArray(inputStream);

				// constructs upload file path
				boolean arquivoVazio = fileName.isEmpty();
				if (!arquivoVazio) {
					if (!fileName.equals("unknown")) {
						if (fileName.endsWith(".jpeg") || fileName.endsWith(".jpg") || fileName.endsWith(".png")) {

							salvarImagem(bytes, raiz, fileName, consumidor);

						} else {
							System.out.println("FORMATO INV�LIDO");
							return Response.status(Response.Status.UNSUPPORTED_MEDIA_TYPE).build();
						}
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		if (emailOk == true && cpfOk == true) {
			if (verificarCpf(consumidor.getCpf())) {
				repositorio.adicionar(consumidor);
				return Response.ok().build();
			}
		}
		return Response.status(Status.FORBIDDEN).build();

	}

	//
	// Lista todos os consumidores
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/listar")
	public List<Consumidor> listar() {
		return repositorio.consumidores();
	}

	//
	// Busca um determinado Consumidor
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Consumidor buscar(@PathParam("id") Long id) {

		return repositorio.buscar(id);
	}

	//
	// Altera um determinado Consumidor
	//
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response alterar(@PathParam("id") Long id, Consumidor consumidor)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		Consumidor buscado = repositorio.buscar(id);
		buscado.setNome(consumidor.getNome());
		buscado.setSobrenome(consumidor.getSobrenome());
		buscado.setCpf(consumidor.getCpf());
		buscado.setEmail(consumidor.getEmail());
		buscado.setTelefone(consumidor.getTelefone());
		buscado.setCelular(consumidor.getCelular());
		buscado.setConta(consumidor.getConta());

		if (verificarCpf(consumidor.getCpf()) == true) {
			repositorio.alterar(buscado);
			return Response.ok().build();
		} else {
			return Response.status(Status.BAD_REQUEST).build();
		}
	}

	//
	// Deleta um determinado Consumidor
	//
	@DELETE
	@Path("/{id}")
	public Response deletar(@PathParam("id") Long id) {

		repositorio.deletar(id);
		return Response.noContent().build();
	}

	//
	// M�TODOS COMPLEMENTARES
	//
	public void criptografarSenha(Usuario usuario) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String senha = usuario.getSenha();

		MessageDigest algorithm = MessageDigest.getInstance("SHA-256");
		byte messageDigestSenhaAdmin[] = algorithm.digest(senha.getBytes("UTF-8"));

		StringBuilder hexStringSenhaAdmin = new StringBuilder();
		for (byte b : messageDigestSenhaAdmin) {
			hexStringSenhaAdmin.append(String.format("%02X", 0xFF & b));
		}
		String senhahex = hexStringSenhaAdmin.toString();
		usuario.setSenha(senhahex);
		System.out.println("senha: " + senhahex);
	}

	private void writeFile(byte[] content, String filename) throws IOException {

		File file = new File(filename);

		System.out.println("CAMINHO FILE: " + file.getAbsolutePath());
		if (!file.exists()) {
			file.createNewFile();
		}

		FileOutputStream fop = new FileOutputStream(file);

		fop.write(content);
		fop.flush();
		fop.close();

	}

	private void criarPasta() {
		try {
			File file = new File(context.getRealPath(File.separator) + File.separator + "usuario");
			file.mkdirs();
			String caminhoCompleto = file.getAbsolutePath();
			System.out.println("CAMINHO COMPLETO: " + caminhoCompleto);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void salvarImagem(byte[] bytes, String raiz, String fileName, Usuario usuario) {
		try {
			Random gerador = new Random();
			int numero = gerador.nextInt();
			String nomeArquivo = String.valueOf(numero) + fileName;

			if (nomeArquivo.startsWith("-")) {
				nomeArquivo = nomeArquivo.replace("-", "").replace(" ", "");
			}

			nomeArquivo = nomeArquivo.replace(" ", "");

			if (nomeArquivo != null) {
				File imagem;

				if (usuario.getCaminhoImagem() != null) {
					// String imagemExistente =
					// usuario.getCaminhoImagem().replace("/", "\\");
					imagem = new File(raiz + usuario.getCaminhoImagem());
					if (imagem != null) {
						imagem.delete();
					}
				}
				String complemento = File.separator + "usuario" + File.separator + nomeArquivo;
				usuario.setCaminhoImagem(complemento.replace(File.separator, "/"));
				// complemento = complemento.replace("/", "\\");
				fileName = raiz + complemento;
				System.out.println("Caminho: " + fileName);	
				System.out.println("Novo caminho: " + fileName);

				writeFile(bytes, fileName);

				System.out.println("Done");
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private String getFileName(MultivaluedMap<String, String> header) {

		String[] contentDisposition = header.getFirst("Content-Disposition").split(";");

		for (String filename : contentDisposition) {
			if ((filename.trim().startsWith("filename"))) {

				String[] name = filename.split("=");

				String finalFileName = name[1].trim().replaceAll("\"", "");
				return finalFileName;
			}
		}
		return "unknown";
	}

	public boolean verificarCpf(String cpf) {
		int somaJ, somaK, restoJ, restoK, a, b, c, d, e, f, g, h, i, j, k, j1, k1;

		a = Integer.parseInt(String.valueOf(cpf.charAt(0)));
		b = Integer.parseInt(String.valueOf(cpf.charAt(1)));
		c = Integer.parseInt(String.valueOf(cpf.charAt(2)));
		d = Integer.parseInt(String.valueOf(cpf.charAt(4)));
		e = Integer.parseInt(String.valueOf(cpf.charAt(5)));
		f = Integer.parseInt(String.valueOf(cpf.charAt(6)));
		g = Integer.parseInt(String.valueOf(cpf.charAt(8)));
		h = Integer.parseInt(String.valueOf(cpf.charAt(9)));
		i = Integer.parseInt(String.valueOf(cpf.charAt(10)));
		j = Integer.parseInt(String.valueOf(cpf.charAt(12)));
		k = Integer.parseInt(String.valueOf(cpf.charAt(13)));
		j1 = 0;
		k1 = 0;

		somaJ = 10 * a + 9 * b + 8 * c + 7 * d + 6 * e + 5 * f + 4 * g + 3 * h + 2 * i;
		restoJ = somaJ % 11;

		somaK = 11 * a + 10 * b + 9 * c + 8 * d + 7 * e + 6 * f + 5 * g + 4 * h + 3 * i + 2 * j;
		restoK = somaK % 11;

		if (restoJ > 1) {
			j1 = 11 - restoJ;
		}

		if (restoK > 1) {
			k1 = 11 - restoK;
		}

		if (a == b && b == c && c == d && d == e && e == f && f == g && g == h && h == i && i == j && j == k) {
			System.out.println("\nCPF inv�lido.");
			return false;
		}

		if (j == j1 && k == k1) {
			System.out.println("\nCPF v�lido.");
			return true;
		} else {
			System.out.println("\nCPF inv�lido.");
			return false;
		}
	}

}
