package br.com.ecommet.controller.restController;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import javax.ejb.EJB;
import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import com.auth0.jwt.JWTVerifier;

import br.com.ecommet.crawler.WebCrawler;
import br.com.ecommet.model.Administrador;
import br.com.ecommet.model.Atributo;
import br.com.ecommet.model.Categoria;
import br.com.ecommet.model.Colaborador;
import br.com.ecommet.model.Imagem;
import br.com.ecommet.model.MovimentoCredito;
import br.com.ecommet.model.Produto;
import br.com.ecommet.model.TipoMovimento;
import br.com.ecommet.model.Usuario;
import br.com.ecommet.repositorio.AtributoRepositorio;
import br.com.ecommet.repositorio.CaracteristicaRepositorio;
import br.com.ecommet.repositorio.CategoriaRepositorio;
import br.com.ecommet.repositorio.ColaboradorRepositorio;
import br.com.ecommet.repositorio.MovimentoCreditoRepositorio;
import br.com.ecommet.repositorio.ProdutoRepositorio;
import br.com.ecommet.repositorio.UsuarioRepositorio;

@Path("/produto")
public class ProdutoRestController {

	@EJB
	private ProdutoRepositorio repositorio;

	@EJB
	private AtributoRepositorio repositorioAtributo;

	@EJB
	private CaracteristicaRepositorio repositorioCaracteristica;
	@EJB
	private CategoriaRepositorio repositorioCategoria;

	@EJB
	private ColaboradorRepositorio colaboradorRepositorio;

	@EJB
	private UsuarioRepositorio usuarioRepositorio;
	@EJB
	private MovimentoCreditoRepositorio movimentoRepositorio;

	@Inject
	private ServletContext context;

	private String inputPart;

	private WebCrawler crawler;

	//
	// Insere um novo Produto com imagem
	//
	@POST
	@Path("/{idProduto}")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response inserirImagem(@MultipartForm MultipartFormDataInput input, @PathParam("idProduto") Long idProduto) {

		Produto produto = repositorio.buscar(idProduto);
		String fileName = "";
		criarPasta();
		String token = null;
		try {
			token = input.getFormDataMap().get("token").get(0).getBodyAsString();

		} catch (IOException e1) {
			e1.printStackTrace();
		}
		System.out.println(token);
		if (token == null) {
			return null;
		} else {
			JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
			Map<String, Object> claims = null;

			try {
				claims = verifier.verify(token);
			} catch (Exception e) {
				e.printStackTrace();
			}
			String idString = claims.get("id_usuario").toString();
			Long idUsuario = Long.valueOf(idString);
			System.out.println(idUsuario);

			Usuario colaboradorPreenchedor = usuarioRepositorio.buscar(idUsuario);

			String raiz = context.getRealPath("/");
			System.out.println("--------------RAIZ: " + raiz);

			Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
			List<InputPart> inputParts = uploadForm.get("imagens");
			Set<Imagem> imagens = new HashSet<Imagem>();
			for (InputPart inputPart : inputParts) {

				try {

					System.out.println("Nome: " + inputPart);
					System.out.println("Imagens: " + inputParts);
					MultivaluedMap<String, String> header = inputPart.getHeaders();
					fileName = getFileName(header);

					// convert the uploaded file to inputstream
					InputStream inputStream = inputPart.getBody(InputStream.class, null);

					byte[] bytes = IOUtils.toByteArray(inputStream);

					// constructs upload file path
					boolean arquivoVazio = fileName.isEmpty();
					if (!arquivoVazio) {
						if (!fileName.equals("unknown")) {
							if (fileName.endsWith(".jpeg") || fileName.endsWith(".jpg") || fileName.endsWith(".png")) {

								salvarImagem(bytes, raiz, fileName, imagens);

							} else {
								System.out.println("FORMATO INV�LIDO");
								return Response.status(Response.Status.UNSUPPORTED_MEDIA_TYPE).build();
							}
						}
					}

				} catch (Exception e) {
					e.printStackTrace();
				}

			}

			if (produto.getImagens().size() < 4) {
				Set<Imagem> atual = produto.getImagens();
				for (Imagem imagem : imagens) {
					System.out.println("entrei no foreach");
					imagem.setUsuarioPreenchedor(colaboradorPreenchedor);
					atual.add(imagem);
				}

				produto.setImagens(atual);
				repositorio.alterar(produto);

				return Response.ok().entity("uploadFile is called, Uploaded file name : " + fileName).build();

			} else {
				System.out.println("N� DE IMAGENS ACIMA DO PERMITIDO");
				return Response.status(Response.Status.NOT_ACCEPTABLE).build();
			}
		}

	}

	@POST
	@Path("/inserir")
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response inserir(@MultipartForm MultipartFormDataInput input) {

		Produto produto = new Produto();
		String fileName = "";
		criarPasta();
		try {

			inputPart = input.getFormDataMap().get("nome").get(0).getBodyAsString();
			produto.setNome(inputPart.toString());

			inputPart = input.getFormDataMap().get("remuneracaoPorAtributo").get(0).getBodyAsString();
			if (!inputPart.isEmpty()) {
				produto.setRemuneracaoPorAtributo(BigDecimal.valueOf(Double.valueOf(inputPart.toString())));
			} else {
				produto.setRemuneracaoPorAtributo(null);
			}

			inputPart = input.getFormDataMap().get("preco").get(0).getBodyAsString();
			if (!inputPart.isEmpty()) {
				produto.setPreco(BigDecimal.valueOf(Double.valueOf(inputPart.toString())));
			} else {
				produto.setPreco(null);
			}

			inputPart = input.getFormDataMap().get("categoria").get(0).getBodyAsString();
			if (!inputPart.isEmpty()) {
				Categoria categoria = repositorioCategoria.buscar(Long.parseLong(inputPart.toString()));
				if (categoria != null) {
					produto.setCategoria(categoria);
				}
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		String raiz = context.getRealPath("/");
		System.out.println("--------------RAIZ: " + raiz);

		Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
		List<InputPart> inputParts = uploadForm.get("imagens");
		Set<Imagem> imagens = new HashSet<Imagem>();
		for (InputPart inputPart : inputParts) {

			try {

				System.out.println("Nome: " + inputPart);
				System.out.println("Imagens: " + inputParts);
				MultivaluedMap<String, String> header = inputPart.getHeaders();
				fileName = getFileName(header);

				// convert the uploaded file to inputstream
				InputStream inputStream = inputPart.getBody(InputStream.class, null);

				byte[] bytes = IOUtils.toByteArray(inputStream);

				// constructs upload file path
				boolean arquivoVazio = fileName.isEmpty();
				if (!arquivoVazio) {
					if (!fileName.equals("unknown")) {
						if (fileName.endsWith(".jpeg") || fileName.endsWith(".jpg") || fileName.endsWith(".png")) {

							salvarImagem(bytes, raiz, fileName, imagens);

						} else {
							System.out.println("FORMATO INV�LIDO");
							return Response.status(Response.Status.UNSUPPORTED_MEDIA_TYPE).build();
						}
					}
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		if (imagens.size() <= 4) {
			produto.setImagens(imagens);
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
				produto.setDataCadastro((sdf.parse(sdf.format(new Date()))));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Produto novoProduto = repositorio.adicionar(produto);

			try {
				realizarCrawler(novoProduto);

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return Response.ok().entity("uploadFile is called, Uploaded file name : " + fileName).build();

		} else {
			System.out.println("N� DE IMAGENS ACIMA DO PERMITIDO");
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

	}

	//
	// Insere um Produto sem imagem
	//
	@POST
	@Path("/inserirSemImagem")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response inserirSemImagem(Produto produto) {

		Categoria categoria = repositorioCategoria.buscar(produto.getCategoria().getId());

		if (categoria != null) {
			produto.setCategoria(categoria);
		}

		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
			produto.setDataCadastro((sdf.parse(sdf.format(new Date()))));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Produto novoProduto = repositorio.adicionar(produto);

		try {
			System.out.println("ANTES DO CRAWLER");
			realizarCrawler(novoProduto);
		} catch (Exception e) {
			// TODO: handle exception
		}

		System.out.println("PRODUTO INSERIDO PELA ECOMMET");
		return Response.ok().build();
	}

	//
	// Insere um Produto sem imagem
	//
	@PUT
	@Path("/desativar/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response desativarProduto(@PathParam("id") Long id) {

		repositorio.desativar(id);

		System.out.println("PRODUTO DESATIVADO");
		return Response.ok().build();
	}

	//
	// Lista todos os produtos onde o ativo = true
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/ativos")
	public List<Produto> listar() {
		criarPasta();
		return repositorio.produtosAtivos();
	}

	//
	// Lista todos os produtos inativos
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/inativos/")
	public List<Produto> listarInativos() {
		return repositorio.produtosInativos();
	}

	//
	// Lista todos os produtos n�o pendentes
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/naoPendentes")
	public List<Produto> listarNaoPendentes() {
		criarPasta();
		return repositorio.buscarNaoPendentes();
	}

	//
	// Busca uma determinada Imagem por Produto
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/filtrar/imagem/{produto_id}/{avaliada}")
	public List<Imagem> listarImagemPorProduto(@PathParam("produto_id") Long id,
			@PathParam("avaliada") Boolean avaliada, @Context HttpServletRequest req) {
		System.out.println("ENTREI AQUI, GET: ");

		String token = req.getHeader("Authorization");
		if (token == null) {
			return null;
		} else {
			JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
			Map<String, Object> claims = null;

			try {
				claims = verifier.verify(token);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String idString = claims.get("id_usuario").toString();
			Long idUsuario = Long.valueOf(idString);
			Usuario usuario = usuarioRepositorio.buscar(idUsuario);
			System.out.println("id ----------------- " + idUsuario);

			return repositorio.imagemPorProduto(id, avaliada, usuario);
		}
	}

	//
	// Lista todos os produtos pendentes
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/pendentes/{tipo}")
	public List<Produto> listarPendentes(@PathParam("tipo") int tipo) {
		criarPasta();

		return repositorio.buscarPendentes(tipo);
	}

	//
	// Lista todos os produtos especifico
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/buscar/{nome}")
	public List<Produto> listarEspecifico(@PathParam("nome") String nome) {

		return repositorio.buscarProduto(nome);
	}

	//
	// Lista todos os produtos especifico
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/buscar/{nome}/{tipo}")
	public List<Produto> listarEspecifico(@PathParam("nome") String nome, @PathParam("tipo") int tipo) {

		return repositorio.buscarProdutosOrdenados(nome, tipo);
	}

	//
	// Lista todos os produtos especifico
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/buscarAtivos/{tipo}")
	public List<Produto> listarProdutosAtivosOrdernadosPorTipo(@PathParam("tipo") int tipo) {

		return repositorio.ordenarProdutosAtivosPorTipo(tipo);

	}

	//
	// Lista todos os produtos especifico
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/buscarInativos/{tipo}")
	public List<Produto> listarProdutosInativosOrdernadosPorTipo(@PathParam("tipo") int tipo) {

		return repositorio.ordenarProdutosInativosPorTipo(tipo);
	}

	//
	// List todos os produtos com imagens avaliadas
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/imagensAvaliadas")
	public List<Imagem> listarImagensAvaliadas() {
		return repositorio.getImagensAvaliadas();
	}

	//
	// List todos os produtos com imagens n�o avaliadas
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/imagensNaoAvaliadas")
	public List<Imagem> listarImagensNaoAvaliadas() {
		return repositorio.getImagensNaoAvaliadas();
	}

	//
	// Busca um determinado Produto
	//
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Produto buscar(@PathParam("id") Long id) {

		return repositorio.buscar(id);
	}

	//
	// Altera um determinado Produto
	//
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response alterar(@PathParam("id") Long id, Produto produto) {
		Produto buscado = repositorio.buscar(id);
		buscado.setCaracteristicas(produto.getCaracteristicas());
		buscado.setCategoria(produto.getCategoria());
		buscado.setNome(produto.getNome());
		buscado.setPendente(produto.isPendente());
		buscado.setPreco(produto.getPreco());
		buscado.setProgresso(produto.getProgresso());
		buscado.setRemuneracaoPorAtributo(produto.getRemuneracaoPorAtributo());
		buscado.setImagens(produto.getImagens());
		repositorio.alterar(buscado);

		return Response.ok().build();
	}

	//
	// Avaliar uma determinada Imagem
	//
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/imagem/{idImagem}/{idProduto}/{remuneracao}")
	public Response avaliarImagem(@PathParam("idImagem") Long idImagem, @PathParam("idProduto") Long idProduto,
			Imagem imagem, @Context HttpServletRequest req, @PathParam("remuneracao") BigDecimal remuneracao) {

		String token = req.getHeader("Authorization");
		if (token == null) {
			return null;
		} else {
			JWTVerifier verifier = new JWTVerifier(UsuarioRestController.SECRET);
			Map<String, Object> claims = null;

			try {
				claims = verifier.verify(token);
			} catch (Exception e) {
				e.printStackTrace();
			}

			String idString = claims.get("id_usuario").toString();
			Long idUsuario = Long.valueOf(idString);

			Imagem buscada = repositorio.buscarImagem(idImagem);
			System.out.println("REMUNERACAO -------" + remuneracao);
			// BigDecimal remuneracao =
			// buscada.getProduto().getRemuneracaoPorAtributo();
			Usuario usuarioBuscado = usuarioRepositorio.buscar(idUsuario);

			if (!(usuarioBuscado instanceof Administrador)) {
				System.out.println("REMUNERACAO -------" + remuneracao);
				// avaliador
				Colaborador usuarioAvaliador = colaboradorRepositorio.buscar(idUsuario);
				buscada.setUsuarioAvaliador(usuarioAvaliador);
				BigDecimal creditoColaboradorAvaliador = usuarioAvaliador.getCredito();
				creditoColaboradorAvaliador = creditoColaboradorAvaliador.add(remuneracao);
				usuarioAvaliador.setCredito(creditoColaboradorAvaliador);
				colaboradorRepositorio.alterar(usuarioAvaliador);
				MovimentoCredito movimentoAvaliador = new MovimentoCredito();
				movimentoAvaliador.setEcoins(remuneracao);
				movimentoAvaliador.setPendente(true);
				movimentoAvaliador.setTipoMovimento(TipoMovimento.ENTRADA);
				movimentoAvaliador.setUsuario(usuarioAvaliador);
				movimentoRepositorio.adicionar(movimentoAvaliador);

				// preenchedor
				if (buscada.getUsuarioPreenchedor() != null) {
					Colaborador usuarioPreenchedor = colaboradorRepositorio
							.buscar(buscada.getUsuarioPreenchedor().getId());
					BigDecimal creditoColaboradorPreenchedor = usuarioPreenchedor.getCredito();
					creditoColaboradorPreenchedor = creditoColaboradorPreenchedor.add(remuneracao);
					usuarioPreenchedor.setCredito(creditoColaboradorPreenchedor);
					colaboradorRepositorio.alterar(usuarioPreenchedor);
					MovimentoCredito movimentoPreenchedor = new MovimentoCredito();
					movimentoPreenchedor.setEcoins(remuneracao);
					movimentoPreenchedor.setPendente(true);
					movimentoPreenchedor.setTipoMovimento(TipoMovimento.ENTRADA);
					movimentoPreenchedor.setUsuario(usuarioPreenchedor);
					movimentoRepositorio.adicionar(movimentoPreenchedor);
				}
			} else {
				Usuario usuarioAvaliador = usuarioRepositorio.buscar(idUsuario);
				buscada.setUsuarioAvaliador(usuarioAvaliador);
			}
			Produto produtoBuscado = repositorio.buscar(idProduto);
			System.out.println("PRODUTO: " + produtoBuscado.getId() + " - " + produtoBuscado.getNome());

			// List<Caracteristica> caracsAvaliadas =
			// repositorio.caracteristicaProdutoTrue(produtoBuscado.getId());
			List<Atributo> atributosPrincipais = repositorioAtributo.atributosPorTipo(0);
			// int totalAtributosPrincipais = atributosPrincipais.size();
			// int contAvaliadas = caracsAvaliadas.size();
			int contTotal = produtoBuscado.getCaracteristicas().size() + produtoBuscado.getImagens().size()
					+ atributosPrincipais.size();
			int total = 100 / contTotal;
			System.out.println("PROGRESSO IMAGEM: " + total);
			produtoBuscado.setProgresso(total);
			repositorio.alterar(produtoBuscado);
			buscada.setAvaliada(true);

			repositorio.alterarImagem(buscada);

			return Response.ok().build();
		}
	}

	//
	// Altera um determinado Produto (Apenas para mobile)
	//
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/mobile/{id}")
	public Response alterarImagens(@PathParam("id") Long id, String imagemBase64) {
		Produto buscado = repositorio.buscar(id);
		System.out.println("MOBILE ALTERAR");
		Imagem novaImagem = new Imagem();
		novaImagem.setImagemAndroid(imagemBase64);
		buscado.getImagens().add(novaImagem);

		Set<Imagem> imagens = buscado.getImagens();
		alterarImagens(novaImagem, imagens, id);

		buscado.setImagens(imagens);

		repositorio.alterar(buscado);

		return Response.ok().build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/web/{id}")
	public Response alterarImagensWeb(@PathParam("id") Long id, Set<Imagem> imagensNovas) {
		Produto buscado = repositorio.buscar(id);
		System.out.println("MOBILE ALTERAR");
		Set<Imagem> imagensExistentes = buscado.getImagens();

		if ((imagensNovas.size() + imagensExistentes.size()) <= 4 || imagensExistentes.size() == 0) {
			buscado.setImagens(imagensNovas);

			for (Imagem imagem : imagensNovas) {
				alterarImagens(imagem, imagensNovas, id);
			}

			repositorio.alterar(buscado);
		} else {
			System.out.println("LIMITE DE IMAGENS EXCEDIDO");
		}
		return Response.ok().build();
	}

	public void alterarImagens(Imagem imagem, Set<Imagem> imagens, Long idProduto) {
		String imagemBase64 = imagem.getImagemAndroid().replaceAll("\\s+", "");
		if (!imagemBase64.isEmpty()) {
			try {
				String raiz = context.getRealPath("/");
				String fileName = "prod" + idProduto + "img.jpg";
				byte[] imagemArray = Base64.getDecoder().decode(imagemBase64.getBytes());

				salvarImagemAndroid(imagemArray, raiz, fileName, imagem, imagens);
			} catch (Exception e) {
				// That string wasn't valid.
				System.out.println("ERRO AO DECODIFICAR IMAGEM: " + e.getMessage());
			}
		} else {
			System.out.println("IMAGEM ANDROID NULA");
		}

	}

	//
	// Deleta um determinado Produto
	//
	@DELETE
	@Path("/{id}")
	public Response desativar(@PathParam("id") Long id) {
		repositorio.desativar(id);
		return Response.noContent().build();
	}

	//
	// M�TODOS COMPLEMENTARES
	//
	public static String removeTillWord(String input, String word) {
		return input.substring(input.indexOf(word) + 1);
	}

	private String getFileName(MultivaluedMap<String, String> header) {

		String[] contentDisposition = header.getFirst("Content-Disposition").split(";");

		for (String filename : contentDisposition) {
			if ((filename.trim().startsWith("filename"))) {

				String[] name = filename.split("=");

				String finalFileName = name[1].trim().replaceAll("\"", "");
				return finalFileName;
			}
		}
		return "unknown";
	}

	private void salvarImagem(byte[] bytes, String raiz, String fileName, Set<Imagem> imagens) {
		try {
			Random gerador = new Random();
			int numero = gerador.nextInt();
			String nomeArquivo = String.valueOf(numero) + fileName;

			if (nomeArquivo.startsWith("-")) {
				nomeArquivo = nomeArquivo.replace("-", "").replace(" ", "");
			}

			nomeArquivo = nomeArquivo.replace(" ", "");

			String complemento = "/produto/" + nomeArquivo;
			Imagem imagem = new Imagem();
			imagem.setCaminho(complemento);
			complemento = complemento.replace("/", "\\");
			fileName = raiz + complemento;
			imagens.add(imagem);
			System.out.println("Caminho: " + fileName);
			fileName = removeTillWord(fileName, "/produto");
			System.out.println("Novo caminho: " + fileName);

			writeFile(bytes, fileName);

			System.out.println("Done");

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void salvarImagemAndroid(byte[] bytes, String raiz, String fileName, Imagem imagem, Set<Imagem> imagens) {
		try {
			Random gerador = new Random();
			int numero = gerador.nextInt();
			String nomeArquivo = String.valueOf(numero) + fileName;

			if (nomeArquivo.startsWith("-")) {
				nomeArquivo = nomeArquivo.replace("-", "").replace(" ", "");
			}

			String complemento = "/produto/" + nomeArquivo;
			imagem.setCaminho(complemento);
			complemento = complemento.replace("/", "\\");
			fileName = raiz + complemento;
			imagens.add(imagem);
			System.out.println("Caminho: " + fileName);
			fileName = removeTillWord(fileName, "/produto");
			System.out.println("Novo caminho: " + fileName);

			writeFile(bytes, fileName);

			System.out.println("Done");

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void writeFile(byte[] content, String filename) throws IOException {

		File file = new File(filename);

		if (!file.exists()) {
			file.createNewFile();
		}

		FileOutputStream fop = new FileOutputStream(file);

		fop.write(content);
		fop.flush();
		System.out.println("ANTES DE FECHAR");
		fop.close();
		System.out.println("FECHEI");

	}

	public void criarPasta() {
		try {
			File file = new File(context.getRealPath(File.separator) + File.separator + "produto");
			file.mkdirs();
			String caminhoCompleto = file.getAbsolutePath();
			System.out.println("CAMINHO COMPLETO: " + caminhoCompleto);
			System.out.println("PASTA CRIADA COM SUCESSO");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/////////////////////////////////////// WEB CRAWLER
	/////////////////////////////////////// /////////////////////////////////////////////

	public void realizarCrawler(Produto produto) throws IOException {
		System.out.println("PRODUTO NO CRAWLER: " + produto.getId());

		crawler = new WebCrawler(context.getRealPath(File.separator) + "produto", repositorio, repositorioAtributo,
				repositorioCaracteristica);

		String nomeProduto = produto.getNome();

		Categoria categoria = produto.getCategoria();

		if (categoria != null) {
			String categoriaPrincipal = getCategoriaPrincipal(categoria);
			System.out.println("entrei nas categorias");
			System.out.println("CATEGORIA PRINCIPAL: " + categoriaPrincipal);
			switch (categoriaPrincipal) {

			case "Acess�rios para Ve�culos":
				crawler.kogaKogaShop(nomeProduto, produto);
				break;

			case "Brinquedos e Hobbies":
				crawler.rihappy(nomeProduto, produto);
				break;

			case "Cal�ados, Roupas e Bolsas":

				if (getSubCategoriaPrincipal(categoria).equals("�culos")) {
					crawler.oculosShop(nomeProduto, produto);
				} else {
					crawler.dafiti(nomeProduto, produto);
				}

				break;

			case "Eletr�nicos, �udio e V�deo":
				if (getSubCategoriaPrincipal(categoria).equals("Pe�as e Componentes El�tricos")) {
					crawler.baudaEletronica(nomeProduto, produto);
				} else {
					crawler.americanas(nomeProduto, produto);
				}

				break;

			case "Esportes e Fitness":
				crawler.netshoes(nomeProduto, produto);
				break;

			case "Ferramentas e Constru��o":
				crawler.telhanorte(nomeProduto, produto);
				break;

			case "Joias e Rel�gios":
				if (getSubCategoriaPrincipal(categoria).equals("Rel�gios")) {
					crawler.myTime(nomeProduto, produto);
				} else {
					crawler.milBijus(nomeProduto, produto);
				}

				break;

			case "Livros":
				crawler.saraiva(nomeProduto, produto);
				break;

			default:
				System.out.println("CRAWLER AMERICANAS");
				crawler.americanas(nomeProduto, produto);
				break;
			}
		} else {
			crawler.americanas(nomeProduto, produto);
		}

		if ((produto.getCaracteristicas() == null && produto.getImagens() == null)
				|| (produto.getCaracteristicas().isEmpty() && produto.getImagens().isEmpty())) {
			System.out.println("SITE PADR�O");
			crawler.americanas(nomeProduto, produto);
		}

	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/crawler")
	public Response realizarCrawler() throws IOException {
		System.out.println("ENREI NO CRAWLER");
		System.out.println("RAIZ NO CONTROLLER: " + context.getRealPath("/"));

		crawler = new WebCrawler(context.getRealPath("/").replace("\\", "/"), repositorio, repositorioAtributo,
				repositorioCaracteristica);
		List<Produto> produtos = new ArrayList<Produto>();
		produtos = repositorio.produtosAtivos();
		for (Produto produto : produtos) {
			String nomeProduto = produto.getNome();

			System.out.println("NOME PRODUTO: " + nomeProduto);
			crawler.americanas(nomeProduto, produto);

		}

		return Response.ok().build();

	}

	public String getSubCategoriaPrincipal(Categoria categoria) {
		if (categoria.getPai().getPai() == null) {
			return categoria.getNome();
		}
		return getSubCategoriaPrincipal(categoria.getPai());
	}

	public String getCategoriaPrincipal(Categoria categoria) {
		if (categoria.getPai() == null) {
			return categoria.getNome();
		}
		return getCategoriaPrincipal(categoria.getPai());
	}

}
