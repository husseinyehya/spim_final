package br.com.ecommet.repositorio;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.ecommet.model.Colaborador;
import br.com.ecommet.model.Consumidor;
import br.com.ecommet.model.MovimentoCredito;
import br.com.ecommet.model.Produto;
import br.com.ecommet.model.Usuario;

@Stateless
public class MovimentoCreditoRepositorio {

	@PersistenceContext
	private EntityManager manager;

	public MovimentoCredito adicionar(MovimentoCredito movimentoCredito) {
		this.manager.persist(movimentoCredito);
		return movimentoCredito;
	}

	public MovimentoCredito buscar(Long id) {
		try {
			TypedQuery<MovimentoCredito> query = this.manager
					.createQuery("select m from MovimentoCredito m where m.id = :id", MovimentoCredito.class);
			query.setParameter("id", id);
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public List<MovimentoCredito> verificarMovimento(Long buscado) {
		System.out.println("ID QUE VEIO: " + buscado);
		try {
			TypedQuery<MovimentoCredito> query = this.manager.createQuery(
					"select m from MovimentoCredito m where m.usuario.id = :buscado and m.tipoMovimento = 1 and m.pendente = true",
					MovimentoCredito.class);
			query.setParameter("buscado", buscado);
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}
	}

	public Colaborador buscarColaborador(Long id) {
		try {
			TypedQuery<Colaborador> query = this.manager.createQuery("select c from Colaborador c where c.id = :id",
					Colaborador.class);
			query.setParameter("id", id);
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public Consumidor buscarConsumidor(Long id) {
		try {
			TypedQuery<Consumidor> query = this.manager.createQuery("select c from Consumidor c where c.id = :id",
					Consumidor.class);
			query.setParameter("id", id);
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public MovimentoCredito alterar(MovimentoCredito movimentoCredito) {
		this.manager.merge(movimentoCredito);
		return movimentoCredito;
	}

	public void deletar(Long id) {
		MovimentoCredito movimentoCredito = buscar(id);
		manager.remove(movimentoCredito);

	}

	public List<MovimentoCredito> tipoHistoricoPorUsuario(Long id, int tipo) {
		try {
			TypedQuery<MovimentoCredito> query;
			switch (tipo) {
			case 0:
				query = this.manager.createQuery("select m from MovimentoCredito m where m.usuario.id = :usuario_id",
						MovimentoCredito.class);
				break;
			case 1:
				query = this.manager.createQuery(
						"select m from MovimentoCredito m where m.usuario.id = :usuario_id and m.tipoMovimento = 0 order by m.dataAcao asc",
						MovimentoCredito.class);
				System.out.println("to aqui no 1");
				break;
			case 2:
				query = this.manager.createQuery(
						"select m from MovimentoCredito m where m.usuario.id = :usuario_id and m.tipoMovimento = 0 order by m.dataAcao desc",
						MovimentoCredito.class);
				System.out.println("to aqui no 2");
				break;
			case 3:
				query = this.manager.createQuery(
						"select m from MovimentoCredito m where m.usuario.id = :usuario_id and m.tipoMovimento = 1 order by m.dataAcao asc",
						MovimentoCredito.class);
				System.out.println("to aqui no 3");
				break;

			default:
				query = this.manager.createQuery(
						"select m from MovimentoCredito m where m.usuario.id = :usuario_id and m.tipoMovimento = 1 order by m.dataAcao desc",
						MovimentoCredito.class);
				System.out.println("to aqui no 4");

			}

			query.setParameter("usuario_id", id);
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}

	}

	public List<MovimentoCredito> historicoOrdenadorTipo(Long id, int tipo) {
		System.out.println("tipo ----------------- " + tipo);
		try {
			TypedQuery<MovimentoCredito> query;
			switch (tipo) {
			case 0:
				query = this.manager.createQuery(
						"select m from MovimentoCredito m where m.usuario.id = :usuario_id and m.pendente = false order by dataAcao desc",
						MovimentoCredito.class);
				break;

			case 1:
				query = this.manager.createQuery(
						"select m from MovimentoCredito m where m.usuario.id = :usuario_id and m.pendente = false order by dataAcao asc",
						MovimentoCredito.class);
				break;
			case 2:
				query = this.manager.createQuery(
						"select m from MovimentoCredito m where m.usuario.id = :usuario_id and m.pendente = false and m.tipoMovimento = 1 order by dataAcao desc",
						MovimentoCredito.class);
				break;
			default:
				query = this.manager.createQuery(
						"select m from MovimentoCredito m where m.usuario.id = :usuario_id and m.pendente = false and m.tipoMovimento = 0 order by dataAcao desc",
						MovimentoCredito.class);
			}

			query.setParameter("usuario_id", id);
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}

	}

	public List<MovimentoCredito> movimentoCreditosTrue() {
		TypedQuery<MovimentoCredito> query = this.manager
				.createQuery("select m from MovimentoCredito m where m.pendente = true", MovimentoCredito.class);
		return query.getResultList();
	}

	public List<MovimentoCredito> movimentoCreditosFalse() {
		TypedQuery<MovimentoCredito> query = this.manager
				.createQuery("select m from MovimentoCredito m where m.pendente = false", MovimentoCredito.class);
		return query.getResultList();
	}

	public List<MovimentoCredito> historicoPendentesPorUsuario(Long id) {
		try {
			TypedQuery<MovimentoCredito> query = this.manager.createQuery(
					"select m from MovimentoCredito m where m.usuario.id = :usuario_id and m.pendente = true order by m.dataAcao desc",
					MovimentoCredito.class);
			query.setParameter("usuario_id", id);
			return query.getResultList();
		} catch (Exception e) {
			return null;
		}

	}

	public List<MovimentoCredito> buscarRetiradasPorTitular(String nome, int tipo) {

		TypedQuery<MovimentoCredito> query;
		switch (tipo) {
		case 0:
			query = this.manager.createQuery(
					"select m from MovimentoCredito m where m.usuario.conta.titular like :nome and m.pendente = false and m.tipoMovimento = 1 order by m.dataAcao desc",
					MovimentoCredito.class);
			break;

		default:
			query = this.manager.createQuery(
					"select m from MovimentoCredito m where m.usuario.conta.titular like :nome and m.pendente = true and m.tipoMovimento = 1 order by m.dataAcao desc",
					MovimentoCredito.class);
		}

		query.setParameter("nome", "%" + nome + "%");
		System.out.println("--------------nome " + nome);
		return query.getResultList();
	}

	public List<MovimentoCredito> saidas(int tipo) {

		TypedQuery<MovimentoCredito> query;
		switch (tipo) {
		case 0:
			query = this.manager.createQuery(
					"select m from MovimentoCredito m where m.pendente = false and m.tipoMovimento = 1 order by m.dataAcao desc",
					MovimentoCredito.class);
			break;

		case 1:
			query = this.manager.createQuery(
					"select m from MovimentoCredito m where m.pendente = false and m.tipoMovimento = 1 order by m.dataAcao asc",
					MovimentoCredito.class);
			break;

		case 2:
			query = this.manager.createQuery(
					"select m from MovimentoCredito m where m.pendente = true and m.tipoMovimento = 1 order by m.dataAcao desc",
					MovimentoCredito.class);
			break;

		default:
			query = this.manager.createQuery(
					"select m from MovimentoCredito m where m.pendente = true and m.tipoMovimento = 1  order by m.dataAcao asc",
					MovimentoCredito.class);
			break;

		}

		return query.getResultList();
	}

}
