package br.com.ecommet.repositorio;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.ecommet.model.Imagem;
import br.com.ecommet.model.Produto;
import br.com.ecommet.model.Usuario;

@Stateless
public class ProdutoRepositorio {

	@PersistenceContext
	private EntityManager manager;

	public Produto adicionar(Produto produto) {
		this.manager.persist(produto);
		return produto;
	}

	public Produto deletarImagem(Long idImagem, Long idProduto) {
		Imagem imagem = buscarImagem(idImagem);
		Produto produto = buscar(idProduto);
		System.out.println("NOVO AFSDFSSS");

		System.out.println("ID IMAGEM : " + imagem.getId());
		System.out.println("ID PRODUTO: " + produto.getId());

		produto.getImagens().remove(imagem);
		this.manager.remove(imagem);
		return this.manager.merge(produto);
	}

	public List<Produto> produtosAtivos() {
		TypedQuery<Produto> query = this.manager.createQuery("select p from Produto p where ativo=true", Produto.class);
		return query.getResultList();
	}

	public List<Produto> produtosInativos() {
		TypedQuery<Produto> query = this.manager
				.createQuery("select p from Produto p where ativo=false order by p.nome", Produto.class);
		return query.getResultList();
	}

	public Produto buscar(Long id) {
		return manager.find(Produto.class, id);
	}

	public Imagem buscarImagem(Long id) {
		return manager.find(Imagem.class, id);
	}

	public Produto alterar(Produto produto) {
		this.manager.merge(produto);
		return produto;
	}

	public Imagem alterarImagem(Imagem imagem) {
		System.out.println("ID: " + imagem.getId() + " / " + imagem.isAvaliada());
		this.manager.merge(imagem);
		return imagem;
	}

	public void desativar(Long id) {
		Produto produto = buscar(id);

		if (produto.isAtivo() == true) {
			produto.setAtivo(false);
			this.manager.merge(produto);
		} else {
			produto.setAtivo(true);
			this.manager.merge(produto);
		}

	}

	public List<Produto> buscarPendentes(int tipo) {
		TypedQuery<Produto> query;
		System.out.println("tipo == " + tipo);
		switch (tipo) {
		case 0:
			query = this.manager.createQuery("select p from Produto p where p.ativo=true and p.pendente = true",
					Produto.class);
			break;
		case 1:
			query = this.manager.createQuery(
					"select p from Produto p where p.ativo=true and p.pendente = true order by p.remuneracaoPorAtributo desc",
					Produto.class);
			break;

		case 2:
			query = this.manager.createQuery(
					"select p from Produto p where p.ativo=true and p.pendente = true order by p.remuneracaoPorAtributo asc",
					Produto.class);
			break;

		case 3:
			query = this.manager.createQuery(
					"select p from Produto p where p.ativo=true and p.pendente = true order by p.dataCadastro desc",
					Produto.class);
			break;

		case 4:
			query = this.manager.createQuery(
					"select p from Produto p where p.ativo=true and p.pendente = true order by dataCadastro asc",
					Produto.class);
			break;

		default:
			query = this.manager.createQuery(
					"select p from Produto p where p.ativo=true and p.pendente = true order by p.nome", Produto.class);
			break;

		}

		return query.getResultList();
	}

	public List<Produto> buscarNaoPendentes() {
		TypedQuery<Produto> query = this.manager
				.createQuery("select p from Produto p where p.ativo=true and p.pendente = false", Produto.class);

		return query.getResultList();
	}

	public List<Produto> buscarProduto(String nome) {

		TypedQuery<Produto> query = this.manager.createQuery(
				"select p from Produto p where p.ativo=true and p.nome like :nome and p.pendente = true",
				Produto.class);
		query.setParameter("nome", "%" + nome + "%");
		System.out.println("--------------nome " + nome);
		return query.getResultList();
	}

	public List<Produto> buscarProdutosOrdenados(String nome, int tipo) {

		TypedQuery<Produto> query;
		if (tipo == 0) {
			query = this.manager.createQuery("select p from Produto p where p.ativo=true and p.nome like :nome",
					Produto.class);

		} else {
			query = this.manager.createQuery("select p from Produto p where p.ativo=false and p.nome like :nome",
					Produto.class);

		}
		query.setParameter("nome", "%" + nome + "%");
		System.out.println("--------------nome " + nome);
		return query.getResultList();
	}

	public List<Produto> ordenarProdutosAtivosPorTipo(int tipo) {
		System.out.println("tipo ------------- " + tipo);
		TypedQuery<Produto> query;

		switch (tipo) {
		case 0:
			query = this.manager.createQuery("select p from Produto p where p.ativo=true order by p.progresso desc",
					Produto.class);
			break;
		case 1:
			query = this.manager.createQuery("select p from Produto p where p.ativo=true order by p.progresso asc",
					Produto.class);
			break;
		case 2:
			query = this.manager.createQuery("select p from Produto p where p.ativo=true order by p.dataCadastro desc",
					Produto.class);
			break;
		case 3:
			query = this.manager.createQuery("select p from Produto p where p.ativo=true order by p.dataCadastro asc",
					Produto.class);
			break;
		default:
			query = this.manager.createQuery("select p from Produto p where p.ativo=true order by p.nome",
					Produto.class);
		}

		return query.getResultList();
	}

	public List<Produto> ordenarProdutosInativosPorTipo(int tipo) {
		System.out.println("tipo ------------- " + tipo);
		TypedQuery<Produto> query;
		switch (tipo) {
		case 0:
			query = this.manager.createQuery("select p from Produto p where p.ativo=false order by p.progresso desc",
					Produto.class);
			break;
		case 1:
			query = this.manager.createQuery("select p from Produto p where p.ativo=false order by p.progresso asc",
					Produto.class);
			break;
		case 2:
			query = this.manager.createQuery("select p from Produto p where p.ativo=false order by p.dataCadastro desc",
					Produto.class);
			break;
		case 3:
			query = this.manager.createQuery("select p from Produto p where p.ativo=false order by p.dataCadastro asc",
					Produto.class);
			break;
		default:
			query = this.manager.createQuery("select p from Produto p where p.ativo=false order by p.nome",
					Produto.class);
		}

		return query.getResultList();
	}

	// IMAGENS

	public List<Imagem> getImagensNaoAvaliadas() {
		TypedQuery<Imagem> query = this.manager.createQuery("select i from Imagem i where avaliada = null",
				Imagem.class);
		return query.getResultList();
	}

	public List<Imagem> getImagensAvaliadas() {
		TypedQuery<Imagem> query = this.manager.createQuery("select i from Imagem i where avaliada=true", Imagem.class);
		return query.getResultList();
	}

	public List<Imagem> imagemPorProduto(Long id, Boolean tipo, Usuario usuario) {
		try {
			System.out.println("tipoaasdasdsa: " + tipo);
			System.out.println("Usuario: " + usuario.getId());
			TypedQuery<Imagem> query = null;
			if (tipo == false) {
				query = this.manager.createQuery(
						"select i from Produto p join p.imagens i where p.id = :produto_id and i.avaliada is null and i.usuarioPreenchedor != :usuario",
						Imagem.class);
			} else {
				query = this.manager.createQuery(
						"select i from Produto p join p.imagens i where p.id = :produto_id and i.avaliada = :avaliada",
						Imagem.class);
				query.setParameter("avaliada", tipo);
			}

			query.setParameter("produto_id", id);
			query.setParameter("usuario", usuario);

			System.out.println(query.getResultList());

			return query.getResultList();
		} catch (Exception e) {
			System.out.println("ERRO EM BUSCAR PRODUTO ----- " + e.getMessage());
			e.printStackTrace();
			return null;
		}

	}

	public List<Imagem> todasImagensPorProduto(Long id) {
		try {
			TypedQuery<Imagem> query = this.manager
					.createQuery("select i from Produto p join p.imagens i where p.id = :produto_id", Imagem.class);

			query.setParameter("produto_id", id);

			return query.getResultList();
		} catch (Exception e) {
			System.out.println("ERRO EM BUSCAR PRODUTO ----- " + e.getMessage());
			e.printStackTrace();
			return null;
		}

	}

}
