package br.com.ecommet.repositorio;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.ecommet.model.Conta;

@Stateless
public class ContaRepositorio {

	@PersistenceContext
	private EntityManager manager;

	public Conta adicionar(Conta conta) {
		this.manager.persist(conta);
		return conta;
	}

	public List<Conta> contas() {
		TypedQuery<Conta> query = this.manager.createQuery("select c from Conta c", Conta.class);
		return query.getResultList();
	}

	public Conta buscar(Long id) {
		return manager.find(Conta.class, id);
	}

	public Conta alterar(Conta conta) {
		this.manager.merge(conta);
		return conta;
	}

	public void deletar(Long id) {
		Conta conta = buscar(id);
		manager.remove(conta);

	}
	
	public Conta buscarPorCpf(String cpf) {
		try {
			TypedQuery<Conta> query = this.manager.createQuery(
					"select c from Conta c where c.cpfTitular = :cpfTitular", Conta.class);
			query.setParameter("cpfTitular", cpf);
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

}
