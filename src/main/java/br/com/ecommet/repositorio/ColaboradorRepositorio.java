package br.com.ecommet.repositorio;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.ecommet.model.Colaborador;
import br.com.ecommet.model.Usuario;

@Stateless
public class ColaboradorRepositorio {

	@PersistenceContext
	private EntityManager manager;

	public Colaborador adicionar(Colaborador colaborador) {
		this.manager.persist(colaborador);
		return colaborador;
	}

	public List<Colaborador> colaboradores() {
		TypedQuery<Colaborador> query = this.manager.createQuery("select c from Colaborador c", Colaborador.class);
		return query.getResultList();
	}

	public Colaborador buscar(Long id) {
		return manager.find(Colaborador.class, id);
	}

	public Usuario alterar(Usuario usuario) {
		this.manager.merge(usuario);
		return usuario;
	}

	public void deletar(Long id) {
		Colaborador colaborador = buscar(id);
		if (colaborador.isAtivo() == true) {
			colaborador.setAtivo(false);
			this.manager.merge(colaborador);
		}

	}

	public Colaborador buscarColaborador(Long id) {
		try {
			TypedQuery<Colaborador> query = this.manager.createQuery("select c from Colaborador c where c.id = :id",
					Colaborador.class);
			query.setParameter("id", id);
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}



}
