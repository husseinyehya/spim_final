package br.com.ecommet.repositorio;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.ecommet.model.Categoria;

@Stateless
public class CategoriaRepositorio {

	@PersistenceContext
	private EntityManager manager;

	public Categoria adicionar(Categoria categoria) {
		this.manager.persist(categoria);
		return categoria;
	}

	public List<Categoria> categorias() {
		TypedQuery<Categoria> query = this.manager.createQuery("select c from Categoria c where c.pai.id = null order by c.nome",
				Categoria.class);
		return query.getResultList();
	}
	

	public List<Categoria> todasCategorias() {
		TypedQuery<Categoria> query = this.manager.createQuery("select c from Categoria c order by c.nome", Categoria.class);
		return query.getResultList();
	}

	public List<Categoria> categoriasPai(Long id) {
		try {
			TypedQuery<Categoria> query = this.manager.createQuery("select c from Categoria c where c.pai.id = :pai_id",
					Categoria.class);
			query.setParameter("pai_id", id);
			return query.getResultList();
		} catch (Exception e) {
			System.out.println("ERRO EM BUSCAR POR PAI ----- " + e.getMessage());
			return null;
		}

	}

	public Categoria buscar(Long id) { 
		return manager.find(Categoria.class, id);
	}

	public Categoria alterar(Categoria categoria) {
		this.manager.merge(categoria);
		return categoria;
	}

	public void deletar(Long id) {
		Categoria categoria = buscar(id);

		manager.remove(categoria);

	}
	
	public List<Categoria> buscarPorCategoria(String nome) {

		TypedQuery<Categoria> query = this.manager
				.createQuery("select c from Categoria c where c.nome like :nome order by c.nome", Categoria.class);
		query.setParameter("nome", "%" + nome + "%");
		System.out.println("--------------nome " + nome);
		return query.getResultList();
	}
	

}
