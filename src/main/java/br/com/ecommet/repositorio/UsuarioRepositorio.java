package br.com.ecommet.repositorio;

import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import br.com.ecommet.model.Administrador;
import br.com.ecommet.model.Colaborador;
import br.com.ecommet.model.Consumidor;
import br.com.ecommet.model.Usuario;

@Stateless
public class UsuarioRepositorio {

	@PersistenceContext
	private EntityManager manager;

	public Usuario adicionar(Usuario usuario) {
		this.manager.persist(usuario);
		return usuario;
	}

	public List<Usuario> usuarios() {
		TypedQuery<Usuario> query = this.manager.createQuery("select u from Usuario u where ativo=true", Usuario.class);
		return query.getResultList();
	}

	public Usuario buscar(Long id) {
		return manager.find(Usuario.class, id);
	}

	public Usuario alterar(Usuario usuario) {
		this.manager.merge(usuario);
		return usuario;
	}

	public void desativar(Long id) {
		Usuario usuario = buscar(id);

		if (usuario.isAtivo() == true) {
			usuario.setAtivo(false);
			this.manager.merge(usuario);
		} else {
			usuario.setAtivo(true);
			this.manager.merge(usuario);
		}

	}

	public void deletar(Long id) {
		Usuario usuario = buscar(id);
		usuario.setAtivo(false);
		this.manager.merge(usuario);
	}

	public Usuario logar(Usuario usuario) {
		TypedQuery<Usuario> query = manager
				.createQuery("select u from Usuario u where u.email = :email and u.senha = :senha", Usuario.class);
		query.setParameter("email", usuario.getEmail());
		query.setParameter("senha", usuario.getSenha());

		try {
			return query.getSingleResult();
		} catch (Exception e) {
			System.out.println("LOGIN INV�LIDO: " + e.getMessage());
			return null;
		}
	}

	public Usuario recuperarSenha(Usuario usuario) {
		TypedQuery<Usuario> query = manager.createQuery("select u from Usuario u where u.email = :email",
				Usuario.class);
		query.setParameter("email", usuario.getEmail());

		try {
			return query.getSingleResult();
		} catch (Exception e) {
			System.out.println("ERRO: " + e.getMessage());
			return null;
		}
	}

	public Usuario alterarSenha(Long id, String senhaNova) {
		Usuario usuario = manager.find(Usuario.class, id);
		usuario.setSenha(senhaNova);
		return manager.merge(usuario);
	}

	public List<Usuario> buscarPorUsuario(String nome, int tipo) {

		TypedQuery<Usuario> query;
		if (tipo == 0) {
			query = this.manager.createQuery("select u from Usuario u where u.nome like :nome and u.ativo = true",
					Usuario.class);

		} else {
			query = this.manager.createQuery("select u from Usuario u where u.nome like :nome and u.ativo = false",
					Usuario.class);
		}
		query.setParameter("nome", "%" + nome + "%");
		System.out.println("--------------nome " + nome);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Object> usuariosAtivos(int tipo) {
		System.out.println("tipo -------- " + tipo);
		Query query;

		switch (tipo) {
		case 0:
			query = this.manager.createQuery(
					"select u from Usuario u where u.ativo = true order by u.dataCadastro desc", Usuario.class);
			break;
		case 1:
			query = this.manager.createQuery("select u from Usuario u where u.ativo = true order by u.dataCadastro asc",
					Usuario.class);
			break;

		case 2:
			query = this.manager.createQuery("select u from Usuario u where u.ativo = true ", Usuario.class);
			List<Usuario> colaborador = query.getResultList();
			return colaborador.stream().filter(u -> (u instanceof Colaborador)).collect(Collectors.toList());

		case 3:
			query = this.manager.createQuery("select u from Usuario u where u.ativo = true ", Usuario.class);
			List<Usuario> consumidor = query.getResultList();
			return consumidor.stream().filter(u -> (u instanceof Consumidor)).collect(Collectors.toList());

		default:
			query = this.manager.createQuery("select u from Usuario u where u.ativo = true ", Usuario.class);
			List<Usuario> administrador = query.getResultList();
			return administrador.stream().filter(u -> (u instanceof Administrador)).collect(Collectors.toList());
		}

		return query.getResultList();

	}

	public List<Usuario> usuariosInativos(int tipo) {
		System.out.println("tipo -------- " + tipo);
		TypedQuery<Usuario> query;

		switch (tipo) {
		case 0:
			query = this.manager.createQuery(
					"select u from Usuario u where u.ativo = false order by u.dataCadastro desc", Usuario.class);
			break;
		default:
			query = this.manager.createQuery(
					"select u from Usuario u where u.ativo = false order by u.dataCadastro asc", Usuario.class);

		}

		return query.getResultList();

	}
	
	public Usuario buscarPorEmail(String email) {
		try {
			TypedQuery<Usuario> query = this.manager.createQuery(
					"select c from Usuario c where c.email = :email and c.ativo = true", Usuario.class);
			query.setParameter("email", email);
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public Usuario buscarPorCpf(String cpf) {
		try {
			TypedQuery<Usuario> query = this.manager.createQuery(
					"select c from Usuario c where c.cpf = :cpf and c.ativo = true", Usuario.class);
			query.setParameter("cpf", cpf);
			return query.getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}
	
}
