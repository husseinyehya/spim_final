package br.com.ecommet.repositorio;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.ecommet.model.Atributo;
import br.com.ecommet.model.Caracteristica;
import br.com.ecommet.model.Produto;
import br.com.ecommet.model.TipoAtributo;

@Stateless
public class CaracteristicaRepositorio {

	@PersistenceContext
	private EntityManager manager;

	public Caracteristica adicionar(Caracteristica caracteristica) {
		this.manager.persist(caracteristica);
		return caracteristica;
	}

	public List<Caracteristica> caracteristicas() {
		TypedQuery<Caracteristica> query = this.manager.createQuery("select c from Caracteristica c",
				Caracteristica.class);
		return query.getResultList();
	}

	public List<Caracteristica> caracteristicaProdutoTrue(Long id) {
		try {
			TypedQuery<Caracteristica> query = this.manager.createQuery(
					"select c from Caracteristica c where c.produto.id = :produto_id and c.avaliada = true",
					Caracteristica.class);
			query.setParameter("produto_id", id);
			return query.getResultList();
		} catch (Exception e) {
			System.out.println("ERRO EM BUSCAR PRODUTO ----- " + e.getMessage());
			return null;
		}

	}

	public List<Caracteristica> caracteristicaProdutoFalse(Long id, Long idUsuario, int tipo) {
		try {

			TypedQuery<Caracteristica> query = this.manager.createQuery(
					"select c from Caracteristica c where c.produto.id = :produto_id and c.avaliada = null and (c.usuarioPreenchedor.id != :idUsuario or c.usuarioPreenchedor.id = null) and c.atributo.tipoAtributo = :tipo ",
					Caracteristica.class);
			query.setParameter("produto_id", id);
			query.setParameter("idUsuario", idUsuario);
			query.setParameter("tipo", TipoAtributo.values()[tipo]);
			return query.getResultList();
		} catch (Exception e) {
			System.out.println("ERRO EM BUSCAR POR PRODUTO FALSE ----- " + e.getMessage());
			return null;
		}

	}

	public List<Caracteristica> caracteristicaPorProduto(Long id) {
		try {
			TypedQuery<Caracteristica> query = this.manager.createQuery(
					"select c from Caracteristica c where c.produto.id = :produto_id", Caracteristica.class);
			query.setParameter("produto_id", id);
			return query.getResultList();
		} catch (Exception e) {
			System.out.println("ERRO EM BUSCAR POR PRODUTO FALSE ----- " + e.getMessage());
			return null;
		}

	}

	public Caracteristica existente(Long idProduto, Long idAtributo, Long idUsuario) {
		try {
			TypedQuery<Caracteristica> query = this.manager.createQuery(
					"select c from Caracteristica c where c.produto.id = :produto_id and c.atributo.id = :atributo_id and c.usuarioPreenchedor.id = :usuario_id",
					Caracteristica.class);
			query.setParameter("produto_id", idProduto);
			query.setParameter("atributo_id", idAtributo);
			query.setParameter("usuario_id", idUsuario);
			return query.getSingleResult();
		} catch (Exception e) {
			System.out.println("ERRO EM BUSCAR POR NOME ----- ");
			return null;
		}
	}

	public Caracteristica existenteSecundario(Long id, String nomeAtributo) {
		try {
			TypedQuery<Caracteristica> query = this.manager.createQuery(
					"select c from Caracteristica c where c.produto.id = :produto_id and c.atributo.nome = :atributo_nome and c.avaliada=true",
					Caracteristica.class);
			query.setParameter("produto_id", id);
			query.setParameter("atributo_nome", nomeAtributo);
			return query.getSingleResult();
		} catch (Exception e) {
			System.out.println("ERRO EM BUSCAR POR NOME SECUNDARIO ----- ");
			return null;
		}
	}

	public List<Caracteristica> caracteristicaNaoAvaliada(Long id) {
		try {
			TypedQuery<Caracteristica> query = this.manager.createQuery(
					"select c from Caracteristica c where c.produto.id = :produto_id and c.avaliada = null",
					Caracteristica.class);
			query.setParameter("produto_id", id);
			return query.getResultList();
		} catch (Exception e) {
			System.out.println("ERRO EM BUSCAR POR N�O AVALIADAS ----- " + e.getMessage());
			return null;
		}

	}

	public Caracteristica buscar(Long id) {
		return manager.find(Caracteristica.class, id);
	}

	public Atributo buscarAtributo(Long id) {
		return manager.find(Atributo.class, id);
	}

	public Caracteristica alterar(Caracteristica caracteristica) {
		this.manager.merge(caracteristica);
		return caracteristica;
	}

	public Produto deletar(Long id) {
		Caracteristica caracteristica = buscar(id);
		Produto produto = caracteristica.getProduto();
		produto.getCaracteristicas().remove(caracteristica);
		this.manager.remove(caracteristica);
		return this.manager.merge(produto);

	}

}
