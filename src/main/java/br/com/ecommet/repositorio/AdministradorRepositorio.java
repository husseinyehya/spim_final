package br.com.ecommet.repositorio;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.ecommet.model.Administrador;
import br.com.ecommet.model.Colaborador;
import br.com.ecommet.model.Usuario;

@Stateless
public class AdministradorRepositorio {

	@PersistenceContext
	private EntityManager manager;

	public Administrador adicionar(Administrador administrador) {
		this.manager.persist(administrador);
		return administrador;
	}

	public List<Administrador> administradores() {
		TypedQuery<Administrador> query = this.manager.createQuery("select a from Administrador a", Administrador.class);
		return query.getResultList();
	}

	public Administrador buscar(Long id) {
		return manager.find(Administrador.class, id);
	}

	public Usuario alterar(Usuario usuario) {
		this.manager.merge(usuario);
		return usuario;
	}

	public void deletar(Long id) {
		Administrador administrador = buscar(id);
		if (administrador.isAtivo() == true) {
			administrador.setAtivo(false);
			this.manager.merge(administrador);
		}

	}
	

	
}
