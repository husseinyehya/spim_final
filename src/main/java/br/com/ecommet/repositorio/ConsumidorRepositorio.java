package br.com.ecommet.repositorio;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.ecommet.model.Colaborador;
import br.com.ecommet.model.Consumidor;
import br.com.ecommet.model.Usuario;

@Stateless
public class ConsumidorRepositorio {

	@PersistenceContext
	private EntityManager manager;

	public Usuario adicionar(Usuario usuario) {
		this.manager.persist(usuario);
		return usuario;
	}

	public List<Consumidor> consumidores() {
		TypedQuery<Consumidor> query = this.manager.createQuery("select c from Consumidor c", Consumidor.class);
		return query.getResultList();
	}

	public Consumidor buscar(Long id) {
		return manager.find(Consumidor.class, id);
	}

	public Usuario alterar(Usuario usuario) {
		this.manager.merge(usuario);
		return usuario;
	}

	public void deletar(Long id) {
		Consumidor consumidor = buscar(id);
		if (consumidor.isAtivo() == true) {
			consumidor.setAtivo(false);
			this.manager.merge(consumidor);
		}

	}
	


}
