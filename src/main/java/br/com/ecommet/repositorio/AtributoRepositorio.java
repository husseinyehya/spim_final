package br.com.ecommet.repositorio;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.ecommet.model.Atributo;
import br.com.ecommet.model.TipoAtributo;

@Stateless
public class AtributoRepositorio {

	@PersistenceContext
	private EntityManager manager;

	public Atributo adicionar(Atributo atributo) {
		this.manager.persist(atributo);
		return atributo;
	}

	public Atributo adicionarSecundario(Atributo atributo) {
		atributo.setTipoAtributo(TipoAtributo.SECUNDARIO);
		this.manager.persist(atributo);
		return atributo;
	}

	public List<Atributo> atributos() {
		TypedQuery<Atributo> query = this.manager.createQuery("select a from Atributo a", Atributo.class);
		return query.getResultList();
	}



	public List<Atributo> atributosPorTipo(int tipo) {
		System.out.println("tipo ----------" + tipo);
		TypedQuery<Atributo> query;
		switch (tipo) {
		case 0:
			query = this.manager.createQuery("select a from Atributo a where a.tipoAtributo = 0 order by a.nome",
					Atributo.class);
			break;
		case 1:
			query = this.manager.createQuery("select a from Atributo a where a.tipoAtributo = 1 order by a.nome",
					Atributo.class);
			break;

		default:
			query = this.manager.createQuery("select a from Atributo a order by a.nome", Atributo.class);
		}

		return query.getResultList();
	}

	public List<Atributo> atributosPorNomeETipo(String nome, int tipo) {
		try {
			TypedQuery<Atributo> query;
			switch (tipo) {
			case 0:
				query = this.manager.createQuery(
						"select a from Atributo a where a.nome like :nome and a.tipoAtributo = 0 order by a.nome",
						Atributo.class);
				break;

			case 1:
				query = this.manager.createQuery(
						"select a from Atributo a where a.nome like :nome and a.tipoAtributo = 1 order by a.nome",
						Atributo.class);
				break;

			default:
				query = this.manager.createQuery("select a from Atributo a where a.nome like :nome order by a.nome",
						Atributo.class);
				break;
			}

			query.setParameter("nome", "%" + nome + "%");
			return query.getResultList();
		} catch (Exception e) {
			System.out.println("ATRIBUTO JA EXISTE");
			return null;
		}
	}

	public Atributo atributosPorNome(String nome) {
		try {
			TypedQuery<Atributo> query = this.manager.createQuery("select a from Atributo a where a.nome = :nome",
					Atributo.class);
			query.setParameter("nome", nome);
			return query.getSingleResult();
		} catch (Exception e) {
			System.out.println("ATRIBUTO JA EXISTE");
			return null;
		}
	}

	public Atributo buscar(Long id) {
		return manager.find(Atributo.class, id);
	}

	public Atributo alterar(Atributo atributo) {
		this.manager.merge(atributo);
		return atributo;
	}

	public void deletar(Long id) {
		Atributo atributo = buscar(id);

		this.manager.remove(atributo);
	}

	public List<Atributo> buscarPorAtributo(String nome) {

		TypedQuery<Atributo> query = this.manager.createQuery("select a from Atributo a where a.nome like :nome",
				Atributo.class);
		query.setParameter("nome", "%" + nome + "%");
		System.out.println("--------------nome " + nome);
		return query.getResultList();
	}

}
