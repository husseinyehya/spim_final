package br.com.ecommet.repositorio;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.com.ecommet.model.Banco;

@Stateless
public class BancoRepositorio {

	@PersistenceContext
	private EntityManager manager;

	public Banco adicionar(Banco banco) {
		this.manager.persist(banco);
		return banco;
	}

	public List<Banco> bancos() {
		TypedQuery<Banco> query = this.manager.createQuery("select b from Banco b", Banco.class);
		return query.getResultList();
	}

	public Banco buscar(Long id) {
		return manager.find(Banco.class, id);
	}

	public Banco alterar(Banco banco) {
		this.manager.merge(banco);
		return banco;
	}

	public void deletar(Long id) {
		Banco banco = buscar(id);
		manager.remove(banco);

	}
}
