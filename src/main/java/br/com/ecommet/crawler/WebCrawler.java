package br.com.ecommet.crawler;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.ejb.EJB;
import javax.imageio.ImageIO;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import br.com.ecommet.model.Atributo;
import br.com.ecommet.model.Caracteristica;
import br.com.ecommet.model.Imagem;
import br.com.ecommet.model.Produto;
import br.com.ecommet.repositorio.AtributoRepositorio;
import br.com.ecommet.repositorio.CaracteristicaRepositorio;
import br.com.ecommet.repositorio.ProdutoRepositorio;

public class WebCrawler {
	@EJB
	private ProdutoRepositorio repositorioProduto;

	@EJB
	private AtributoRepositorio repositorioAtributo;

	@EJB
	private CaracteristicaRepositorio repositorioCaracteristica;
	private String context;
	private static String searchQuery = "";
	private static String baseUrl = "";
	private static List<String> listaUrl = new ArrayList<String>();
	private static String searchUrl;
	private static Elements ele;
	private static Document d;
	private static String imgUrl = "";

	public WebCrawler(String context, ProdutoRepositorio repositorioProduto, AtributoRepositorio repositorioAtributo,
			CaracteristicaRepositorio repositorioCaracteristica) {

		this.context = context;
		this.repositorioProduto = repositorioProduto;
		this.repositorioAtributo = repositorioAtributo;
		this.repositorioCaracteristica = repositorioCaracteristica;

		System.out.println("RAIZ DENTRO DO M�TODO: " + context);
	}

	public void americanas(String nomeProduto, Produto produto) {

		try {
			boolean matchEncontrado = false;
			baseUrl = "http://www.americanas.com.br";
			searchQuery = nomeProduto;

			searchUrl = baseUrl + "/busca/?conteudo=" + URLEncoder.encode(searchQuery, "UTF-8");
			d = Jsoup.connect(searchUrl).timeout(6000).get();
			ele = d.select("div#root");

			// SELECIONANDO PRODUTOS
			for (Element element : ele.select("div.product-grid-item")) {
				System.out.println("CLASS: " + element.select("section").attr("class"));
				if (!element.select("section").attr("class").equals("card-product product-unavailable")) {

					String title = element.select("section.card-product a.card-product-url").attr("title");

					String itemUrl = element.select("section.card-product a.card-product-url").attr("href");

					matchEncontrado = realizarMatch(title, nomeProduto);

					searchUrl = baseUrl + itemUrl;

					Document doc = Jsoup.connect(searchUrl).timeout(100000).get();
					Elements elem = doc.select("div#content");
					if (matchEncontrado
							&& !elem.select("div.row section").attr("class").equals("card-title unavailable")) {

						for (Element element2 : elem
								.select("div.gallery-product.swiper-wrapper figure.swiper-slide > a")) {
							imgUrl = element2.attr("href");
							break;
						}

						System.out.println("IMAGEM ANTES: " + imgUrl);

						String complemento = gerarCaminhoImagem(imgUrl, produto);

						System.out.println("IMAGEM DEPOIS: " + imgUrl);
						List<Imagem> imagens = new ArrayList<Imagem>();
						imagens.add(new Imagem(complemento));

						HashMap<String, String> caracteristicas = new HashMap<String, String>();

						for (Element element2 : elem.select("table.table-striped > tbody > tr")) {

							String titulo = element2.select("td").get(0).text();
							String descricao = element2.select("td").get(1).text();
							caracteristicas.put(titulo, descricao);
						}

						System.out.println("IMAGEM ANTES DE ALTERAR: " + imgUrl);
						realizarAlteracoes(produto, converterCaracteristica(caracteristicas), imagens, complemento,
								imgUrl);

						break;
					}

				}

			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void casasBahia(String nomeProduto, Produto produto) {
		try {

			boolean matchEncontrado = false;
			baseUrl = "http://www.casasbahia.com.br";
			searchQuery = nomeProduto;

			searchUrl = baseUrl + "/?strBusca=" + URLEncoder.encode(searchQuery, "UTF-8");

			d = Jsoup.connect(searchUrl).timeout(60000).get();
			ele = d.select("div.main");

			// SELECIONANDO PRODUTOS
			for (Element element : ele.select("div.hproduct")) {
				String title = element.select("a.link.url").attr("title").toLowerCase();

				String itemUrl = element.select("a.link.url").attr("href");

				matchEncontrado = realizarMatch(title, nomeProduto);

				// EXTRAINDO INFORMA��ES DO PRODUTO
				if (matchEncontrado) {
					searchUrl = itemUrl;

					Document doc = Jsoup.connect(searchUrl).timeout(60000).get();

					Elements elem = doc.select("form#aspnetForm");

					if (!(elem.select("div#contImagens div.boxImg div#divFullImage a.jqzoom").attr("href").isEmpty())) {
						imgUrl = elem.select("div#contImagens div.boxImg div#divFullImage a.jqzoom").attr("href");
					} else {
						imgUrl = elem.select("div#contImagens div.boxImg div#divFullImage a.jqzoom img.photo")
								.attr("src");
					}

					String complemento = gerarCaminhoImagem(imgUrl, produto);

					List<Imagem> imagens = new ArrayList<Imagem>();
					imagens.add(new Imagem(complemento));

					HashMap<String, String> caracteristicas = new HashMap<String, String>();

					String titulo = "";
					String descricao = "";
					for (Element element2 : elem.select("div#caracteristicas div.wrp > dl")) {
						System.out.println("ENTREI NO FOR");
						if (!element2.select("dt").text().isEmpty()) {
							titulo = element2.select("dt").text();
						}

						if (!element2.select("dd").text().isEmpty()) {
							descricao = element2.select("dd").text();
						}

						caracteristicas.put(titulo, descricao);

					}

					realizarAlteracoes(produto, converterCaracteristica(caracteristicas), imagens, complemento, imgUrl);

					break;
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void walmart(String nomeProduto, Produto produto) {

		try {
			boolean matchEncontrado = false;
			String complemento = null;
			baseUrl = "https://www.walmart.com.br";
			searchQuery = nomeProduto;

			searchUrl = baseUrl + "/busca/?ft=" + URLEncoder.encode(searchQuery, "UTF-8");

			d = Jsoup.connect(searchUrl).timeout(100000).get();
			ele = d.select("div#site-container");

			// SELECIONANDO PRODUTOS
			for (Element element : ele.select("section.card.zoom-out")) {

				String title = element.select("a").attr("title");

				imgUrl = "http:" + element.select("img").attr("data-src").replace("220-220", "1000-1000");

				complemento = gerarCaminhoImagem(imgUrl, produto);

				List<Imagem> imagens = new ArrayList<Imagem>();
				imagens.add(new Imagem(complemento));

				String itemUrl = "https:" + element.select("a").attr("href");

				matchEncontrado = realizarMatch(title, nomeProduto);

				// EXTRAINDO INFORMA��ES DO PRODUTO
				if (matchEncontrado) {
					searchUrl = itemUrl;
					Document doc = Jsoup.connect(searchUrl).timeout(100000).get();
					Elements elem = doc.select("div#main");

					HashMap<String, String> caracteristicas = new HashMap<String, String>();

					for (Element element2 : elem.select("table.characteristics.table-striped > tbody")) {
						String titulo = "";
						String descricao = "";
						for (Element tbody : element2.getElementsByTag("tr")) {

							titulo = tbody.select("th").text();

							if (tbody.select("td > ul").isEmpty() && tbody.select("td > p").isEmpty()) {
								descricao = tbody.select("td").text();

							} else {
								if (!tbody.select("td > ul").isEmpty()) {
									descricao = "";
									for (Element element3 : tbody.select("td > ul > li")) {
										if (descricao.isEmpty()) {
											descricao = element3.text();
										} else {
											descricao += ", " + element3.text();
										}
									}

								}

								if (!tbody.select("td > p").isEmpty()) {
									descricao = "";
									String[] valores = tbody.select("td > p").html().split("<br>");
									for (String valor : valores) {
										valor = valor.replace("&nbsp;", " ");
										if (descricao.isEmpty()) {
											descricao = valor;
										} else {
											descricao += ", " + valor;
										}
									}

								}
							}

							if (!(titulo.equals("")) && !(titulo.equalsIgnoreCase("aviso sobre o produto"))) {
								if (caracteristicas.containsKey(titulo)
										&& !caracteristicas.get(titulo).equals(descricao)) {
									String novaDescricao = caracteristicas.get(titulo);
									novaDescricao += ", " + descricao;
									caracteristicas.put(titulo, novaDescricao);
								} else {
									caracteristicas.put(titulo, descricao);
								}
							}
						}

					}

					realizarAlteracoes(produto, converterCaracteristica(caracteristicas), imagens, complemento, imgUrl);

					break;
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void saraiva(String nomeProduto, Produto produto) {
		try {
			boolean matchEncontrado = false;
			listaUrl.clear();
			baseUrl = "http://busca.saraiva.com.br";
			searchQuery = nomeProduto;

			searchUrl = baseUrl + "?q=" + URLEncoder.encode(searchQuery, "UTF-8");

			d = Jsoup.connect(searchUrl).timeout(100000).get();
			ele = d.select("div.page");

			// SELECIONANDO PRODUTOS
			for (Element element : ele.select("div.cs-product-container")) {

				String itemUrl = element.select("div.cs-list-container a.cs-product").attr("href");
				listaUrl.add(itemUrl);

			}

			// EXTRAINDO INFORMA��ES DE CADA PRODUTO
			for (String itemUrl : listaUrl) {

				searchUrl = itemUrl;

				Document doc = Jsoup.connect(searchUrl).timeout(100000).userAgent("Chrome").get();
				Elements elem = doc.select("div.page");

				String title = elem.select("section.product-allinfo section.product-info h1.livedata").text();

				matchEncontrado = realizarMatch(title, nomeProduto);

				if (matchEncontrado) {

					imgUrl = elem.select("div.product-image-center div.container-product-image a.image-link img")
							.attr("src");

					String complemento = gerarCaminhoImagem(imgUrl, produto);

					List<Imagem> imagens = new ArrayList<Imagem>();
					imagens.add(new Imagem(complemento));

					HashMap<String, String> caracteristicas = new HashMap<String, String>();

					for (Element element : elem.select("table#product-attribute-specs-table")) {
						String titulo = null;
						String descricao = null;
						for (Element tbody : element.select("tr")) {

							titulo = tbody.select("th").text();
							descricao = tbody.select("td").text();

							caracteristicas.put(titulo, descricao);

						}

					}

					realizarAlteracoes(produto, converterCaracteristica(caracteristicas), imagens, complemento, imgUrl);

					break;

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void dafiti(String nomeProduto, Produto produto) {
		try {
			boolean matchEncontrado = false;
			baseUrl = "https://www.dafiti.com.br";
			searchQuery = nomeProduto;

			searchUrl = baseUrl + "/catalog/?q=" + URLEncoder.encode(searchQuery, "UTF-8");

			d = Jsoup.connect(searchUrl).timeout(100000).get();
			ele = d.select("div#wrapper");

			// SELECIONANDO PRODUTOS
			for (Element element : ele.select("div.product-box")) {

				if (!(element.select("div.product-box-detail p.product-box-title.hide-mobile").text().equals("")
						|| element.select("a.product-box-link").attr("href").equals(""))) {

					String title = element.select("div.product-box-detail p.product-box-title.hide-mobile").text();

					String itemUrl = element.select("a.product-box-link").attr("href");

					matchEncontrado = realizarMatch(title, nomeProduto);

					// EXTRAINDO INFORMA��ES DO PRODUTO
					if (matchEncontrado) {
						searchUrl = itemUrl;
						System.out.println("URL ITEM : " + searchUrl);
						Document doc = Jsoup.connect(searchUrl).timeout(100000).get();

						Elements elem = doc.select("div#wrapper");

						for (Element element2 : elem.select("ul.carousel-items.row")) {
							for (Element element3 : element2
									.select("li.carousel-item.gallery-items.col-md-2 a.gallery-thumb")) {
								imgUrl = element3.attr("href");
								break;
							}
						}

						String complemento = gerarCaminhoImagem(imgUrl, produto);

						List<Imagem> imagens = new ArrayList<Imagem>();
						imagens.add(new Imagem(complemento));

						HashMap<String, String> caracteristicas = new HashMap<String, String>();

						for (Element element2 : elem.select("table.product-informations")) {
							for (Element element3 : element2.select("tr")) {
								String titulo = element3.select("td").get(0).text().replace(":", "");
								String descricao = element3.select("tr > td").get(1).text();

								caracteristicas.put(titulo, descricao);

							}

						}

						realizarAlteracoes(produto, converterCaracteristica(caracteristicas), imagens, complemento,
								imgUrl);

						break;
					}
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void milBijus(String nomeProduto, Produto produto) {
		try {
			boolean matchEncontrado = false;
			baseUrl = "http://milbijus.com.br";
			searchQuery = nomeProduto;

			searchUrl = baseUrl + "/busca?q=" + URLEncoder.encode(searchQuery, "UTF-8").replace("+", "-");

			d = Jsoup.connect(searchUrl).timeout(100000).get();
			System.out.println(searchUrl);
			ele = d.select("div.product-grid");

			// SELECIONANDO PRODUTOS
			for (Element element : ele.select("ul.clearfix.grid li")) {
				String title = element.select("a.title").text();

				String itemUrl = element.select("a.title").attr("href");

				matchEncontrado = realizarMatch(title, nomeProduto);

				// EXTRAINDO INFORMA��ES DO PRODUTO
				if (matchEncontrado) {
					searchUrl = itemUrl;
					Document doc = Jsoup.connect(searchUrl).timeout(100000).userAgent("Chrome").get();
					Elements elem = doc.select("div.product_page");
					Elements elem1 = doc.getElementsByClass("big_img");

					for (Element element2 : elem1.select("div.item")) {
						imgUrl = element2.select("a").attr("href");
						break;
					}

					String complemento = gerarCaminhoImagem(imgUrl, produto);

					List<Imagem> imagens = new ArrayList<Imagem>();
					imagens.add(new Imagem(complemento));

					HashMap<String, String> caracteristicas = new HashMap<String, String>();

					for (Element element2 : elem.select("div.texto_descricao p")) {
						if (element2.text().contains(": ") || element2.text().contains(" - ")) {
							String titulo = element2.text().split("-|\\:")[0].trim();
							String descricao = element2.text().split("-|\\:")[1].trim();

							caracteristicas.put(titulo, descricao);
						}

					}

					realizarAlteracoes(produto, converterCaracteristica(caracteristicas), imagens, complemento, imgUrl);

					break;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void netshoes(String nomeProduto, Produto produto) {
		try {
			boolean matchEncontrado = false;
			baseUrl = "http://search.netshoes.com.br";
			searchQuery = nomeProduto;

			searchUrl = baseUrl + "/?q=" + URLEncoder.encode(searchQuery, "UTF-8");

			d = Jsoup.connect(searchUrl).timeout(100000).get();
			System.out.println(searchUrl);
			ele = d.select("div.cs-results.grid");

			// SELECIONANDO PRODUTOS
			for (Element element : ele.select("li.cs-product-container")) {
				String title = element
						.select("a.cs-product.ns-countdown span.cs-product-details-container h2.cs-product-title")
						.text();
				String itemUrl = element.select("a.cs-product.ns-countdown").attr("href");

				matchEncontrado = realizarMatch(title, nomeProduto);

				// EXTRAINDO INFORMA��ES DO PRODUTO
				if (matchEncontrado) {
					searchUrl = itemUrl;
					Document doc = Jsoup.connect(searchUrl).timeout(100000).userAgent("Chrome").get();

					Elements elem = doc.select("div.body-wrapper");
					Elements elem1 = doc.getElementsByClass("photo-gallery-list");

					for (Element element2 : elem1.select("li")) {
						imgUrl = "http:" + element2.select("a").attr("data-large");
						break;
					}

					String complemento = gerarCaminhoImagem(imgUrl, produto);

					List<Imagem> imagens = new ArrayList<Imagem>();
					imagens.add(new Imagem(complemento));

					HashMap<String, String> caracteristicas = new HashMap<String, String>();

					for (Element element2 : elem.select("ul.description-list li")) {

						if (!(element2.text().contains("OBS:") || element2.text().contains("Importante:")
								|| element2.text().contains("Garantia do Fabricante:"))) {
							String titulo = element2.select("strong").text().replace(":", "");
							String descricao = element2.text().replace(titulo, "").replace(":", "").trim();

							caracteristicas.put(titulo, descricao);
						}

					}

					realizarAlteracoes(produto, converterCaracteristica(caracteristicas), imagens, complemento, imgUrl);

					break;
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void poupafarma(String nomeProduto, Produto produto) {
		try {
			boolean matchEncontrado = false;
			baseUrl = "https://www.poupafarma.com.br";
			searchQuery = nomeProduto;

			searchUrl = baseUrl + "/busca/" + URLEncoder.encode(searchQuery, "UTF-8").replace("+", "-");

			d = Jsoup.connect(searchUrl).timeout(100000).get();
			ele = d.select("div.Main");
			// SELECIONANDO PRODUTOS
			for (Element element : ele.select("div.item-prod")) {
				String itemUrl = baseUrl + element.select("ul.no-bullet li.nome a.link-prod").attr("href");
				listaUrl.add(itemUrl);
			}

			// EXTRAINDO INFORMA��ES DE CADA PRODUTO
			for (String itemUrl : listaUrl) {

				searchUrl = itemUrl;
				Document doc = Jsoup.connect(searchUrl).timeout(100000).userAgent("Chrome").get();
				// pw.println("URL PRODUTO: " + searchUrl);
				Elements elem = doc.select("div.Main");
				Element elem1 = doc.getElementById("gallery");

				String title = elem.select("div.row.prod-index div#side-prod div.show-for-large-up h2").text();

				matchEncontrado = realizarMatch(title, nomeProduto);

				if (matchEncontrado) {

					for (Element element : elem1.select("li")) {
						imgUrl = baseUrl + element.select("a").attr("data-zoom-image");
						break;
					}

					String complemento = gerarCaminhoImagem(imgUrl, produto);

					List<Imagem> imagens = new ArrayList<Imagem>();
					imagens.add(new Imagem(complemento));

					HashMap<String, String> caracteristicas = new HashMap<String, String>();

					for (Element element : elem.select(
							"div.prod-descr div.row div.columns.large-8.medium-12.prod-content-left div.row.collapse div.columns.large-12.medium-12 div.others-advertising.panel.callout div.register-ms > p")) {
						String titulo = element.text().split(":")[0].trim();
						String descricao = element.text().split(":")[1].trim();

						caracteristicas.put(titulo, descricao);
					}

					realizarAlteracoes(produto, converterCaracteristica(caracteristicas), imagens, complemento, imgUrl);

					break;

				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void telhanorte(String nomeProduto, Produto produto) {
		try {
			boolean matchEncontrado = false;
			listaUrl.clear();
			baseUrl = "http://busca.telhanorte.com.br";
			searchQuery = nomeProduto;

			searchUrl = baseUrl + "/busca?q=" + URLEncoder.encode(searchQuery, "UTF-8");

			d = Jsoup.connect(searchUrl).timeout(100000).get();
			System.out.println(searchUrl);
			ele = d.select("ul.neemu-products-container.nm-view-type-grid");
			// SELECIONANDO PRODUTOS
			if (!(ele.hasText())) {
				ele = d.select("div.main");
				for (Element element : ele
						.select("div.prateleira.vitrine.n25colunas > ul > li:not(.helperComplement)")) {
					String title = element.select("a").attr("title");
					imgUrl = element.select("a.productImage img").attr("src").replaceAll("161-161", "1000-1000");

					String itemUrl = element.select("a").attr("href");

					matchEncontrado = realizarMatch(title, nomeProduto);

					// EXTRAINDO INFORMA��ES DO PRODUTO
					if (matchEncontrado) {
						searchUrl = itemUrl;
						Document doc = Jsoup.connect(searchUrl).timeout(100000).userAgent("Chrome").get();
						Elements elem = doc.select("div.content");

						String complemento = gerarCaminhoImagem(imgUrl, produto);

						List<Imagem> imagens = new ArrayList<Imagem>();
						imagens.add(new Imagem(complemento));

						HashMap<String, String> caracteristicas = new HashMap<String, String>();

						for (Element element2 : elem.select("div.detalhes div.container-detalhes")) {

							for (Element element3 : element2.select("div.especificacao div#caracteristicas table")) {
								String titulo = null;
								String descricao = null;
								for (Element tbody : element3.select("tr")) {

									titulo = tbody.select("th").text().trim();
									descricao = tbody.select("td").text().trim();
									if (!titulo.equals("Caracter�sticas T�cnicas") && !titulo.equals("Benef�cios")
											&& !titulo.equals("Garantia")) {

										caracteristicas.put(titulo, descricao);
									}

								}
							}

							for (Element element3 : element2
									.select("div.dimensoes div#div_Conteudo_DetalhesDoProduto_pnlDimensoes dl")) {
								String tituloDimensao;
								String descricaoDimensao;
								for (Element element4 : element3.select("dd")) {

									tituloDimensao = element4.text().split(":")[0].trim();
									descricaoDimensao = element4.text().split(":")[1].trim();

									caracteristicas.put(tituloDimensao, descricaoDimensao);

								}
							}
						}

						realizarAlteracoes(produto, converterCaracteristica(caracteristicas), imagens, complemento,
								imgUrl);
						break;
					}

				}
			} else {
				for (Element element : ele.select("li.nm-product-item")) {
					String title = element.select("div.nm-product-info div.nm-product-data h2.nm-product-name a")
							.text();
					imgUrl = "http:"
							+ element.select("div.nm-product-img-container a.nm-product-img-link img.nm-product-img")
									.attr("src").replaceAll("200-200", "1000-1000");

					String itemUrl = "http:" + element
							.select("div.nm-product-info div.nm-product-data h2.nm-product-name a").attr("href");

					matchEncontrado = realizarMatch(title, nomeProduto);

					// EXTRAINDO INFORMA��ES DO PRODUTO
					if (matchEncontrado) {
						searchUrl = itemUrl;
						System.out.println("PRODUTO TELHANORTE: " + searchUrl);
						Document doc = Jsoup.connect(searchUrl).timeout(100000).userAgent("Chrome").get();
						Elements elem = doc.select("div.content");

						String complemento = gerarCaminhoImagem(imgUrl, produto);

						List<Imagem> imagens = new ArrayList<Imagem>();
						imagens.add(new Imagem(complemento));

						HashMap<String, String> caracteristicas = new HashMap<String, String>();

						for (Element element2 : elem.select("div.detalhes div.container-detalhes")) {

							for (Element element3 : element2.select("div.especificacao div#caracteristicas table")) {
								String titulo = null;
								String descricao = null;
								for (Element tbody : element3.select("tr")) {

									titulo = tbody.select("th").text().trim();
									descricao = tbody.select("td").text().trim();
									if (!titulo.equals("Caracter�sticas T�cnicas") && !titulo.equals("Benef�cios")
											&& !titulo.equals("Garantia")) {
										caracteristicas.put(titulo, descricao);
									}

								}
							}

							for (Element element3 : element2
									.select("div.dimensoes div#div_Conteudo_DetalhesDoProduto_pnlDimensoes dl")) {
								String tituloDimensao = null;
								String descricaoDimensao = null;
								for (Element element4 : element3.select("dd")) {

									tituloDimensao = element4.text().split(":")[0].trim();
									descricaoDimensao = element4.text().split(":")[1].trim();

									caracteristicas.put(tituloDimensao, descricaoDimensao);

								}
							}
						}

						realizarAlteracoes(produto, converterCaracteristica(caracteristicas), imagens, complemento,
								imgUrl);

						break;
					}

				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void oculosShop(String nomeProduto, Produto produto) {
		try {
			boolean matchEncontrado = false;
			searchQuery = nomeProduto;
			baseUrl = "http://www.oculosshop.com.br";

			searchUrl = baseUrl + "/catalogsearch/result/?q=" + URLEncoder.encode(searchQuery, "UTF-8");

			d = Jsoup.connect(searchUrl).timeout(100000).get();
			System.out.println(searchUrl);
			ele = d.select("div.category-products");

			// SELECIONANDO PRODUTOS
			int j = 1;
			for (Element element : ele.select("div.products-grid.clearfix article.col-sm-6.col-md-3.item")) {
				String title = element.select("a").attr("title");

				System.out.println("Produto n� " + j + " - " + title);
				String itemUrl = element.select("a").attr("href");
				listaUrl.add(itemUrl);
				System.out.println("Link produto: " + itemUrl);
				System.out.println("");

				matchEncontrado = realizarMatch(title, nomeProduto);

				// EXTRAINDO INFORMA��ES DO PRODUTO
				if (matchEncontrado) {
					searchUrl = itemUrl;
					Document doc = Jsoup.connect(searchUrl).timeout(100000).userAgent("Chrome").get();
					Elements elem = doc.select("div.clearfix");

					imgUrl = elem
							.select("div.col-md-7.col-product-view div.product-view div.product-essential div.product-img-box.clearfix.image-reload div.product-image.product-image-zoom img.elevatezoom")
							.attr("data-zoom-image");

					String complemento = gerarCaminhoImagem(imgUrl, produto);

					List<Imagem> imagens = new ArrayList<Imagem>();
					imagens.add(new Imagem(complemento));

					HashMap<String, String> caracteristicas = new HashMap<String, String>();

					for (Element element2 : elem.select(
							"div.col-md-7.col-product-view div.product-view div.section-info section.section-product-view.product-collateral.clearfix span.additional_attributes-reload div.clearfix.description.description-specifications div.col-sm-6 ul.list-specifications li")) {
						String descricao = element2.select("strong").text().trim();
						String titulo = element2.text();
						titulo = titulo.replace(descricao, "").replace(":", "");

						caracteristicas.put(titulo, descricao);

					}

					realizarAlteracoes(produto, converterCaracteristica(caracteristicas), imagens, complemento, imgUrl);

					break;
				}

				j++;

				if (j == 21) {
					break;
				}
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void baudaEletronica(String nomeProduto, Produto produto) {
		try {
			boolean matchEncontrado = false;
			baseUrl = "http://www.baudaeletronica.com.br";
			searchQuery = nomeProduto;

			searchUrl = baseUrl + "/catalogsearch/result/?q=" + URLEncoder.encode(searchQuery, "UTF-8");

			d = Jsoup.connect(searchUrl).timeout(100000).get();
			System.out.println(searchUrl);
			ele = d.select("ul.products-grid");
			for (Element element : ele.select("li.item")) {
				String title = element.select("h2.product-name a").text();

				String itemUrl = element.select("h2.product-name a").attr("href");

				matchEncontrado = realizarMatch(title, nomeProduto);

				if (matchEncontrado) {
					searchUrl = itemUrl;
					Document doc = Jsoup.connect(searchUrl).timeout(100000).userAgent("Chrome").get();
					Elements elem = doc.select("div.main.container");

					imgUrl = elem.select("div.product-img-column div.img-box p.product-image a#zoom1").attr("href");

					String complemento = gerarCaminhoImagem(imgUrl, produto);

					List<Imagem> imagens = new ArrayList<Imagem>();
					imagens.add(new Imagem(complemento));

					HashMap<String, String> caracteristicas = new HashMap<String, String>();

					for (Element element2 : elem.select("div.tabs-panels div.panel div.std p")) {
						if (element2.text().contains("- ")) {
							String[] caracteristicasGerais = element2.text().split("- ");
							for (String caracteristica : caracteristicasGerais) {
								if (caracteristica.contains(": ")) {
									String titulo = caracteristica.split(": ")[0].trim();
									String descricao = caracteristica.split(": ")[1].trim();

									caracteristicas.put(titulo, descricao);
								}
							}
						}
					}

					realizarAlteracoes(produto, converterCaracteristica(caracteristicas), imagens, complemento, imgUrl);

					break;
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void casaMedica(String nomeProduto, Produto produto) {
		try {
			boolean matchEncontrado = false;
			baseUrl = "http://www.casamedica.com.br";
			searchQuery = nomeProduto;

			searchUrl = baseUrl + "/" + URLEncoder.encode(searchQuery, "UTF-8").replace("+", "%20");

			d = Jsoup.connect(searchUrl).timeout(100000).get();
			System.out.println(searchUrl);
			ele = d.select("div.prateleira.vitrine.n3colunas");
			int j = 1;
			for (Element element : ele.select("ul > li > a.productImage")) {
				String title = element.attr("title");
				String itemUrl = element.attr("href");

				matchEncontrado = realizarMatch(title, nomeProduto);

				if (matchEncontrado) {
					searchUrl = itemUrl;
					Document doc = Jsoup.connect(searchUrl).timeout(100000).userAgent("Chrome").get();
					Elements elem = doc.select("div#content");
					Element elem1 = doc.getElementById("caracteristicas");

					imgUrl = elem
							.select("div.span6 div.thumbnail.text-center div.apresentacao div#show div#include div#image a.image-zoom")
							.attr("href");

					String complemento = gerarCaminhoImagem(imgUrl, produto);

					List<Imagem> imagens = new ArrayList<Imagem>();
					imagens.add(new Imagem(complemento));

					HashMap<String, String> caracteristicas = new HashMap<String, String>();

					String[] caracteristicasGerais = elem1.select("table.group.Especificacoes > tbody > tr > td").html()
							.split("<br>");

					for (String caracteristica : caracteristicasGerais) {
						if (!caracteristica.trim().isEmpty() && caracteristica.contains(": ")
								&& !caracteristica.trim().equals("Tamanho da Caixa")
								&& !caracteristica.contains("Modo de usar") && !caracteristica.contains("Precau��es")
								&& !caracteristica.contains("Conte�do do pacote") && !caracteristica.contains("- ")) {
							String titulo = caracteristica.split(":")[0].trim();
							String descricao = caracteristica.split(":")[1].trim();

							caracteristicas.put(titulo, descricao);
						}

					}

					realizarAlteracoes(produto, converterCaracteristica(caracteristicas), imagens, complemento, imgUrl);

					break;
				}

				j++;

				if (j == 13) {
					break;
				}
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void kogaKogaShop(String nomeProduto, Produto produto) {
		try {
			boolean matchEncontrado = false;
			baseUrl = "http://kogakogashop.com.br";
			listaUrl.clear();
			searchQuery = nomeProduto;

			searchUrl = baseUrl + "/Busca?q=" + URLEncoder.encode(searchQuery, "UTF-8");

			d = Jsoup.connect(searchUrl).timeout(100000).get();
			System.out.println(searchUrl);
			ele = d.select("div#productList");

			for (Element element : ele.select("div.prodLista")) {
				String item_url = baseUrl
						+ element.select("div#BlocoProdutos div.infosProdsReduzidas div.NomeProd a").attr("href");
				listaUrl.add(item_url);

			}

			for (String itemUrl : listaUrl) {

				searchUrl = itemUrl;
				System.out.println("URL PRODUTO: " + searchUrl);
				Document doc = Jsoup.connect(searchUrl).timeout(100000).userAgent("Chrome").get();
				Elements elem = doc.select("div.container");

				String title = elem.select("div.halfRight div.prodName h1.name").text();

				matchEncontrado = realizarMatch(title, nomeProduto);

				if (matchEncontrado) {
					String stringJson = elem
							.select("div.halfLeft div.thumbs div.thumbHolder ul.thumbnails li.producthtumb.tamanhoImgMiniatura a.cloudzoom-gallery.thumbnail")
							.attr("data-cloudzoom");
					JSONObject jsonObject = new JSONObject(stringJson);
					String imgUrlJson = (String) jsonObject.get("zoomImage");

					imgUrl = URLEncoder.encode(imgUrlJson, "UTF-8").replace("%3A", ":").replace("%2F", "/");

					String complemento = gerarCaminhoImagem(imgUrl, produto);

					List<Imagem> imagens = new ArrayList<Imagem>();
					imagens.add(new Imagem(complemento));

					HashMap<String, String> caracteristicas = new HashMap<String, String>();

					if (!elem.select("div.boxAbas div.conteudo").text().contains("Caracteristicas: ")) {
						for (Element element : elem.select("div.boxAbas div.conteudo > ul")) {
							System.out.println("PRIMEIRO FOR");
							boolean pararBusca = false;
							for (Element element2 : element.select("li")) {
								String caracteristicasGerais = element2.text();

								if (caracteristicasGerais.contains(": ")) {
									String titulo = caracteristicasGerais.split(":")[0].trim();
									String descricao = caracteristicasGerais.split(":")[1].trim();

									caracteristicas.put(titulo, descricao);
									pararBusca = true;
								}
							}

							if (pararBusca) {
								break;
							}

						}
					} else {
						for (Element element : elem.select("div.boxAbas div.conteudo")) {
							if (element.text().contains(": ")) {
								String[] caracteristicasGerais = elem.select("div.boxAbas div.conteudo").html()
										.split("<br>");
								System.out.println("SEGUNDO FOR");
								for (String caracteristica : caracteristicasGerais) {
									if (!caracteristica.trim().isEmpty() && caracteristica.contains(": ")) {
										String titulo = caracteristica.split(":")[0].trim();
										String descricao = caracteristica.split(":")[1].trim();
										if (!(titulo.contains("<b>") || titulo.contains("<p>")
												|| titulo.contains("<ul>") || titulo.contains("<li>")
												|| titulo.contains("<") || descricao.contains("<b>")
												|| descricao.contains("<p>") || descricao.contains("<ul>")
												|| descricao.contains("<li>") || descricao.contains("<"))) {

											caracteristicas.put(titulo, descricao);
										}

									}

								}
								break;
							}
						}

					}

					realizarAlteracoes(produto, converterCaracteristica(caracteristicas), imagens, complemento, imgUrl);

					break;
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void myTime(String nomeProduto, Produto produto) {
		try {
			boolean matchEncontrado = false;
			baseUrl = "http://www.mytime.com.br";
			searchQuery = nomeProduto;

			searchUrl = baseUrl + "/busca?busca=" + URLEncoder.encode(searchQuery, "UTF-8");

			d = Jsoup.connect(searchUrl).timeout(100000).get();
			System.out.println(searchUrl);
			ele = d.select("div.spots-interna");

			int j = 1;
			for (Element element : ele.select("div.spot div.spotContent")) {
				String title = element.select("div.fbits-spot-conteudo > a > h3.spotTitle").text();
				String itemUrl = baseUrl + element.select("a").attr("href");

				matchEncontrado = realizarMatch(title, nomeProduto);

				if (matchEncontrado) {
					searchUrl = itemUrl;
					System.out.println("URL PRODUTO: " + searchUrl);
					Document doc = Jsoup.connect(searchUrl).timeout(100000).userAgent("Chrome").get();
					Elements elem = doc.select("div.interna.cf");
					Element elem1 = doc.getElementById("galeria");

					for (Element element2 : elem1.select("li.fbits-produto-imagensMinicarrossel-item")) {
						imgUrl = element2.select("a").attr("data-zoom-image");
						break;
					}

					String complemento = gerarCaminhoImagem(imgUrl, produto);

					List<Imagem> imagens = new ArrayList<Imagem>();
					imagens.add(new Imagem(complemento));

					HashMap<String, String> caracteristicas = new HashMap<String, String>();

					if (elem.select(
							"div > div.fbits-produto-informacoes-extras > div > div.infoProd div.paddingbox > div > div")
							.hasText()) {
						for (Element element2 : elem.select(
								"div > div.fbits-produto-informacoes-extras > div > div.infoProd div.paddingbox > div > div")) {
							String titulo = element2.select("span.mytime_inf_c").first().text().replace(":", "").trim();
							String descricao = element2.select("span.mytime_inf_c").last().text();

							caracteristicas.put(titulo, descricao);
						}
					} else {
						String[] caracteristicasGerais = elem
								.select("div > div.fbits-produto-informacoes-extras > div > div.infoProd div.paddingbox")
								.html().split("<br>");
						String[] caracteristicasFormatadas = caracteristicasGerais[caracteristicasGerais.length - 1]
								.replace("Dados t�cnicos:", "").split("\\.");

						for (String caracteristica : caracteristicasFormatadas) {

							String titulo = caracteristica.replace("(", "").replace(")", "").trim().split(":")[0]
									.trim();
							String descricao = caracteristica.replace("(", "").replace(")", "").trim().split(":")[1]
									.trim();

							caracteristicas.put(titulo, descricao);
						}

					}

					realizarAlteracoes(produto, converterCaracteristica(caracteristicas), imagens, complemento, imgUrl);

					break;
				}

				j++;

				if (j == 21) {
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void rihappy(String nomeProduto, Produto produto) {
		try {
			boolean matchEncontrado = false;
			baseUrl = "http://www.rihappy.com.br";
			searchQuery = nomeProduto;
			listaUrl.clear();

			searchUrl = baseUrl + "/" + URLEncoder.encode(searchQuery, "UTF-8").replace("+", "%20") + "?qs="
					+ URLEncoder.encode(searchQuery, "UTF-8").replace("+", "%20");

			d = Jsoup.connect(searchUrl).timeout(100000).get();
			System.out.println(searchUrl);
			ele = d.select("div.baby-departamento");

			for (Element element : ele.select("div.baby-departamento.n3colunas ul > li")) {
				String itemUrl = element.select("a.productIdInRel").attr("href");

				if (!itemUrl.equals("")) {
					listaUrl.add(itemUrl);
				}

			}

			for (String itemUrl : listaUrl) {

				searchUrl = itemUrl;
				Document doc = Jsoup.connect(searchUrl).timeout(100000).userAgent("Chrome").get();
				Elements elem = doc.select("div.center.home-align");
				Element elem1 = doc.getElementById("caracteristicas");

				String title = elem.select("h1.nome-produto div.fn.productName").text();

				matchEncontrado = realizarMatch(title, nomeProduto);

				if (matchEncontrado) {

					imgUrl = elem
							.select("div.produto-images div.apresentacao div#show div#include div#image img#image-main")
							.attr("src").replace("400-400", "800-800");

					String complemento = gerarCaminhoImagem(imgUrl, produto);

					List<Imagem> imagens = new ArrayList<Imagem>();
					imagens.add(new Imagem(complemento));

					HashMap<String, String> caracteristicas = new HashMap<String, String>();

					for (Element element : elem1.select("table.group.Especificacoes")) {
						String titulo = null, descricao = null;
						for (Element tbody : element.select("tr")) {

							titulo = tbody.select("th").text().replace(":", "").trim();
							descricao = tbody.select("td").text().trim();

							caracteristicas.put(titulo, descricao);

						}
					}

					for (Element element : elem1.select("table.group.Informacoes-do-Fornecedor")) {
						String titulo = null, descricao = null;
						for (Element tbody : element.select("tr")) {

							titulo = tbody.select("th").text().replace(":", "").trim();
							descricao = tbody.select("td").text().trim();

							if (titulo.contains("Fornecedor")) {
								caracteristicas.put(titulo, descricao);
							}

						}
					}

					realizarAlteracoes(produto, converterCaracteristica(caracteristicas), imagens, complemento, imgUrl);

					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/////////////////////////////// OUTROS M�TODOS

	private List<Caracteristica> converterCaracteristica(HashMap<String, String> caracteristicas) {
		List<Caracteristica> caracs = new ArrayList<Caracteristica>();
		caracteristicas.forEach((titulo, descricao) -> {
			if (!(titulo.equals("") && descricao.equals("")) && descricao.length() <= 255 && !titulo.toLowerCase().contains("aviso")) {
				titulo = Jsoup.parse(titulo).text();
				descricao = Jsoup.parse(descricao).text();
				caracs.add(new Caracteristica(titulo, descricao));
			}
		});
		return caracs;
	}

	private boolean realizarMatch(String title, String nomeProduto) {
		if (title.contains(nomeProduto) || title.equals(nomeProduto) || nomeProduto.contains(title)
				|| nomeProduto.equalsIgnoreCase(nomeProduto)) {
			System.out.println("MATCH ENCONTRADO!");
			return true;
		} else {
			System.out.println("MATCH N�O ENCONTRADO!");
			return false;

		}
	}

	private String gerarCaminhoImagem(String imgUrl, Produto produto) {
		String nomeImagem = "prod" + produto.getId().toString() + "img";
		String complemento = "";
		boolean podeSalvarImagem = true;

		if (!imgUrl.isEmpty()) {
			BufferedImage buffImagemNova = null;
			try {

				URL url = new URL(new String(imgUrl.getBytes(), "UTF-8"));
				System.out.println("URL IMAGEM PRODUTO: " + url);
				buffImagemNova = ImageIO.read(url);

				if (buffImagemNova != null) {
					System.out.println("FILE IMAGEM NOVA EXISTE: " + buffImagemNova);
					Set<Imagem> imagensExistentes = produto.getImagens();

					if (imagensExistentes == null) {
						podeSalvarImagem = true;
					} else {

						if (imagensExistentes.size() >= 1 && imagensExistentes.size() <= 3) {
							String raiz = context;
							for (Imagem imagemExistente : imagensExistentes) {
								File fileImagemExistente = new File(raiz + imagemExistente.getCaminho());
								BufferedImage buffImagemExistente = ImageIO.read(fileImagemExistente);
								System.out.println("ANTES DE COMPARAR IMAGENS");
								if (compararImagens(buffImagemExistente, buffImagemNova)) {
									System.out.println("IMAGEM J� EXISTE");
									podeSalvarImagem = false;
								} else {
									System.out.println("IMAGEM AINDA N�O EXISTE");
									podeSalvarImagem = true;
								}
							}
						}

					}
				}

			} catch (IOException e) {
				e.printStackTrace();
			}

			if (podeSalvarImagem) {
				Random gerador = new Random();
				int numero = gerador.nextInt();
				String nomeArquivo = nomeImagem + String.valueOf(numero).replace("-", "") + ".jpg";

				complemento = "/produto/" + nomeArquivo;
			} else {
				complemento = null;
			}

			return complemento;
		} else {
			System.out.println("IMAGEM NULA");
			return null;
		}
	}

	private void realizarAlteracoes(Produto produto, List<Caracteristica> caracteristicasNovas,
			List<Imagem> imagensNovas, String complemento, String imagemUrl) {
		System.out.println("ENTREI NO REALIZAR ALTERA��ES");
		System.out.println("TOTAL ROBO: " + caracteristicasNovas.size());
		try {
			System.out.println("ontgdfogjndfopsgmnds");
			System.out.println("ENTREI NO REALIZAR ALTERA��ES");
			Set<Caracteristica> caracteristicasExistentes = produto.getCaracteristicas();

			List<Atributo> todosAtributosExistentes = repositorioAtributo.atributos();
			if (caracteristicasNovas != null && todosAtributosExistentes != null) {
				System.out.println("ENTREI AQUI");

				caracteristicasNovas.forEach(caracteristicaNova -> {
					String nomeAtributoNovo = caracteristicaNova.getAtributo().getNome();

					todosAtributosExistentes.stream()
							.filter(atributoExistente -> atributoExistente.getNome().equals(nomeAtributoNovo))
							.forEach(atributoExistente -> {
								Atributo atributoNovo = caracteristicaNova.getAtributo();
								atributoNovo = atributoExistente;
								caracteristicaNova.setAtributo(atributoNovo);
							});
				});

			}

			// Se j� existir uma ou mais caracter�sticas, compara se as novas
			// caracter�sticas s�o iguais as caracter�sticas
			// Se n�o, ele j� adiciona as novas caracter�sticas no produto
			// diretamente
			if (caracteristicasExistentes != null) {
				System.out.println("J� TEM CARACS");
				List<Caracteristica> caracteristicasRepetidas = new ArrayList<Caracteristica>();
				caracteristicasExistentes.forEach(caracExistenteDoProduto -> {

					String nomeExistenteDoAtributo = caracExistenteDoProduto.getAtributo().getNome().trim();
					// TipoAtributo tipoAtributoExistente =
					// caracExistenteDoProduto.getAtributo().getTipoAtributo();
					// String valorExistenteDaCaracteristica =
					// caracExistenteDoProduto.getValor();
					caracteristicasNovas.stream()
							.filter(nova -> nova.getAtributo().getNome().equals(nomeExistenteDoAtributo))
							.forEach(nova -> {
								// if (valorExistenteDaCaracteristica == null
								// && tipoAtributoExistente ==
								// TipoAtributo.PRINCIPAL) {
								// caracExistenteDoProduto.setValor(nova.getValor());
								// repositorioCaracteristica.alterar(caracExistenteDoProduto);
								// }

								caracteristicasRepetidas.add(nova);

							});

					caracteristicasNovas.removeAll(caracteristicasRepetidas);
				});

			}

			caracteristicasNovas.forEach(caracteristicaNova -> {
				caracteristicaNova.setProduto(produto);
				Atributo atributo = caracteristicaNova.getAtributo();
				if (atributo.getId() == null) {
					repositorioAtributo.adicionar(atributo);
				}
			});

			if (caracteristicasNovas != null) {
				System.out.println("CARACS FINAL: " + caracteristicasNovas.size());
				produto.setCaracteristicas(new HashSet<Caracteristica>(caracteristicasNovas));

			}

			// Se o set de imagens e o caminho da imagem n�o forem nulos,
			// adiciona o set de imagens no
			// produto diretamente e salva a imagem no servidor.
			if (imagensNovas != null && complemento != null) {
				System.out.println("IMAGEM ANTES DE SALVAR: " + imagemUrl);
				salvarImagem(imagemUrl, complemento);

				produto.setImagens(new HashSet<Imagem>(imagensNovas));
			}
			System.out.println("ANTES DE ALTERAR O PRODUTO: " + produto.getCaracteristicas().size());
			Produto produtoNovo = repositorioProduto.alterar(produto);
			System.out.println("ALTEREI O PRODUTO: " + produtoNovo.getCaracteristicas().size());
			System.out.println("ALTEREI O PRODUTO");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void salvarImagem(String imgUrl, String caminhoImagem) {
		System.out.println("IMAGEM DENTRO DO SALVAR: " + imgUrl);
		try {
			criarPasta();

			URL url = new URL(new String(imgUrl.getBytes(), "UTF-8"));
			InputStream is = url.openStream();
			String raiz = context;
			System.out.println("CAMINHO ANTES DE SALVAR:" + raiz);
			caminhoImagem = caminhoImagem.replace("/produto/", "");
			System.out.println("CAMINHO COMPLETO ANTES DE SALVAR: " + raiz + File.separator + caminhoImagem);
			OutputStream os = new FileOutputStream(raiz + File.separator + caminhoImagem);

			byte[] b = new byte[1024];
			int length;

			while ((length = is.read(b)) != -1) {
				os.write(b, 0, length);
			}

			is.close();
			os.close();

			System.out.println("SEM FECHAR CRAWLER");

			System.out.println("Imagem salva com sucesso!");
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Erro ao salvar imagem: " + e.getMessage());
		}

	}

	private void criarPasta() {
		try {
			File file = new File(context);
			file.mkdirs();
			String caminhoCompleto = file.getAbsolutePath();
			System.out.println("CAMINHO COMPLETO: " + caminhoCompleto);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static boolean compararImagens(BufferedImage biA, BufferedImage biB) {
		try {
			// take buffer data from both image files //
			DataBuffer dbA = biA.getData().getDataBuffer();
			int sizeA = dbA.getSize();
			DataBuffer dbB = biB.getData().getDataBuffer();
			int sizeB = dbB.getSize();
			// compare data-buffer objects //
			if (sizeA == sizeB) {
				for (int i = 0; i < sizeA; i++) {
					if (dbA.getElem(i) != dbB.getElem(i)) {
						return false;
					}
				}
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			System.out.println("Failed to compare image files ... " + e.getMessage());
			return false;
		}
	}

}
