package br.com.ecommet.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "imagem")
public class Imagem {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String caminho;
	@Transient
	private String imagemAndroid;
	@ManyToOne(optional = true, targetEntity = Usuario.class)
	private Usuario usuarioAvaliador;
	@ManyToOne(optional = true, targetEntity = Usuario.class)
	private Usuario usuarioPreenchedor;
	private Boolean avaliada = null; 
	
	public Imagem() {
	}

	public Imagem(String caminho) {
		this.setCaminho(caminho);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCaminho() {
		return caminho;
	}

	public void setCaminho(String caminho) {
		this.caminho = caminho;
	}

	public String getImagemAndroid() {
		return imagemAndroid;
	}

	public void setImagemAndroid(String imagemAndroid) {
		this.imagemAndroid = imagemAndroid;
	}

	public Boolean isAvaliada() {
		return avaliada;
	}

	public void setAvaliada(Boolean avaliada) {
		this.avaliada = avaliada;
	}

	public Usuario getUsuarioAvaliador() {
		return usuarioAvaliador;
	}

	public void setUsuarioAvaliador(Usuario usuarioAvaliador) {
		this.usuarioAvaliador = usuarioAvaliador;
	}

	public Usuario getUsuarioPreenchedor() {
		return usuarioPreenchedor;
	}

	public void setUsuarioPreenchedor(Usuario usuarioPreenchedor) {
		this.usuarioPreenchedor = usuarioPreenchedor;
	}

	
	

}
