package br.com.ecommet.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.ws.rs.FormParam;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;




@Entity

@Table(name = "caracteristica")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Caracteristica {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne(optional = false, targetEntity = Atributo.class)
	private Atributo atributo;
	@Size(min = 1, max = 255)
	private String valor;
	@ManyToOne(optional = false, targetEntity = Produto.class)
	private Produto produto;
	@ManyToOne(optional = true, targetEntity = Usuario.class)
	@JsonIgnoreProperties(ignoreUnknown = true)
	@XmlAnyElement(lax=true)
	private Usuario usuarioPreenchedor;
	@ManyToOne(optional = true, targetEntity = Usuario.class)
	@JsonIgnoreProperties(ignoreUnknown = true)
	@XmlAnyElement(lax=true)
	private Usuario usuarioAvaliador;
	private Boolean avaliada = null;
	
	public Caracteristica() {
	}

	public Caracteristica(String titulo, String descricao) {
		Atributo at = new Atributo();
		at.setNome(titulo);
		at.setTipoAtributo(TipoAtributo.SECUNDARIO);
		this.setAtributo(at);
		this.setValor(descricao);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getValor() {
		return valor;
	}

	@FormParam(value = "valor")
	public void setValor(String valor) {
		this.valor = valor;
	}

	@XmlTransient
	public Produto getProduto() {
		return produto;
	}

	@XmlElement(name = "produto")
	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public Atributo getAtributo() {
		return atributo;
	}

	public void setAtributo(Atributo atributo) {
		this.atributo = atributo;
	}

	public Usuario getUsuarioPreenchedor() {
		return usuarioPreenchedor;
	}

	public void setUsuarioPreenchedor(Usuario usuarioPreenchedor) {
		this.usuarioPreenchedor = usuarioPreenchedor;
	}

	public Usuario getUsuarioAvaliador() {
		return usuarioAvaliador;
	}

	public void setUsuarioAvaliador(Usuario usuarioAvaliador) {
		this.usuarioAvaliador = usuarioAvaliador;
	}

	public Boolean isAvaliada() {
		return avaliada;
	}

	public void setAvaliada(Boolean avaliada) {
		this.avaliada = avaliada;
	}
	
}
