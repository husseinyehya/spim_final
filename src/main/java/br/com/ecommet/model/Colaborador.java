package br.com.ecommet.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonIgnore;
import com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.auth0.jwt.internal.com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name="colaborador")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Colaborador extends Usuario {
	
	@JsonIgnoreProperties
	private int ranking = 0;	
	@JsonIgnoreProperties
	private BigDecimal credito = BigDecimal.valueOf(0.0);
	
	
	public int getRanking() {
		return ranking;
	}
	public void setRanking(int ranking) {
		this.ranking = ranking;
	}
	public BigDecimal getCredito() {
		return credito;
	}
	public void setCredito(BigDecimal credito) {
		this.credito = credito;
	}
	

	
	

	
}
