package br.com.ecommet.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.ws.rs.FormParam;
import javax.xml.bind.annotation.XmlTransient;

import com.fasterxml.jackson.annotation.JsonFormat;


@Entity
@Table(name = "produto")
public class Produto {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nome;
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Imagem> imagens;
	private BigDecimal remuneracaoPorAtributo;
	private int progresso = 0;
	private boolean pendente = true;
	private BigDecimal preco;
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", timezone ="Brazil/East")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	private boolean ativo = true;
	@ManyToOne(optional = true, targetEntity = Categoria.class)
	private Categoria categoria;
	@OneToMany(mappedBy = "produto", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private Set<Caracteristica> caracteristicas;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	@FormParam("nome")
	public void setNome(String nome) {
		this.nome = nome;
	}

	public BigDecimal getRemuneracaoPorAtributo() {
		return remuneracaoPorAtributo;
	}

	public Set<Imagem> getImagens() {
		return imagens;
	}

	public void setImagens(Set<Imagem> imagens) {
		this.imagens = imagens;
	}

	@FormParam("remuneracaoPorAtributo")
	public void setRemuneracaoPorAtributo(BigDecimal remuneracaoPorAtributo) {
		this.remuneracaoPorAtributo = remuneracaoPorAtributo;
	}

	public int getProgresso() {
		return progresso;
	}

	public void setProgresso(int progresso) {
		this.progresso = progresso;
	}

	public boolean isPendente() {
		return pendente;
	}

	// @FormParam("pendente")
	public void setPendente(boolean pendente) {
		this.pendente = pendente;
	}

	public BigDecimal getPreco() {
		return preco;
	}

	@FormParam("preco")
	public void setPreco(BigDecimal preco) {
		this.preco = preco;
	}

	
	public Date getDataCadastro() {
		return dataCadastro;
	}

	@FormParam("dataCadastro")
	public void setDataCadastro(Date dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public boolean isAtivo() {
		return ativo;
	}

	@FormParam("ativo")
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	@FormParam("categoria")
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Set<Caracteristica> getCaracteristicas() {
		return caracteristicas;
	}

	@FormParam("caracteristicas")
	public void setCaracteristicas(Set<Caracteristica> caracteristicas) {
		this.caracteristicas = caracteristicas;
	}

}
