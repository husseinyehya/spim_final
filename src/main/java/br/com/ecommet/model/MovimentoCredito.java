package br.com.ecommet.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;



@Entity
@Table(name = "movimentoCredito")
public class MovimentoCredito {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@ManyToOne(optional = false, targetEntity = Usuario.class)
	private Usuario usuario;

	private BigDecimal valor;

	private BigDecimal ecoins;
	private TipoMovimento tipoMovimento;
	
	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", timezone ="Brazil/East")
	@Temporal(TemporalType.TIMESTAMP)	
	private Date dataAcao;
	
	private boolean pendente = true;

	/*// mudar dataAcao para dataSolicitacao
	@Temporal(TemporalType.DATE) private Date dataFinalizacao = new Date();*/
	 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public BigDecimal getEcoins() {
		return ecoins;
	}

	public void setEcoins(BigDecimal ecoins) {
		this.ecoins = ecoins;
	}

	public TipoMovimento getTipoMovimento() {
		return tipoMovimento;
	}

	public void setTipoMovimento(TipoMovimento tipoMovimento) {
		this.tipoMovimento = tipoMovimento;
	}

	public Date getDataAcao() {
		return dataAcao;
	}

	public void setDataAcao(Date dataAcao) {
		this.dataAcao = dataAcao;
	}

	public boolean isPendente() {
		return pendente;
	}

	public void setPendente(boolean pendente) {
		this.pendente = pendente;
	}

}
