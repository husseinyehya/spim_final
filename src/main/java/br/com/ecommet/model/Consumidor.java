package br.com.ecommet.model;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="consumidor")
public class Consumidor extends Usuario {
	
	private BigDecimal credito = BigDecimal.valueOf(0.0);

	public BigDecimal getCredito() {
		return credito;
	}

	public void setCredito(BigDecimal credito) {
		this.credito = credito;
	}
	
	
}
