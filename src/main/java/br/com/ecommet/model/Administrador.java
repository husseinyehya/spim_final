package br.com.ecommet.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="administrador")
public class Administrador extends Usuario {
	
	private TipoAdministrador tipoAdministrador;

	public TipoAdministrador getTipoAdministrador() {
		return tipoAdministrador;
	}

	public void setTipoAdministrador(TipoAdministrador tipoAdministrador) {
		this.tipoAdministrador = tipoAdministrador;
	}
	
	
	
}
