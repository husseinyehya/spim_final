package br.com.ecommet.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.ws.rs.FormParam;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;

import org.jboss.resteasy.annotations.providers.multipart.PartType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;



@Entity
@Table(name = "usuario")
@Inheritance(strategy = InheritanceType.JOINED)
@JsonIgnoreProperties(ignoreUnknown=true)
public class Usuario {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(length = 64, nullable = false)
	@Size(min = 2, max = 64)
	private String nome;

	@Column(length = 64, nullable = false)
	@Size(min = 2, max = 64)
	private String sobrenome;

	@Column(length = 15, nullable = false, unique = true)
	@Size(min = 14, max = 14)
	@Pattern(regexp = "[0-9]{3}\\.?[0-9]{3}\\.?[0-9]{3}\\-?[0-9]{2}")
	private String cpf;

	@Column(length = 255, nullable = false, unique = true)
	@Size(min = 5, max = 255)
	@Pattern(regexp = "\\A[^@]+@([^@\\.]+\\.)+[^@\\.]+\\z")
	private String email;

	@Column(nullable = false)
	@Size(min = 13, max = 13)
	@Pattern(regexp = "\\([0-9]{2}\\)[0-9]{4,5}\\-[0-9]{4}")
	private String telefone;

	@Column(nullable = false)
	@Size(min = 13, max = 14)
	@Pattern(regexp = "\\([0-9]{2}\\)[0-9]{5}\\-[0-9]{3,4}")
	private String celular;

	@Column(nullable = false)
	@Size(min = 8, max = 64)
	private String senha;

	@JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss", timezone ="Brazil/East")	@Temporal(TemporalType.TIMESTAMP)
	private Date dataCadastro;
	private boolean ativo = true;
	@OneToOne(optional = true, cascade = CascadeType.ALL, targetEntity = Conta.class)
	private Conta conta;
	
	@Transient
	@Column(nullable = true)
	private byte[] imagemPerfil;
	@Column(nullable = true)

	private String caminhoImagem;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	@FormParam("nome")
	public String getNome() {
		return nome;
	}

	@FormParam("nome")
	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSobrenome() {
		return sobrenome;
	}
	
	@FormParam("sobrenome")
	public void setSobrenome(String sobrenome) {
		this.sobrenome = sobrenome;
	}

	public String getCpf() {
		return cpf;
	}

	@FormParam("cpf")
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	@FormParam("email")
	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	@FormParam("telefone")
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCelular() {
		return celular;
	}

	@FormParam("celular")
	public void setCelular(String celular) {
		this.celular = celular;
	}

	@XmlTransient
	public String getSenha() {
		return senha;
	}

	@XmlElement
	@FormParam("senha")
	public void setSenha(String senha) {
	
		this.senha = senha;
	}

	public Date getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Date dataCadastro) {

		this.dataCadastro = dataCadastro;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Conta getConta() {
		return conta;
	}

	public void setConta(Conta conta) {
		this.conta = conta;
	}

	@XmlTransient
	public byte[] getImagemPerfil() {
		return imagemPerfil;
	}

	@FormParam("fotoPerfil")
	@PartType(MediaType.APPLICATION_OCTET_STREAM)
	public void setImagemPerfil(byte[] imagemPerfil) {
		this.imagemPerfil = imagemPerfil;
	}
	
	public String getCaminhoImagem() {
		return caminhoImagem;
	}

	public void setCaminhoImagem(String caminhoImagem) {
		this.caminhoImagem = caminhoImagem;
	}



}
